/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.git;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.nuiton.scmwebeditor.api.RepositoryNotFoundException;
import org.nuiton.scmwebeditor.api.ScmFileManager;
import org.nuiton.scmwebeditor.api.dto.*;
import org.nuiton.scmwebeditor.api.dto.result.*;

import javax.naming.AuthenticationException;
import java.io.File;
import java.io.IOException;

/**
 * Implementation of the Git's features related to file management
 */
public class GitFileManager implements ScmFileManager {

    private static final Log log = LogFactory.getLog(GitFileManager.class);

    protected final String PLACEHOLDER_MESSAGE_EN = "This file has been automatically added by SCMWebEditor when you " +
            "created its directory.\nGit repositories require at least one file per directory, so this file was created " +
            "according to this rule.\nYou can remove this file once you have added other files in this directory.";

    protected final String PLACEHOLDER_MESSAGE_FR = "Ce fichier a été ajouté automatiquement par SCMWebEditor lorsque " +
            "vous avez créé son répertoire.\nLes dépôts Git imposent qu'il y ait au moins un fichier par répertoire, " +
            "ce fichier a donc été créé selon cette règle.\nVous pouvez supprimer ce fichier une fois que vous avez " +
            "ajouté d'autres fichiers à ce répertoire.";

    /** the connection to the Git repository */
    GitConnection connection;

    /**
     * Creates a new file manager for Git repositories
     * @param connection the connection to the Git repository
     * @throws IOException if the repository can not be reached
     */
    public GitFileManager(GitConnection connection) throws IOException {

        this.connection = connection;
    }

    @Override
    public UploadFileResultDto uploadFile(UploadFileDto dto) {

        UploadFileResultDto resultDto = new UploadFileResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;
        } catch (IOException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;
        } catch (AuthenticationException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(UploadFileResultDto.AUTH_ERROR);
            return resultDto;
        }

        resultDto.setFileRoot(connection.getAddressGit());
        resultDto.setScmRoot(connection.getAddressGit());

        // if there is no file to upload, we get back to the upload form
        if (dto.getUpload() == null) {
            resultDto.setError(UploadFileResultDto.REDIRECT);
            return resultDto;
        }

        if (log.isDebugEnabled()) {
            log.debug("FileName : " + dto.getUploadFileName());
            log.debug("ContentType : " + dto.getUploadContentType());
        }

        // Writing the file to the local directory
        String pathOnRepo = dto.getScmPath();
        String path;

        if (pathOnRepo.length() > connection.getAddressGit().length()) {
            pathOnRepo = pathOnRepo.substring(connection.getAddressGit().length() + 1);
            path = pathOnRepo + File.separator + dto.getUploadFileName();
        } else {
            path = dto.getUploadFileName();
        }

        File file = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + path);

        try {
            FileUtils.copyFile(dto.getUpload(), file);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't copy the file to the local directory", e);
            }

            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;
        }

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }

        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {
            Git git = Git.open(connection.getLocalDirectory());

            // add
            AddCommand add = git.add();
            add.addFilepattern(path);

            try {
                add.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not add new files", e);
                }
            }

            // commit
            try {
                connection.doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- add the file : "
                        + dto.getUploadFileName());
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(UploadFileResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            PushCommand push = git.push();
            push.setRemote(connection.getAddressGit());
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                boolean fileDeleted = file.delete();
                if (!fileDeleted && log.isDebugEnabled()) {
                    log.debug("Could not delete file " + file.getAbsolutePath());
                }

                connection.handlePushException(e);

                if (e.getMessage().endsWith("not authorized")) {
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } else {
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                return resultDto;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + connection.getLocalDirectory().getAbsolutePath(), e);
            }

            boolean fileDeleted = file.delete();
            if (!fileDeleted && log.isDebugEnabled()) {
                log.debug("Could not delete file " + file.getAbsolutePath());
            }
            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;
        }

        return resultDto;
    }

    @Override
    public RemoveFileResultDto removeFile(RemoveFileDto dto) {

        RemoveFileResultDto resultDto = new RemoveFileResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (IOException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (AuthenticationException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
            return resultDto;
        }

        resultDto.setFileRoot(connection.getAddressGit());
        resultDto.setScmRoot(connection.getAddressGit());


        // if there is no file to remove, we get back to the remove form
        if (dto.getScmPath() == null) {
            resultDto.setError(RemoveFileResultDto.REDIRECT);
            return resultDto;
        } else if (dto.getScmPath().equals("") || dto.getScmPath().equals(connection.getAddressGit())) {
            resultDto.setError(RemoveFileResultDto.REDIRECT);
            return resultDto;
        }

        if (log.isDebugEnabled()) {
            log.debug("FileName : " + dto.getScmPath());
        }

        // Removing the file from the local directory
        String pathOnRepo = dto.getScmPath();

        if (pathOnRepo.length() > connection.getAddressGit().length()) {
            pathOnRepo = pathOnRepo.replace(connection.getAddressGit() + "/", "");
        }

        File file = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + pathOnRepo);

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }

        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {
            Git git = Git.open(connection.getLocalDirectory());

            // removing the file
            RmCommand rm = git.rm();
            rm.addFilepattern(pathOnRepo);
            try {
                rm.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not remove Git file " + pathOnRepo, e);
                }

                resultDto.setError(RemoveFileResultDto.ERROR);
                return resultDto;
            }
            file.delete();

            // commit
            try {
                connection.doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- remove the file : " + pathOnRepo);
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(RemoveFileResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            PushCommand push = git.push();
            push.setRemote(connection.getAddressGit());
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                connection.handlePushException(e);

                try {
                    connection.cloneRepository(credentials);
                } catch (AuthenticationException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } catch (RepositoryNotFoundException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                if (e.getMessage().endsWith("not authorized")) {
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } else {
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                return resultDto;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + connection.getLocalDirectory().getAbsolutePath(), e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;
        }

        return resultDto;
    }

    @Override
    public CreateDirectoryResultDto createDirectory(CreateDirectoryDto dto) {

        CreateDirectoryResultDto resultDto = new CreateDirectoryResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;

        } catch (IOException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;

        } catch (AuthenticationException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(UploadFileResultDto.AUTH_ERROR);
            return resultDto;
        }

        resultDto.setFileRoot(connection.getAddressGit());
        resultDto.setScmRoot(connection.getAddressGit());

        // if the name of the new directory is empty we get back to the create directory form
        if (dto.getDirectoryName() == null) {

            resultDto.setError(UploadFileResultDto.REDIRECT);
            return resultDto;
        }

        if (dto.getDirectoryName().equals("")) {

            resultDto.setError(UploadFileResultDto.REDIRECT);
            return resultDto;
        }

        // Creating the new directory in the local directory
        String pathOnRepo = dto.getParentDirectory();

        if (pathOnRepo.length() > connection.getAddressGit().length()) {
            pathOnRepo = pathOnRepo.substring(connection.getAddressGit().length() + 1);
        } else {
            pathOnRepo = "";
        }

        File file = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + pathOnRepo +
                File.separator + dto.getDirectoryName());
        file.mkdir();
        File emptyFile = new File(file.getAbsolutePath() + File.separator + "placeholder");

        try {
            emptyFile.createNewFile();
            FileUtils.writeStringToFile(emptyFile, PLACEHOLDER_MESSAGE_EN + "\n\n" + PLACEHOLDER_MESSAGE_FR);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not create file '" + emptyFile.getAbsolutePath() + "'");
            }
        }

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }

        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {
            Git git = Git.open(connection.getLocalDirectory());

            // add
            AddCommand add = git.add();
            String toAdd;

            if (pathOnRepo.equals("")) {
                toAdd = dto.getDirectoryName() + "/placeholder";
            } else {
                toAdd = pathOnRepo + "/" + dto.getDirectoryName() + "/placeholder";
            }

            add.addFilepattern(toAdd);

            try {
                add.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not add new files", e);
                }
            }

            // commit
            try {
                connection.doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- create the directory : " + dto.getDirectoryName());
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(UploadFileResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            PushCommand push = git.push();
            push.setRemote(connection.getAddressGit());
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                emptyFile.delete();
                file.delete();

                connection.handlePushException(e);

                if (e.getMessage().endsWith("not authorized")) {
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } else {
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                return resultDto;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + connection.getLocalDirectory().getAbsolutePath(), e);
            }

            emptyFile.delete();
            file.delete();
            resultDto.setError(UploadFileResultDto.ERROR);
            return resultDto;
        }

        return resultDto;
    }

    @Override
    public RemoveDirectoryResultDto removeDirectory(RemoveDirectoryDto dto) {

        RemoveDirectoryResultDto resultDto = new RemoveDirectoryResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (IOException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (AuthenticationException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
            return resultDto;
        }

        resultDto.setFileRoot(connection.getAddressGit());
        resultDto.setScmRoot(connection.getAddressGit());

        // if there is no file to remove, we get back to the remove form
        if (dto.getDirectoryToRemove() == null) {
            resultDto.setError(RemoveFileResultDto.REDIRECT);
            return resultDto;
        }
        if (dto.getDirectoryToRemove().equals("") || dto.getDirectoryToRemove().equals(connection.getAddressGit())) {
            resultDto.setError(RemoveFileResultDto.REDIRECT);
            return resultDto;
        }

        // Removing the directory from the local directory
        String pathOnRepo = dto.getDirectoryToRemove();

        if (pathOnRepo.length() > connection.getAddressGit().length()) {
            pathOnRepo = pathOnRepo.replace(connection.getAddressGit() + "/", "");
        }

        File directory = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + pathOnRepo);

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }

        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {
            Git git = Git.open(connection.getLocalDirectory());

            // removing the directory
            RmCommand rm = git.rm();
            rm.addFilepattern(pathOnRepo);
            try {
                rm.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not execute Git remove " + pathOnRepo, e);
                }

                resultDto.setError(RemoveFileResultDto.ERROR);
                return resultDto;
            }
            directory.delete();

            // commit
            try {
                connection.doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- remove the directory : " + pathOnRepo);
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(RemoveFileResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            PushCommand push = git.push();
            push.setRemote(connection.getAddressGit());
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                connection.handlePushException(e);

                try {
                    connection.cloneRepository(credentials);
                } catch (AuthenticationException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } catch (RepositoryNotFoundException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                if (e.getMessage().endsWith("not authorized")) {
                    resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                } else {
                    resultDto.setError(RemoveFileResultDto.ERROR);
                }

                return resultDto;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + connection.getLocalDirectory().getAbsolutePath(), e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;
        }

        return resultDto;
    }

    @Override
    public MoveFileResultDto moveFile(MoveFileDto dto) {

        MoveFileResultDto resultDto = new MoveFileResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (IOException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

            resultDto.setError(RemoveFileResultDto.ERROR);
            return resultDto;

        } catch (AuthenticationException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
            return resultDto;
        }

        resultDto.setFileRoot(connection.getAddressGit());
        resultDto.setScmRoot(connection.getAddressGit());

        // if the name of the file to move is empty we get back to the move a file form
        if (dto.getFileToMove() == null) {
            resultDto.setError(MoveFileResultDto.REDIRECT);
            return resultDto;
        }

        if (dto.getFileToMove().equals("") || dto.getFileToMove().equals(resultDto.getFileRoot())) {
            resultDto.setError(MoveFileResultDto.REDIRECT);
            return resultDto;
        }

        // if the name of the destination directory is empty we get back to the move a file form
        if (dto.getDestinationDirectory() == null) {
            resultDto.setError(MoveFileResultDto.REDIRECT);
            return resultDto;
        }

        if (dto.getDestinationDirectory().equals("")) {
            resultDto.setError(MoveFileResultDto.REDIRECT);
            return resultDto;
        }

        // Moving the file on the local directory
        String pathOnRepoFile = dto.getFileToMove();

        if (pathOnRepoFile.length() > connection.getAddressGit().length()) {
            pathOnRepoFile = pathOnRepoFile.replace(connection.getAddressGit() + "/", "");
        }

        String pathOnRepoDirectory = dto.getDestinationDirectory();

        if (pathOnRepoDirectory.length() > connection.getAddressGit().length()) {
            pathOnRepoDirectory = pathOnRepoDirectory.replace(connection.getAddressGit() + "/", "");
        }

        String sourceFileName = pathOnRepoFile.substring(pathOnRepoFile.lastIndexOf('/') + 1);

        File sourceFile = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + pathOnRepoFile);
        File destFile = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + pathOnRepoDirectory
                + File.separator + sourceFileName);

        try {
            FileUtils.moveFile(sourceFile, destFile);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not move file " + sourceFile.getAbsolutePath() + " to " + destFile.getAbsolutePath(), e);
            }
            return resultDto;
        }

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }

        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {
            Git git = Git.open(connection.getLocalDirectory());

            // adding the moved file
            AddCommand add = git.add();
            add.addFilepattern(pathOnRepoDirectory + "/" + sourceFileName);
            try {
                add.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not execute Git add " + pathOnRepoDirectory + "/" + sourceFileName, e);
                }

                resultDto.setError(MoveFileResultDto.ERROR);
                return resultDto;
            }

            // commit
            try {
                connection.doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- move the file : " + pathOnRepoFile +
                        " to : " + pathOnRepoDirectory);
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(MoveFileResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            PushCommand push = git.push();
            push.setRemote(connection.getAddressGit());
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                connection.handlePushException(e);

                try {
                    connection.cloneRepository(credentials);
                } catch (AuthenticationException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(MoveFileResultDto.AUTH_ERROR);
                } catch (RepositoryNotFoundException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not clone repository at address " + connection.getAddressGit());
                    }
                    resultDto.setError(MoveFileResultDto.ERROR);
                }

                if (e.getMessage().endsWith("not authorized")) {
                    resultDto.setError(MoveFileResultDto.AUTH_ERROR);
                } else {
                    resultDto.setError(MoveFileResultDto.ERROR);
                }

                return resultDto;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + connection.getLocalDirectory().getAbsolutePath(), e);
            }

            resultDto.setError(MoveFileResultDto.ERROR);
            return resultDto;
        }

        return resultDto;
    }


    /**
     * Updates the repository to the last version by a clone or a pull command
     * @param username the username to use to connect to the repository
     * @param password the password to use to connect to the repository
     * @throws RepositoryNotFoundException if the repository is not found
     * @throws IOException if it is not possible to reach the repository
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    public void updateRepository(String username, String password)
            throws RepositoryNotFoundException, IOException, AuthenticationException {

        // Cloning the remote repository to a local directory
        String hashedAddress = connection.getAddressGit();

        String hashResult = connection.hash(connection.getAddressGit(), "SHA-512");

        if (hashResult != null) {
            hashedAddress = hashResult;
        }

        connection.setLocalDirectory(new File(connection.getPathToLocalRepos() + File.separator +  hashedAddress));

        CredentialsProvider credentials = null;

        if (username != null && password != null) {
            credentials = new UsernamePasswordCredentialsProvider(username, password);
        }

        if (!connection.getLocalDirectory().exists()) {

            connection.cloneRepository(credentials);

        } else {

            Git git = Git.open(connection.getLocalDirectory());

            PullCommand pull = git.pull();
            pull.setCredentialsProvider(credentials);

            try {
                pull.call();
            } catch (InvalidRemoteException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't pull the remote repository", e);
                }
                connection.cloneRepository(credentials);

            } catch (JGitInternalException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't pull the remote repository", e);
                }
                connection.cloneRepository(credentials);

            } catch (TransportException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't pull the remote repository: " + connection.getAddressGit(), e);
                }

                if (e.getMessage().endsWith("500 Internal Server Error")) {
                    throw new AuthenticationException("Can not pull the Git repository: auth failed");
                } else {
                    connection.cloneRepository(credentials);
                }
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't pull the remote repository", e);
                }
                connection.cloneRepository(credentials);
            }

            if (log.isDebugEnabled()) {
                log.debug("Pulled repository " + connection.getAddressGit());
            }
        }


        if (log.isDebugEnabled()) {
            log.debug("Connection to local repository");
        }

        // Connection to the local repository
        File gitFile = new File(connection.getLocalDirectory().getAbsolutePath() + File.separator + ".git");
        FileRepositoryBuilder gitRepoBuilder = new FileRepositoryBuilder();
        gitRepoBuilder.setGitDir(gitFile);
        connection.setGitRepo(gitRepoBuilder.build());

        if (!connection.getGitRepo().getObjectDatabase().exists()) {

            if (log.isErrorEnabled()) {
                log.error("The repository at address " + connection.getAddressGit() + " doesn't exist");
                throw new RepositoryNotFoundException("The repository at address " + connection.getAddressGit() + " doesn't exist");
            }
        }
    }
}
