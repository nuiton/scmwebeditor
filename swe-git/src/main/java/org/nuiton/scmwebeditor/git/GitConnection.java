/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.git;

import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.nuiton.scmwebeditor.api.RepositoryNotFoundException;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmRevision;
import org.nuiton.scmwebeditor.api.dto.BrowseDto;
import org.nuiton.scmwebeditor.api.dto.CommitDto;
import org.nuiton.scmwebeditor.api.dto.result.BrowseResultDto;
import org.nuiton.scmwebeditor.api.dto.result.CommitResultDto;
import org.nuiton.scmwebeditor.api.dto.result.RemoveFileResultDto;

import javax.naming.AuthenticationException;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementation of the Git's main features
 */
public class GitConnection implements ScmConnection {

    private static final Log log = LogFactory.getLog(GitConnection.class);

    /** git existing repository */
    protected Repository gitRepo;

    /** the local directory where the repository has been cloned to */
    protected File localDirectory;

    /** path to the directory of the file to edit */
    protected String addressGit;

    /** the name of the file to edit */
    protected String fileName;

    /** the path to the directory which contains all the repositories */
    protected String pathToLocalRepos;

    /** the default branch to use when the requested one was not found */
    protected static final String DEFAULT_BRANCH = "master";

    protected final String REPOSITORY_EXTENSION = ".git";


    public Repository getGitRepo() { return gitRepo; }

    public void setGitRepo(Repository gitRepo) { this.gitRepo = gitRepo; }

    public File getLocalDirectory() { return localDirectory; }

    public void setLocalDirectory(File localDirectory) { this.localDirectory = localDirectory; }

    public String getAddressGit() { return addressGit; }

    public String getPathToLocalRepos() { return pathToLocalRepos; }

    public void setPathToLocalRepos(String pathToLocalRepos) { this.pathToLocalRepos = pathToLocalRepos; }

    /**
     * Creates a new connection to a Git repository
     * @param address the address of the Git repository to connect to
     * @param pathToLocalRepos the path to the local folder which will store the user's repositories
     * @throws IOException if the repository can not be reached
     */
    public GitConnection(String address, String pathToLocalRepos) throws IOException {

        if(log.isDebugEnabled()) {
            log.debug("Git repository");
        }

        if (address.equals(REPOSITORY_EXTENSION)) {
            throw new IOException("Can not reach Git repository");
        } else if (address.contains(REPOSITORY_EXTENSION)) {
            addressGit = address.substring(0, address.indexOf(REPOSITORY_EXTENSION) + 4);
            fileName = address.substring(address.indexOf(REPOSITORY_EXTENSION) + 4);
        } else if (address.startsWith("git://")) {
            String lastPart = address.substring(address.lastIndexOf('/') + 1);

            if (lastPart.contains(".")) {
                // if the path is for a file
                addressGit = address.replace(lastPart, "");
                fileName = address.substring(address.lastIndexOf('/') + 1);
            } else {
                // if the path is for a directory
                addressGit = address;
                fileName = "";
            }
        } else {
            addressGit = address;
            fileName = address.substring(address.lastIndexOf('/') + 1);
        }

        this.pathToLocalRepos = pathToLocalRepos;
    }


    @Override
    public BrowseResultDto browse(BrowseDto dto) {

        BrowseResultDto resultDto = new BrowseResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(BrowseResultDto.ERROR);
            return resultDto;

        }

        String url;

        String headBranchName;
        try {
            headBranchName = gitRepo.getBranch();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while getting the head branch name", e);
            }
            resultDto.setError(BrowseResultDto.ERROR);
            return resultDto;
        }
        resultDto.setHeadBranchName(headBranchName);

        resultDto.setFiles(new LinkedList<String>());
        resultDto.setDirectories(new HashMap<String, String>());

        ObjectId commitId;

        // getting the requested branch, or the last commit if none was requested
        try {
            commitId = gitRepo.resolve(Constants.HEAD);

            String selectedBranch = dto.getSelectedBranch();

            if (selectedBranch != null) {
                if (!selectedBranch.equals("")) {

                    changeBranch(selectedBranch);
                    commitId = gitRepo.resolve(selectedBranch);

                    // if the given branch was not found, we use the master branch instead
                    if (commitId == null) {

                        if (log.isDebugEnabled()) {
                            log.debug("Branch " + selectedBranch + " was not found, using " + DEFAULT_BRANCH + " instead");
                        }

                        resultDto.setSelectedBranch(DEFAULT_BRANCH);
                        changeBranch(DEFAULT_BRANCH);
                        commitId = gitRepo.resolve(DEFAULT_BRANCH);
                    }
                }
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't access to the repository", e);
            }
            resultDto.setError(BrowseResultDto.ERROR);
            return resultDto;
        }

        String id = dto.getId();
        String address = dto.getAddress();

        // if the id parameter is not given, we look for the root
        if (id.equals("")) {

            resultDto.setError(BrowseResultDto.ROOT);
            return resultDto;

        } else if (id.equals("0")) {
            url = address;
        } else {
            url = id;
        }

        // if the id parameter is given, we look for the specified directory

        try {

            if (!gitRepo.getObjectDatabase().exists()) {

                resultDto.setError(BrowseResultDto.ERROR);
                return resultDto;
            }

            RevCommit commit;
            RevWalk revWalk = new RevWalk(gitRepo);

            if (commitId != null) {
                commit = revWalk.parseCommit(commitId);
            } else {

                if (log.isDebugEnabled()) {
                    log.debug("Can not access to repository " + url);
                }
                resultDto.setError(BrowseResultDto.ERROR);
                return resultDto;
            }
            RevTree tree = commit.getTree();

            // making a list of the repository's files and directories
            TreeWalk treeWalk;

            treeWalk = new TreeWalk(gitRepo);
            treeWalk.addTree(tree);
            treeWalk.setRecursive(false);

            // the directories we have to open to find the requested url
            String path = url.substring(url.indexOf(REPOSITORY_EXTENSION) + 4);
            ArrayList<String> dirs = Lists.newArrayList(path.split("/"));

            while (treeWalk.next()) {

                String currentFileName = address + File.separator + treeWalk.getPathString();

                String fileNameComplete = currentFileName + File.separator;
                String urlComplete = url + File.separator;

                if (fileNameComplete.startsWith(urlComplete) || urlComplete.startsWith(fileNameComplete)) {

                    // subtree = directory
                    if (treeWalk.isSubtree()) {

                        String pathString = treeWalk.getPathString();
                        String currentDir = pathString.substring(pathString.lastIndexOf(File.separator) + 1);

                        if (dirs.contains(currentDir)) {
                            treeWalk.enterSubtree();
                        } else {
                            resultDto.getDirectories().put(currentFileName, currentFileName);
                        }
                    } else {
                        resultDto.getFiles().add(currentFileName);
                    }
                }
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't access to the repository", e);
            }
            resultDto.setError(BrowseResultDto.ERROR);
            return resultDto;
        }

        resultDto.setError(null);

        return resultDto;
    }


    @Override
    public CommitResultDto commit(CommitDto dto) {

        CommitResultDto resultDto = new CommitResultDto();

        // getting the last version of the repository
        try {
            updateRepository(dto.getUsername(), dto.getPassword());
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            resultDto.setError(CommitResultDto.ERROR);
            return resultDto;

        }

        if (log.isDebugEnabled()) {
            log.debug("Entering Git commit");
        }

        File localFile = new File(localDirectory.getAbsolutePath() + File.separator + fileName);

        resultDto.setLastText(dto.getNewText());

        try {
            String originalText = FileUtils.readFileToString(localFile);

            resultDto.setOrigText(originalText);

        } catch (FileNotFoundException e) {
            log.error("Can not find the local file", e);

            resultDto.setError(CommitResultDto.ERROR);
            return resultDto;
        } catch (IOException e) {
            log.error("Can not open the local file", e);

            resultDto.setError(CommitResultDto.ERROR);
            return resultDto;
        }

        // authentication
        if (dto.getUsername() == null || dto.getPassword() == null) {
            resultDto.setError(CommitResultDto.AUTH_ERROR);
            return resultDto;
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        // applying the changes on the local file
        try {
            FileUtils.writeStringToFile(localFile, dto.getNewText());
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not modify the local file", e);
            }

            resultDto.setError(CommitResultDto.ERROR);
            return resultDto;
        }

        try {
            Git git = Git.open(localDirectory);

            // commit
            try {
                doCommit(git, dto.getUsername(), "unknown", "From scmwebeditor -- " + dto.getCommitMessage());
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not commit", e);
                }

                resultDto.setError(CommitResultDto.ERROR);
                return resultDto;
            }

            // push
            if (log.isDebugEnabled()) {
                log.debug("Preparing push");
            }

            if (!dto.isCommitOnly()) {
                PushCommand push = git.push();
                push.setRemote(addressGit);
                push.setCredentialsProvider(credentials);

                try {
                    push.call();
                } catch (GitAPIException e) {

                    handlePushException(e);

                    if (e.getMessage().endsWith("not authorized")) {
                        resultDto.setError(RemoveFileResultDto.AUTH_ERROR);
                    } else {
                        resultDto.setError(RemoveFileResultDto.ERROR);
                    }

                    return resultDto;
                }
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + localDirectory.getAbsolutePath(), e);

                resultDto.setError(CommitResultDto.ERROR);
                return resultDto;
            }
        }

        try {
            resultDto.setNumRevision(getHeadRevisionNumber(dto.getAddress(), dto.getUsername(), dto.getPassword()));
        } catch (AuthenticationException e) {
            if (log.isErrorEnabled()) {
                log.error("Auth fail", e);
            }
        }

        return resultDto;
    }

    @Override
    public File getFileContent(String path, String username, String password) throws AuthenticationException {

        // getting the last version of the repository
        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
        }

        File fileToEdit = new File(localDirectory.getAbsolutePath() + File.separator + fileName);

        if (!fileToEdit.exists()) {
            throw new IllegalArgumentException("There is no entry at '" + path + "'.");
        }

        return fileToEdit;
    }

    @Override
    public String getHeadRevisionNumber(String path, String username, String password) throws AuthenticationException {

        // getting the last version of the repository
        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
        }

        String headRevision = null;

        try {
            ObjectId commitId = gitRepo.resolve(Constants.HEAD);
            headRevision = commitId.getName();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not resolve commit " + Constants.HEAD, e);
            }
        }

        return headRevision;
    }

    @Override
    public String getRepositoryId() {
        return addressGit;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFilePath(String address, String repositoryRoot, String username, String password) {

        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            return null;
        }

        String path = localDirectory.getAbsolutePath() + address.replace(repositoryRoot, "");
        path = path.replaceAll("/", File.separator);

        return path;
    }

    @Override
    public Map<ScmRevision, String> getRevisions(String address, String username, String password) {

        final int MAX_MESSAGE_LENGTH = 64;
        String pathOnRepo = address.replace(addressGit + "/", "");
        Map<ScmRevision, String> revisions = new TreeMap<ScmRevision, String>();

        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }
            return null;

        }

        try {
            // getting the revisions
            Git git = Git.open(localDirectory);

            LogCommand logCmd = git.log();
            logCmd.addPath(pathOnRepo);

            try {
                Iterable<RevCommit> commits = logCmd.call();

                // making the value and the key to put in the Map
                for (RevCommit commit : commits) {

                    // getting the commit message
                    String msg = commit.getShortMessage();

                    if (msg.length() > MAX_MESSAGE_LENGTH) {
                        msg = msg.substring(0, MAX_MESSAGE_LENGTH) + "...";
                    }

                    // getting the date
                    long commitTime = ((long) commit.getCommitTime()) * 1000;

                    Date commitDate = new Date(commitTime);

                    DateFormat displayDf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    String formattedDate = displayDf.format(commitDate);

                    msg = formattedDate + " - " + msg;

                    // adding the revision to the Map
                    ScmRevision scmRev = new ScmRevision(commit.getName(), commitTime);
                    revisions.put(scmRev, msg);
                }

            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not get revisions for address " + address, e);
                }
                return null;
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open git local repository : " + localDirectory.getAbsolutePath(), e);
            }
            return null;
        }

        return revisions;
    }

    @Override
    public File getFileContentAtRevision(String path, String username, String password,
                                         String revision) throws AuthenticationException {

        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

        }

        String pathOnRepo = path.replace(addressGit + "/", "");

        String tempFileName = localDirectory.getAbsolutePath() + File.separator + pathOnRepo + "_" + revision;
        File tempFile = new File(tempFileName);

        ObjectId commitId;

        try {

            commitId = gitRepo.resolve(revision);

            // a RevWalk allows to walk over commits based on some filtering that is defined
            RevWalk revWalk = new RevWalk(gitRepo);
            RevCommit commit = revWalk.parseCommit(commitId);
            // and using commit's tree find the path
            RevTree tree = commit.getTree();

            // now try to find a specific file
            TreeWalk treeWalk = new TreeWalk(gitRepo);
            treeWalk.addTree(tree);
            treeWalk.setRecursive(true);
            treeWalk.setFilter(PathFilter.create(pathOnRepo));
            if (!treeWalk.next()) {
                throw new IllegalStateException("Did not find expected file '" + pathOnRepo + "'");
            }

            ObjectId objectId = treeWalk.getObjectId(0);
            ObjectLoader loader = gitRepo.open(objectId);

            // and then one can the loader to read the file
            FileOutputStream fos = new FileOutputStream(tempFile);
            loader.copyTo(fos);

            revWalk.dispose();

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while getting file '" + pathOnRepo + "' content at revision " + revision, e);
            }
        }

        return tempFile;
    }

    @Override
    public File getDiffs(String path, String username, String password, String revision1,
                         String revision2) throws AuthenticationException {

        try {
            updateRepository(username, password);
        } catch (RepositoryNotFoundException e) {

            if (log.isErrorEnabled()) {
                log.error("Error while cloning or pulling the repository", e);
            }

        }

        String pathOnRepo = path.replace(addressGit + "/", "");

        String tempFileName = localDirectory.getAbsolutePath() + File.separator + pathOnRepo
                + "_" + revision1 + "_" + revision2;
        File tempFile = new File(tempFileName);

        ObjectId commitId1, commitId2;

        try {

            commitId1 = gitRepo.resolve(revision1 + "^{tree}");
            commitId2 = gitRepo.resolve(revision2 + "^{tree}");

            Git git = Git.open(localDirectory);

            ObjectReader reader = gitRepo.newObjectReader();

            CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
            oldTreeIter.reset(reader, commitId2);
            CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
            newTreeIter.reset(reader, commitId1);

            DiffCommand diffCommand = git.diff();
            PathFilter pathFilter = PathFilter.create(pathOnRepo);
            diffCommand.setPathFilter(pathFilter);
            diffCommand.setNewTree(newTreeIter);
            diffCommand.setOldTree(oldTreeIter);

            List<DiffEntry> diffs = diffCommand.call();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DiffFormatter df = new DiffFormatter(baos);
            df.setRepository(gitRepo);

            for(DiffEntry diff : diffs) {
                df.format(diff);
                String diffText = baos.toString("UTF-8");
                FileUtils.writeStringToFile(tempFile, diffText);
                baos.reset();
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while getting file '" + pathOnRepo + "' content at revisions " + revision1
                        + " and " + revision2, e);
            }
        } catch (GitAPIException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not get revisions for address " + path, e);
            }
        }

        return tempFile;
    }


    /**
     * Changing for another branch
     * @param branchName the new branch's name
     * @throws IOException if reaching the repository is not possible
     */
    public void changeBranch(String branchName) throws IOException {

        if (branchName == null) {
            branchName = DEFAULT_BRANCH;
        } else if (branchName.equals("")) {
            branchName = DEFAULT_BRANCH;
        }

        Git git = Git.open(localDirectory);

        CheckoutCommand checkout = git.checkout();
        checkout.setCreateBranch(true);
        checkout.setName(branchName);
        checkout.setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK);
        checkout.setStartPoint("origin/" + branchName);

        try {
            checkout.call();
        } catch (GitAPIException e) {

            // if we could not create a new local branch, it may be because it already exists
            checkout = git.checkout();
            checkout.setName(branchName);

            try {
                checkout.call();
            } catch (GitAPIException e1) {
                if (log.isErrorEnabled()) {
                    log.error("Can not checkout branch " + branchName, e1);
                }
                throw new IOException("Can not checkout branch " + branchName, e1);
            }
        }
    }


    /**
     * Updates the repository to the last version by a clone or a pull command
     * @param username the username to use to connect to the repository
     * @param password the password to use to connect to the repository
     * @throws RepositoryNotFoundException if the repository is not found
     */
    public void updateRepository(String username, String password) throws RepositoryNotFoundException {

        // Cloning the remote repository to a local directory
        String hashedAddress = addressGit;

        String hashResult = hash(addressGit, "SHA-512");

        if (hashResult != null) {
            hashedAddress = hashResult;
        }

        localDirectory = new File(pathToLocalRepos + File.separator +  hashedAddress);

        CredentialsProvider credentials = null;

        if (username != null && password != null) {
            credentials = new UsernamePasswordCredentialsProvider(username, password);
        }

        try {
            if (!localDirectory.exists()) {

                cloneRepository(credentials);

            } else {

                Git git = Git.open(localDirectory);

                PullCommand pull = git.pull();
                pull.setCredentialsProvider(credentials);

                try {
                    pull.call();
                } catch (InvalidRemoteException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't pull the remote repository", e);
                    }
                    cloneRepository(credentials);

                } catch (JGitInternalException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't pull the remote repository", e);
                    }
                    cloneRepository(credentials);

                } catch (TransportException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't pull the remote repository: " + addressGit, e);
                    }

                    if (e.getMessage().endsWith("500 Internal Server Error")) {
                        throw new AuthenticationException("Can not pull the Git repository: auth failed");
                    } else {
                        cloneRepository(credentials);
                    }
                } catch (GitAPIException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't pull the remote repository", e);
                    }
                    cloneRepository(credentials);
                }

                if (log.isDebugEnabled()) {
                    log.debug("Pulled repository " + addressGit);
                }
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not clone repository " + addressGit);
            }
        } catch (AuthenticationException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not clone repository " + addressGit + ": authentication error");
            }
        }


        if (log.isDebugEnabled()) {
            log.debug("Connection to local repository");
        }

        // Connection to the local repository
        File gitFile = new File(localDirectory.getAbsolutePath() + File.separator + REPOSITORY_EXTENSION);
        FileRepositoryBuilder gitRepoBuilder = new FileRepositoryBuilder();
        gitRepoBuilder.setGitDir(gitFile);

        try {
            gitRepo = gitRepoBuilder.build();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not get repository at address " + addressGit);
            }
        }

        if (!gitRepo.getObjectDatabase().exists()) {

            if (log.isErrorEnabled()) {
                log.error("The repository at address " + addressGit + " doesn't exist");
                throw new RepositoryNotFoundException("The repository at address " + addressGit + " doesn't exist");
            }
        }
    }


    /**
     * Clones the repository (clone command)
     * @param credentials the username and password to use to connect to the repository
     * @throws RepositoryNotFoundException if the repository is not found
     * @throws IOException if it is not possible to reach the repository
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    protected void cloneRepository(CredentialsProvider credentials)
            throws IOException, AuthenticationException, RepositoryNotFoundException {

        if (localDirectory.exists()) {
            FileUtils.deleteDirectory(localDirectory);
        }

        CloneCommand clone = Git.cloneRepository();
        clone.setURI(addressGit);
        clone.setDirectory(localDirectory);
        clone.setCredentialsProvider(credentials);

        try {
            clone.call();
        } catch (InvalidRemoteException e) {
            FileUtils.deleteDirectory(localDirectory);
            if (log.isErrorEnabled()) {
                log.error("Can't clone the remote repository", e);
            }
        } catch (TransportException e) {
            FileUtils.deleteDirectory(localDirectory);
            if (log.isErrorEnabled()) {
                log.error("Can't clone the remote repository: " + addressGit, e);
            }

            if (e.getMessage().endsWith("500 Internal Server Error")
                    || e.getMessage().endsWith("no CredentialsProvider has been registered")) {
                throw new AuthenticationException("Can not clone the Git repository: auth failed");
            } else {
                throw new RepositoryNotFoundException("Can not find a Git repository at address " + addressGit, e);
            }
        } catch (GitAPIException e) {
            FileUtils.deleteDirectory(localDirectory);
            if (log.isErrorEnabled()) {
                log.error("Can't clone the remote repository", e);
            }
            throw new RepositoryNotFoundException("Can not find a Git repository at address " + addressGit, e);
        }

        if (log.isDebugEnabled()) {
            log.debug("Cloned repository " + addressGit);
        }
    }


    /**
     * Hashes a String with then given algorithms
     * @param toHash the String to hash
     * @param algorithm the algorithm to use to hash the String
     * @return the hashed String
     */
    protected String hash(String toHash, String algorithm) {

        String hashed = null;

        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            try {
                md.update(toHash.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not get bytes from UTF-8 encoding");
                }
            }
            byte byteData[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte aByteData : byteData) {
                String hexByte = Integer.toString((aByteData & 0xff) + 0x100, 16);
                hexByte = hexByte.substring(1);
                sb.append(hexByte);
            }

            hashed = sb.toString();
            if (log.isDebugEnabled()) {
                log.debug("hashed " + toHash + " : " + hashed);
            }
        } catch (NoSuchAlgorithmException e) {
            log.error("Can not hash " + toHash + " : the algorithm " + algorithm + " does not exist", e);
        }

        return hashed;
    }


    /**
     * Calls the commit command
     * @param git the git repository which will receive the commit
     * @param authorName the name of the commit's author
     * @param authorEmail the e-mail address of the author
     * @param commitMessage the message that describes the commit
     * @throws GitAPIException if there is a problem during the commit process
     */
    protected void doCommit(Git git, String authorName, String authorEmail, String commitMessage) throws GitAPIException {

        if (log.isDebugEnabled()) {
            log.debug("Preparing commit");
        }

        CommitCommand commit = git.commit();
        commit.setAll(true);
        commit.setAuthor(authorName, authorEmail);
        commit.setMessage(commitMessage);

        commit.call();
    }

    /**
     * Handles the exception thrown by a push command
     * @param e the exception thrown by a push command
     */
    protected void handlePushException(GitAPIException e) {
        String logMessage = "Can not push";

        if (e instanceof NoHeadException) {
            logMessage = "Can not push : the Git repository has no HEAD reference";
        } else if (e instanceof UnmergedPathsException) {
            logMessage = "Can not push : conflicts found (unmerged paths)";
        } else if (e instanceof ConcurrentRefUpdateException) {
            logMessage = "Can not push : someone else is updating the HEAD or the branch";
        } else if (e instanceof WrongRepositoryStateException) {
            logMessage = "Can not push : the repository is not in the right state";
        }
        //TC-2015-12-22 does not exist any longer
//        else if (e instanceof RejectCommitException) {
//            logMessage = "Can not push : commit rejected";
//        }

        if (log.isErrorEnabled()) {
            log.error(logMessage, e);
        }
    }
}
