/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.git;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.nuiton.scmwebeditor.api.*;
import org.nuiton.scmwebeditor.api.dto.CreateBranchDto;
import org.nuiton.scmwebeditor.api.dto.result.AbstractResultDto;

import javax.naming.AuthenticationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of the Git's specific features
 */
public class GitProvider implements ScmProvider {

    private static final Log log = LogFactory.getLog(GitProvider.class);

    /** the repository's address */
    protected String address;


    @Override
    public boolean supportsBranches() {
        return true;
    }

    @Override
    public List<String> listBranches(String address, String username, String password) throws OperationNotSupportedException {

        if (username == null) {
            username = "anonymous";
        }
        if (password == null) {
            password = "anonymous";
        }

        List<String> branches = new ArrayList<String>();

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(username, password);

        LsRemoteCommand lsRemote = new LsRemoteCommand(null);
        lsRemote.setRemote(address);
        lsRemote.setTags(false);
        lsRemote.setHeads(true);
        lsRemote.setCredentialsProvider(credentials);

        try {
            Collection<Ref> lsRemoteResult = lsRemote.call();
            branches = new ArrayList<String>();

            for (Ref branch : lsRemoteResult) {

                // we only take the name of the branch, not "refs/heads/"
                String name = branch.getName();
                name = name.substring(name.indexOf("/") + 1);
                name = name.substring(name.indexOf("/") + 1);

                branches.add(name);
            }
        } catch (GitAPIException e) {
            if (log.isErrorEnabled()) {
                log.error("The repository at address " + address + " could not be reached", e);
            }
        }

        return branches;
    }

    @Override
    public String createBranch(CreateBranchDto dto) throws OperationNotSupportedException,
            AuthenticationException, RepositoryNotFoundException {

        String error = null;
        GitConnection conn = (GitConnection) getConnection(address, dto.getPathToLocalRepos());

        // authentication
        if (dto.getUsername() == null) {
            dto.setUsername("anonymous");
        }
        if (dto.getPassword() == null) {
            dto.setPassword("anonymous");
        }

        CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(dto.getUsername(), dto.getPassword());

        try {

            // setting the local repository in the right state
            conn.updateRepository(dto.getUsername(), dto.getPassword());
            File localDirectory = conn.getLocalDirectory();
            conn.changeBranch(dto.getSelectedBranch());

            // getting the reference to the origin branch
            ObjectId id = conn.getGitRepo().resolve(dto.getSelectedBranch());
            RevWalk revWalk = new RevWalk(conn.getGitRepo());
            RevCommit revCommit = revWalk.parseCommit(id);

            Git git = Git.open(localDirectory);

            // creating the local branch
            CreateBranchCommand createBranch = git.branchCreate();
            createBranch.setName(dto.getNewBranchName());
            createBranch.setStartPoint(revCommit);

            try {
                createBranch.call();
            } catch (GitAPIException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not create local branch " + dto.getNewBranchName() + " from " + dto.getSelectedBranch(), e);
                    error = AbstractResultDto.ERROR;
                }
            }

            conn.changeBranch(dto.getNewBranchName());

            // push
            PushCommand push = git.push();
            push.setRemote(address);
            push.setCredentialsProvider(credentials);

            try {
                push.call();
            } catch (GitAPIException e) {

                String logMessage = "Can not push";

                if (e instanceof NoHeadException) {
                    logMessage = "Can not push : the Git repository has no HEAD reference";
                } else if (e instanceof UnmergedPathsException) {
                    logMessage = "Can not push : conflicts found (unmerged paths)";
                } else if (e instanceof ConcurrentRefUpdateException) {
                    logMessage = "Can not push : someone else is updating the HEAD or the branch";
                } else if (e instanceof WrongRepositoryStateException) {
                    logMessage = "Can not push : the repository is not in the right state";
                }
                //TC-2015-12-22 does not exist any longer
//                else if (e instanceof RejectCommitException) {
//                    logMessage = "Can not push : commit rejected";
//                }

                if (log.isErrorEnabled()) {
                    log.error(logMessage, e);
                }

                // if the branch could not be pushed, we delete it to avoid errors
                DeleteBranchCommand deleteBranch = git.branchDelete();
                deleteBranch.setBranchNames(dto.getNewBranchName());
                deleteBranch.setForce(true);

                try {
                    deleteBranch.call();
                } catch (GitAPIException e1) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not delete the new local branch", e1);
                    }
                }

                if (e.getMessage().endsWith("not authorized")) {
                    throw new AuthenticationException("Authentication error");
                } else {
                    throw new RepositoryNotFoundException("Can not reach the remote repository");
                }
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not open local Git repository", e);
            }
            error = AbstractResultDto.ERROR;
        }

        return error;
    }

    @Override
    public String changeBranch(String branchName, String pathToLocalRepos, String username, String password)
            throws OperationNotSupportedException {

        String error = null;
        GitConnection conn = (GitConnection) getConnection(address, pathToLocalRepos);

        try {
            conn.updateRepository(username, password);

            try {
                conn.changeBranch(branchName);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not change branch to " + branchName, e);
                }
                error = AbstractResultDto.ERROR;
            }
        } catch (RepositoryNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not reach repository " + address, e);
            }
            error = AbstractResultDto.ERROR;
        }

        return error;
    }

    @Override
    public boolean supportsPush() {
        return true;
    }

    @Override
    public boolean filesDirectlyAccessible() {
        return false;
    }

    @Override
    public ScmConnection getConnection(String address, String pathToLocalRepos) {

        GitConnection gitConn = null;
        this.address = address;

        try {
            gitConn = new GitConnection(address, pathToLocalRepos);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not connect to Git repository at " + address);
            }
        }

        return gitConn;
    }

    @Override
    public ScmFileManager getFileManager(ScmConnection connection) {

        GitFileManager fileManager = null;

        if (connection instanceof GitConnection) {
            try {
                fileManager = new GitFileManager((GitConnection) connection);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not connect to Git repository", e);
                }
            }
        } else {
            throw new SweInternalException("Can not get Git file manager for a non-Git connection");
        }

        return fileManager;
    }

    @Override
    public boolean addressSeemsCompatible(String address) {

        if (address.contains(".git") || address.startsWith("git://") || address.contains("git.")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getDefaultBranchName() throws OperationNotSupportedException {
        return "master";
    }
}
