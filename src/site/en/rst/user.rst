.. -
.. * #%L
.. * ScmWebEditor
.. * %%
.. * Copyright (C) 2009 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

User documentation
==================

You will find here all the documentation available for ScmWebEditor.

You can find an `installation guide`_ and a `tutorial`_ to understand how to use ScmWebEditor.

.. _installation guide:install.html
.. _tutorial:tutoriel.html
