/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api.dto;

public class MoveFileDto {

    /** the username used to connect to the SCM */
    protected String username;

    /** the password used to connect to the SCM */
    protected String password;

    /** path to the repository */
    protected String scmPath;

    /** path to the file to move */
    protected String fileToMove;

    /** path to the destination directory */
    protected String destinationDirectory;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScmPath() { return scmPath; }

    public void setScmPath(String scmPath) { this.scmPath = scmPath; }

    public String getFileToMove() { return fileToMove; }

    public void setFileToMove(String fileToMove) { this.fileToMove = fileToMove; }

    public String getDestinationDirectory() { return destinationDirectory; }

    public void setDestinationDirectory(
            String destinationDirectory) { this.destinationDirectory = destinationDirectory; }
}
