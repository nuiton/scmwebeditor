package org.nuiton.scmwebeditor.api.dto.result;

/*
 * #%L
 * ScmWebEditor SCM API
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BrowseResultDto extends AbstractResultDto {

    public static final String ROOT = "root";


    /** the name of the head branch on the SCM */
    protected String headBranchName;

    /** the list of files found on the SCM */
    protected List<String> files;

    /** the list of directories found on the SCM */
    protected Map<String, String> directories;

    /** the name of the selected branch on the SCM */
    protected String selectedBranch;


    public BrowseResultDto() {
        files = new LinkedList<String>();
        directories = new HashMap<String, String>();
    }

    public String getHeadBranchName() {
        return headBranchName;
    }

    public void setHeadBranchName(String headBranchName) {
        this.headBranchName = headBranchName;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public Map<String, String> getDirectories() {
        return directories;
    }

    public void setDirectories(Map<String, String> directories) {
        this.directories = directories;
    }

    public String getSelectedBranch() {
        return selectedBranch;
    }

    public void setSelectedBranch(String selectedBranch) {
        this.selectedBranch = selectedBranch;
    }
}
