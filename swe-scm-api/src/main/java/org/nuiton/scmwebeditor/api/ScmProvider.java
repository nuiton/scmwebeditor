/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api;

import org.nuiton.scmwebeditor.api.dto.CreateBranchDto;

import javax.naming.AuthenticationException;
import java.util.List;

/**
 * An interface which allows to know and to use the specific features of each SCM
 */
public interface ScmProvider {

    /**
     * Tells whether the SCM supports branches
     * @return true if the SCM supports branches
     */
    boolean supportsBranches();

    /**
     * Makes a list of the repository's branches
     * @param address the repository's address
     * @param username the username used to connect to the SCM
     * @param password the password used to connect to the SCM
     * @return a list of the repository's branches
     * @throws OperationNotSupportedException if the SCM doesn't support branches
     */
    List<String> listBranches(String address, String username, String password) throws OperationNotSupportedException;

    /**
     * Allows to create a new branch on the repository
     * @param dto the DTO which contains all the parameters
     * @return an error code or null if there was no error during the process
     * @throws OperationNotSupportedException if the SCM doesn't support branches
     * @throws AuthenticationException if there is an authentication problem
     * @throws RepositoryNotFoundException if the repository could not be reached
     */
    String createBranch(CreateBranchDto dto) throws OperationNotSupportedException,
            AuthenticationException, RepositoryNotFoundException;

    /**
     * Changes the working branch for the local repository
     * @param branchName the new working branch
     * @param pathToLocalRepos the path to the directory where the local repositories are stored
     * @param username the username to use to connect to the repository
     * @param password the password to use to connect to the repository
     * @return an error code or null if there was no error during the process
     * @throws OperationNotSupportedException if the SCM doesn't support branches
     */
    String changeBranch(String branchName, String pathToLocalRepos, String username, String password)
            throws OperationNotSupportedException;

    /**
     * Tells whether the SCM allows to choose when the commits are pushed to the server
     * @return true if the SCM supports a command to push the commits to the server
     */
    boolean supportsPush();

    /**
     * Tells whether the repositories' files are directly accessible by their address
     * @return true if the files are directly accessible
     */
    boolean filesDirectlyAccessible();

    /**
     * Gives the connection to the SCM
     * @param address the repository's address
     * @param pathToLocalRepos the folder which will be used to store one client's repositories
     * @return the connection to the SCM
     */
    ScmConnection getConnection(String address, String pathToLocalRepos);

    /**
     * Gives the file manager for the given connection
     * @param connection the connection to the repository
     * @return the file manager for the repository
     */
    ScmFileManager getFileManager(ScmConnection connection);

    /**
     * Tells whether the given address seems compatible with the SCM
     * @param address the repository's address
     * @return true if the repository seems to be compatible with the SCM
     */
    boolean addressSeemsCompatible(String address);

    /**
     * Gives the name of the default branch if the SCM supports branches
     * @return the name of the default branch to use
     * @throws OperationNotSupportedException if the SCM doesn't support branches
     */
    String getDefaultBranchName() throws OperationNotSupportedException;
}
