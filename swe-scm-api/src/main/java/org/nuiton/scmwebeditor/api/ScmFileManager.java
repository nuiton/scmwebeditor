/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api;

import org.nuiton.scmwebeditor.api.dto.*;
import org.nuiton.scmwebeditor.api.dto.result.*;

/**
 * An interface which gives the SCMs features relative to file management
 */
public interface ScmFileManager {

    /**
     * Uploads a file to the repository as a new file
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    UploadFileResultDto uploadFile(UploadFileDto dto);


    /**
     * Removes a file from the repository
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    RemoveFileResultDto removeFile(RemoveFileDto dto);


    /**
     * Creates a new directory on the repository
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    CreateDirectoryResultDto createDirectory(CreateDirectoryDto dto);


    /**
     * Removes a directory from the repository
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    RemoveDirectoryResultDto removeDirectory(RemoveDirectoryDto dto);


    /**
     * Moves a file to a directory in the repository
     * @param dto the DTO which contains all the parameters
     * @return a DTo which contains all the results
     */
    MoveFileResultDto moveFile(MoveFileDto dto);
}
