package org.nuiton.scmwebeditor.api;

/*
 * #%L
 * ScmWebEditor SCM API
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class ScmRevision implements Comparable {

    /** the revision identifier (or revision number) */
    protected String revisionId;

    /** the commit time for this revision, in milliseconds */
    protected long commitTime;


    public String getRevisionId() { return revisionId; }

    public void setRevisionId(String revisionId) { this.revisionId = revisionId; }

    public long getCommitTime() { return commitTime; }

    public void setCommitTime(long commitTime) { this.commitTime = commitTime; }


    public ScmRevision(String revisionId, long commitTime) {
        this.revisionId = revisionId;
        this.commitTime = commitTime;
    }

    @Override
    public String toString() {
        return revisionId;
    }

    @Override
    public int compareTo(Object o) {

        Long thisCommitTime = commitTime;
        Long otherCommitTime = ((ScmRevision) o).getCommitTime();

        int returnVal = thisCommitTime.compareTo(otherCommitTime);

        returnVal *= -1; // to get the revisions from the last one to the oldest one

        return returnVal;
    }
}
