/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api;

import org.nuiton.scmwebeditor.api.dto.BrowseDto;
import org.nuiton.scmwebeditor.api.dto.CommitDto;
import org.nuiton.scmwebeditor.api.dto.result.BrowseResultDto;
import org.nuiton.scmwebeditor.api.dto.result.CommitResultDto;

import javax.naming.AuthenticationException;
import java.io.File;
import java.util.Map;

/**
 * An interface which gives the SCMs main features
 */
public interface ScmConnection {

    /**
     * Searches the repository's files to make a list of them
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    BrowseResultDto browse(BrowseDto dto);


    /**
     * Makes a commit of the changed made to the edited file
     * @param dto the DTO which contains all the parameters
     * @return a DTO which contains all the results
     */
    CommitResultDto commit(CommitDto dto);


    /**
     * Gives the content of a file
     * @param path the path to the file to get the content from
     * @param username the user's login for the SCM
     * @param password the user's password for the SCM
     * @return a String which contains the file's content
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    File getFileContent(String path, String username, String password) throws AuthenticationException;


    /**
     * Gives the number of the head revision
     * @param path the path to the SCM
     * @param username the user's login for the SCM
     * @param password the user's password for the SCM
     * @return a String which contains the head revision's number
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    String getHeadRevisionNumber(String path, String username, String password) throws AuthenticationException;


    /**
     * Gives the repository's unique identifier
     * @return the repository's unique identifier
     */
    String getRepositoryId();


    /**
     * Gives the name of the edited file
     * @return the name of the edited file
     */
    String getFileName();


    /**
     * Gives the path to use to get a file from the repository
     * @param address the full address of the file on the repository
     * @param repositoryRoot the address of the repository's root
     * @param username the user's login for the SCM
     * @param password the user's password for the SCM
     * @return the path to use to get a file from the repository
     */
    String getFilePath(String address, String repositoryRoot, String username, String password);


    /**
     * Gives a list of the important revisions for the file at the given address
     * @param address the file's address
     * @param username the username to use to connect to the repository
     * @param password the password to use to connect to the repository
     * @return a list of revisions for the file at the given address with their revision name
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    Map<ScmRevision, String> getRevisions(String address, String username, String password) throws AuthenticationException;


    /**
     * Gives the content of a file at the specified revision
     * @param path the path to the file to get the content from
     * @param username the user's login for the SCM
     * @param password the user's password for the SCM
     * @param revision the revision ID to use to get the file content
     * @return a String which contains the file's content
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    File getFileContentAtRevision(String path, String username, String password, String revision)
            throws AuthenticationException;


    /**
     * Gives the content of a file at the specified revision
     * @param path the path to the file to get the content from
     * @param username the user's login for the SCM
     * @param password the user's password for the SCM
     * @param revision1 one of the revisions ID to compare
     * @param revision2 the other revision ID to compare
     * @return a String which contains the file's content
     * @throws AuthenticationException if there is a problem during the authentication process
     */
    File getDiffs(String path, String username, String password, String revision1,String revision2)
            throws AuthenticationException;
}
