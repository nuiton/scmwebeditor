/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api.dto.result;

public class RemoveFileResultDto extends AbstractResultDto {

    public static final String REDIRECT = "redirect";

    public static final String CONNECTION_FAILED = "connection failed";

    /** the root directory of the repository */
    protected String scmRoot;

    /** the full path of the file to remove */
    private String fileRoot;


    public String getScmRoot() {
        return scmRoot;
    }

    public void setScmRoot(String scmRoot) {
        this.scmRoot = scmRoot;
    }

    public String getFileRoot() {
        return fileRoot;
    }

    public void setFileRoot(String fileRoot) {
        this.fileRoot = fileRoot;
    }
}
