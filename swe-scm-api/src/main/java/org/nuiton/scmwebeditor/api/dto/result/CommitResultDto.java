package org.nuiton.scmwebeditor.api.dto.result;

/*
 * #%L
 * ScmWebEditor SCM API
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

public class CommitResultDto extends AbstractResultDto {

    public static final String ERROR_PATH = "error path";

    public static final String FILE_MODIFY = "file modify";

    /** the last read text */
    protected String lastText;

    /** the original text */
    protected String origText;

    /** the differences between the current version and the last one */
    protected String diff;

    /** the commiter of the head version */
    protected String headCommiter;

    /** the number of the revision */
    protected String numRevision;


    public String getLastText() {
        return lastText;
    }

    public void setLastText(String lastText) {
        this.lastText = lastText;
    }

    public String getOrigText() {
        return origText;
    }

    public void setOrigText(String origText) {
        this.origText = origText;
    }

    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }

    public String getHeadCommiter() {
        return headCommiter;
    }

    public void setHeadCommiter(String headCommiter) {
        this.headCommiter = headCommiter;
    }

    public String getNumRevision() {
        return numRevision;
    }

    public void setNumRevision(String numRevision) {
        this.numRevision = numRevision;
    }
}
