/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.api.dto;

public class CreateBranchDto {

    /** the repository's address */
    protected String address;

    /** the name of the branch to create */
    protected String newBranchName;

    /** the name of the origin branch for the new one */
    protected String selectedBranch;

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String password;

    /** the folder which is used to store the repositories */
    protected String pathToLocalRepos;


    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getNewBranchName() { return newBranchName; }

    public void setNewBranchName(String newBranchName) { this.newBranchName = newBranchName; }

    public String getSelectedBranch() { return selectedBranch; }

    public void setSelectedBranch(String selectedBranch) { this.selectedBranch = selectedBranch; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getPathToLocalRepos() { return pathToLocalRepos; }

    public void setPathToLocalRepos(String pathToLocalRepos) { this.pathToLocalRepos = pathToLocalRepos; }
}
