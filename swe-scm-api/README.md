To deploy new version of pom: mvn deploy
To install localy: mvn install

To make a new release, see

https://forge.nuiton.org/projects/pom/repository/revisions/develop/entry/nuitonpom/README.txt
