<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<script type="text/javascript">
  document.getElementById("historyIndicator").style.display = 'none';
</script>

<s:if test="error == null">

<pre id="historyContent">
<s:property value="fileContent"/>
</pre>

</s:if>
<s:elseif test="error.equals('login')">
  <s:text name="scm.badUsernameOrPassword"/>
</s:elseif>
<s:else>
  <s:text name="scm.repoError"/>
</s:else>
