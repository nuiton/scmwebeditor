<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjt" uri="/struts-jquery-tree-tags" %>
<sj:head jquerytheme="default"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title><s:text name="scm.titles.swe"/></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <script type="text/javascript" src="js/selectLanguage.js"></script>

  <script src="js/cancelRedirect.js" type="text/javascript"></script>
  <script src="js/popup.js" type="text/javascript"></script>

  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <script type="text/javascript" src="js/popin.js"></script>

  <script type="text/javascript">
    var svgAddress;

    // Opens the selected file in the tree
    $.subscribe('treeClicked', function(event, data) {
      var item = event.originalEvent.data.rslt.obj;

      if (item.length == 1) {
        var classAttr = item[0].getAttribute("class");

        if (classAttr.indexOf("jstree-leaf") != -1) {
          var fileExtension = item.attr("id").substr(item.attr("id").lastIndexOf(".") + 1);

          if (fileExtension.toLowerCase() === "svg") {
            svgAddress = item.attr("id");
            openPopin("svgPopin");
          } else {

            var scmType = document.getElementById("scmType").value;
            var actionFile = "edit.action";

            var supportedImages = ["jpg", "jpeg", "png", "gif"];

            if (supportedImages.indexOf(fileExtension.toLowerCase()) != -1) {
              actionFile = "viewImage.action";
            }

            document.location.href = (actionFile + "?address=" + item.attr("id") + "&scmType=" + scmType
                        + '&selectedBranch=<s:property value="selectedBranch"/>&repositoryRoot=<s:property value="repositoryRoot"/>');
          }
        }
      }
    });

    // automatically expand the directory when there is no other file
    $.subscribe('treeChanged', function(event, data) {

        var json = event.originalEvent.data.responseJSON;

        if (json.length == 1) {
            var object = json[0];

            if (object.state === "closed") {
                var htmlObject = document.getElementById(object.id);
                var children = htmlObject.children;
                children.item('ins').click();
            }
        }
    });

    /* Allows to open a SVG file in the selected mode */
    function openSvg(action) {
      var scmType = document.getElementById("scmType").value;

      document.location.href = (action + "?address=" + svgAddress + "&scmType=" + scmType
              + '&selectedBranch=<s:property value="selectedBranch"/>&repositoryRoot=<s:property value="repositoryRoot"/>');
    }
  </script>

</head>
<body id="viewImageBody">

<div id="wrapper">


<s:hidden name="scmType" id="scmType" value="%{scmType}"/>

<div id="head">


  <div id="flagEdit">

    <ul class="flags">
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] != null && #session['WW_TRANS_I18N_LOCALE'].language == 'en'}">
          <img src="img/flag-i18n-uk.png"/>
        </s:if>
        <s:else>
          <s:a action="viewImage.action" namespace="/">
            <s:param name="address"><s:property value="address"/></s:param>
            <s:param name="scmType"><s:property value="scmType"/></s:param>
            <s:param name="selectedBranch"><s:property value="selectedBranch"/></s:param>
            <s:param name="repositoryRoot"><s:property value="repositoryRoot"/></s:param>
            <s:param name="request_locale">en_GB</s:param>
            <img src="img/flag-i18n-uk.png"/>
          </s:a>
        </s:else>
      </li>
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] == null || #session['WW_TRANS_I18N_LOCALE'].language == 'fr'}">
          <img src="img/flag-i18n-fr.png"/>
        </s:if>
        <s:else>
          <s:a action="viewImage.action" namespace="/">
            <s:param name="address"><s:property value="address"/></s:param>
            <s:param name="scmType"><s:property value="scmType"/></s:param>
            <s:param name="selectedBranch"><s:property value="selectedBranch"/></s:param>
            <s:param name="repositoryRoot"><s:property value="repositoryRoot"/></s:param>
            <s:param name="request_locale">fr_FR</s:param>
            <img src="img/flag-i18n-fr.png"/>
          </s:a>
        </s:else>
      </li>

    </ul>

  </div>


  <!--     <a title="ScmWebEditor Project Website" target="_blank" href="http://scmwebeditor.nuiton.org/"><img width="200" height="160" src="img/editor/machine-a-ecrire.png" alt="ScmWebEditor logo"/></a> -->

  <h1 id="editorTitle">SCMWebEditor</h1>

  <p id="fileRevision">
    <s:text name="scm.image"/> <s:property value="address"/>
    <s:text name="scm.atRevision"/> <span id="numrevisionDiv"><s:property value="numRevision"/></span>
  </p>

  <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="resetIndicator"/>

  <s:if test="scmSupportsBranches">
    <p id="workingBranch">
        <s:text name="scm.modificationViewer.branch"/> <s:property value="selectedBranch"/>
    </p>
  </s:if>

</div>

<div id="editBody">

  <!-- BEGIN menu -->
  <ul id="sweMenu" class="buttonsGroup">

   <li>

     <div class="menuDescription">

       <p><s:text name="scm.files"/></p>

       <!-- Buttons for the files operations -->

       <s:set id="scm.upload">
         <s:text name="scm.upload"/>
       </s:set>
       <s:set id="scm.uploadTitle">
         <s:text name="scm.uploadTitle"/>
       </s:set>

       <s:set id="scm.removeFile">
         <s:text name="scm.removeFile"/>
       </s:set>
       <s:set id="scm.removeFileTitle">
         <s:text name="scm.removeFileTitle"/>
       </s:set>

       <s:set id="scm.moveFile">
         <s:text name="scm.moveFile"/>
       </s:set>
       <s:set id="scm.moveFileTitle">
         <s:text name="scm.moveFileTitle"/>
       </s:set>

       <s:set name="address">
         <s:property value="address"/>
       </s:set>

       <ul id="repositoryButtons" class="buttonsGroup">
         <li>
           <s:a
             href="#"
             title="%{scm.uploadTitle}"
             onClick="javascript:open_popup('doUploadFile.action', 'upload', getElementById('address').value, '%{scmType}');"
             name="uploadButton">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-file fa-stack-2x"></i>
                 <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
             </span>

           </s:a>
         </li>
         <li>
           <s:a
             href="#"
             title="%{scm.removeFileTitle}"
             onClick="javascript:open_popup('doRemoveFile.action', 'remove', getElementById('address').value, '%{scmType}');"
             name="removeButton">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-file fa-stack-2x"></i>
                 <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
             </span>

           </s:a>
         </li>
         <li>
           <s:a
             href="#"
             title="%{scm.moveFileTitle}"
             onClick="javascript:open_popup('doMoveFile.action', 'move file',getElementById('address').value,'%{scmType}');"
             name="moveFileButton">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-file fa-stack-2x"></i>
                 <i class="fa fa-arrow-right fa-stack-1x fa-inverse"></i>
             </span>

           </s:a>
         </li>
       </ul>

     </div>

   </li>

   <li>

     <div class="menuDescription">

       <p><s:text name="scm.directories"/></p>

       <!-- Buttons for the directories operations -->

       <s:set id="scm.createDirectory">
         <s:text name="scm.createDirectory"/>
       </s:set>
       <s:set id="scm.createDirectoryTitle">
         <s:text name="scm.createDirectoryTitle"/>
       </s:set>

       <s:set id="scm.removeDirectory">
         <s:text name="scm.removeDirectory"/>
       </s:set>
       <s:set id="scm.removeDirectoryTitle">
         <s:text name="scm.removeDirectoryTitle"/>
       </s:set>

       <ul id="directoryButtons" class="buttonsGroup">
         <li>
           <s:a
             href="#"
             title="%{scm.createDirectoryTitle}"
             onClick="javascript:open_popup('doCreateDirectory.action', 'create directory',getElementById('address').value,'%{scmType}');"
             name="createDirectoryButton">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-folder fa-stack-2x"></i>
                 <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
             </span>

           </s:a>
         </li>
         <li>
           <s:a
             href="#"
             title="%{scm.removeDirectoryTitle}"
             onClick="javascript:open_popup('doRemoveDirectory.action', 'remove directory',getElementById('address').value,'%{scmType}');"
             name="removeDirectoryButton">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-folder fa-stack-2x"></i>
                 <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
             </span>

           </s:a>
         </li>
       </ul>

     </div>

   </li>

   <li>

     <div class="menuDescription">

       <p><s:text name="scm.shortcuts"/></p>

       <ul id="fileViewButtons" class="buttonsGroup">
         <li>
           <s:set id="openAnotherFile">
             <s:text name="scm.openAnotherFile"/>
           </s:set>

           <s:a
             id="openAnotherFile"
             href="#"
             title="%{openAnotherFile}"
             onClick="javascript:openPopin('openFilePopin');">

             <span class="fa-stack fa-lg">
                 <i class="fa fa-folder-open fa-2x"></i>
             </span>

           </s:a>
         </li>
       </ul>

     </div>

   </li>


   <li>

     <div class="menuDescription">

       <p><s:text name="scm.edit"/></p>

       <ul id="contentButtons" class="buttonsGroup">

         <!-- BEGIN exit -->
         <li>
           <s:if test="projectUrl != null">
             <s:hidden key="projectUrl" label=''/>
           </s:if>
           <s:else>
             <s:hidden id="projectUrl" value="checkout.action"/>
           </s:else>


           <s:set id="scm.exitTitle">
             <s:text name="scm.exitTitle"/>
           </s:set>
           <s:set id="scm.exitJavascript">
             <s:text name="scm.exitJavascript"/>
           </s:set>

           <s:a
             href="checkout.action"
             title="%{scm.exitTitle}"
             value="%{scm.exit}"
             name="Cancel">

             <span id="exitButton" class="fa-stack fa-lg">
                 <i class="fa fa-times fa-2x"></i>
             </span>
           </s:a>
         </li>
         <!-- END exit -->

       </ul>

     </div>

   </li>

  </ul>
  <!-- END menu -->

  <s:if test="filesDirectlyAccessible && format != 'svg'">

    <img src="<s:property value="imagePath"/>" id="displayedImage"/>

  </s:if>
  <s:else>

    <s:set id="imagePath">
      <s:property value="imagePath"/>
    </s:set>
    <img src="<s:url action='getImage.action?imagePath=%{imagePath}'/>" id="displayedImage"/>

  </s:else>

  <noscript><h4><s:text name="scm.modificationViewer.noJavascript"/></h4>
  </noscript>
  <noscript><h4><s:text name="scm.modificationViewer.betterUseJavascript"/></h4>
  </noscript>

  <div id="form">

    <p>

      <s:hidden key="address" label=''/>
      <s:hidden key="origText" label=''/>
      <s:hidden key="scmEditorUrl" label=''/>
      <s:hidden key="selectedBranch" label=''/>
      <s:hidden key="repositoryRoot" label=''/>

    <div id="scmButton"></div>

  </div>
</div>

<div id="footer">
  <jsp:include page="footer.jsp"/>
</div>
</div>

<!-- popin to open another file -->
<div class="popin" id="openFilePopin">
  <span class="closePopin" onclick="closePopin('openFilePopin')">
    X
  </span>

  <h1><s:text name="scm.openAnotherFile"/></h1>

  <div id="searchTree">

    <s:url id="searchTreeUrl"
           action="browse?address=%{repositoryRoot}&username=%{username}&pw=%{pw}&selectedBranch=%{selectedBranch}&scmType=%{scmType}"/>
    <sjt:tree id="scmTree"
              htmlTitles="true"
              jstreetheme="classic"
              href="%{searchTreeUrl}"
              onClickTopics="treeClicked"
              onSuccessTopics="treeChanged"
      />

  </div>

</div>

<!-- popin to choose how to open the SVG file -->
<div id="svgPopin" class="popin">
    <span class="closePopin" onclick="closePopin('svgPopin')">
      X
    </span>

    <s:set id="scm.openSvgEdit">
      <s:text name="scm.openSvgEdit"/>
    </s:set>

    <s:set id="scm.openSvgView">
      <s:text name="scm.openSvgView"/>
    </s:set>

    <h4><s:text name="scm.svgFile"/></h4>
    <p>
      <s:text name="scm.openSvgFile"/>
    </p>
    <s:submit id="openSvgEdit" value="%{scm.openSvgEdit}" onclick="openSvg('edit.action');"/>
    <s:submit id="openSvgView" value="%{scm.openSvgView}" onclick="openSvg('viewImage.action');"/>
</div>

<div id="popinBackground"></div>

</body>
</html>
