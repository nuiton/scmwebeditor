<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjt" uri="/struts-jquery-tree-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.createBranch"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<sj:head debug="true" jquerytheme="default"/>


<form method="POST" id="removeForm" action="createBranch.action">

  <s:hidden name="scmType" value="%{scmType}"/>

  <center><h1><s:text name="scm.createBranch"/></h1></center>

  <label for="branchName"><s:text name="scm.createBranch.branchName"/></label> <input type="text" id="newBranchName" name="newBranchName"/><br/>

  <label for="selectedBranch"><s:text name="scm.createBranch.originBranch"/></label>
  <s:select id="selectedBranch" name="selectedBranch" list="branches"/><br/>

  <s:if test="username==null || pw==null">
    <label><s:text name="scm.username"/> : <input type="text" name="username"/></label><br/>
    <label><s:text name="scm.password"/> : <input type="password"
                                                  name="pw"/></label><br/>
  </s:if>

  <s:else>
    <s:text name="scm.logAs"/> <s:property value="username"/> <br/>
  </s:else>


  <s:hidden key="address"/>
  <s:if test="badLogin">
    <p><font color="red"><s:text name="scm.badUsernameOrPassword"/></font></p>
  </s:if>
  <s:elseif test="error">
    <p><font color="red"><s:text name="scm.repoError"/></font></p>
  </s:elseif>
  <s:else>
    <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="loadingIndicator"/>
  </s:else>

  <input type="submit" onclick="document.getElementById('loadingIndicator').style.display = 'block'"/>
</form>

</body>
</html>
