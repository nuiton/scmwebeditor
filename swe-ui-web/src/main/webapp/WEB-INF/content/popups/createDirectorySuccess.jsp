<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <script type="text/javascript" src="js/popup.js"></script>
  <title><s:text name="scm.titles.success"/></title>

  <script type="text/javascript">
    if (window.opener.document.location.href.endsWith("edit.action")) {
      var address = window.opener.document.getElementById("address").value;
      var scmType = window.opener.document.getElementById("scmType").value;
      var selectedBranch = window.opener.document.getElementById("selectedBranch").value;
      var repositoryRoot = window.opener.document.getElementById("repositoryRoot").value;

      window.opener.document.location.href = "edit.action?address=" + address + "&scmType=" + scmType
        + "&selectedBranch=" + selectedBranch + "&repositoryRoot=" + repositoryRoot;
    }
    else {
      var searchButton = window.opener.document.getElementById("ajaxSearchButton");

      if (searchButton != null) {
        searchButton.click();
      } else {
        window.opener.document.location.reload(false);
      }
    }
  </script>

</head>
<body>
<p><s:text name="scm.createDirectorySuccess"/></p>

<s:set id="close">
  <s:text name="scm.close"/>
</s:set>

<form>
  <s:submit type="button" value="%{close}" onclick="window.close()"/>
</form>
</body>
</html>
