<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjt" uri="/struts-jquery-tree-tags" %>

<sj:head debug="true" jquerytheme="default"/>

<script>
  var svgAddress;
  document.getElementById('loadingIndicator').style.display = 'none';

  function openSvg(action) {
    var scmType = document.getElementById("scmType").value;
    var selectedBranch = $("#selectedBranchDisplay").text().trim();
    var repositoryRoot = document.getElementById("addressInput").value;

    document.location.href = (action + "?address=" + svgAddress + "&scmType=" + scmType
                + "&selectedBranch=" + selectedBranch + "&repositoryRoot=" + repositoryRoot);
  }

  $.subscribe('treeClicked', function(event, data) {
    var item = event.originalEvent.data.rslt.obj;

    if (item.length == 1) {
      var classAttr = item[0].getAttribute("class");

      if (classAttr.indexOf("jstree-leaf") != -1) {
        var fileExtension = item.attr("id").substr(item.attr("id").lastIndexOf(".") + 1);

        if (fileExtension.toLowerCase() === "svg") {
          svgAddress = item.attr("id");
          openPopin("svgPopin");
        } else {

          var scmType = document.getElementById("scmType").value;
          var selectedBranch = $("#selectedBranchDisplay").text().trim();
          var repositoryRoot = document.getElementById("addressInput").value;
          var actionFile = "edit.action";

          var supportedImages = ["jpg", "jpeg", "png", "gif"];

          if (supportedImages.indexOf(fileExtension.toLowerCase()) != -1) {
            actionFile = "viewImage.action";
          }

          document.location.href = (actionFile + "?address=" + item.attr("id") + "&scmType=" + scmType
                      + "&selectedBranch=" + selectedBranch + "&repositoryRoot=" + repositoryRoot);
        }
      }
    }

  });

  // automatically expand the directory when there is no other file
  $.subscribe('treeChanged', function(event, data) {

    var json = event.originalEvent.data.responseJSON;

    if (json.length == 1) {
      var object = json[0];

      if (object.state == "closed") {
        var htmlObject = document.getElementById(object.id);
        var children = htmlObject.children;
        children.item('ins').click();
      }
    }
  });

  $("#load-icon").hide();

  scmAddress = document.getElementById('addressInput').value;
  if (!scmAddress.endsWith("/")) {
    scmAddress += "/";
  }

</script>

<script src="js/popup.js" type="text/javascript"></script>

<s:if test="scmSupportsBranches && !error">

  <s:text name="scm.outConnection.headBranch"/>

  <s:if test="selectedBranch != null">
    <s:if test="!selectedBranch.equals('')">

      <s:label id="selectedBranchDisplay" name="selectedBranch"></s:label>

    </s:if>
    <s:else>

      <s:label id="selectedBranchDisplay" name="headBranchName"></s:label>

    </s:else>
  </s:if>
  <s:else>

    <s:label id="selectedBranchDisplay" name="headBranchName"></s:label>

  </s:else>

  <s:set id="createBranch">
    <s:text name="scm.createBranch"/>
  </s:set>
  <s:submit type="button" value="%{createBranch}"
            onClick="javascript:open_popup('createBranch.action', 'create branch' , getElementById('addressInput').value, '%{scmType}' );"/>

</s:if>

<div id="searchTree">
  <s:set name="address">
    <s:property value="address"/>
  </s:set>
  <s:set name="username">
    <s:property value="username"/>
  </s:set>
  <s:set name="pw">
    <s:property value="pw"/>
  </s:set>


  <s:url id="searchTreeUrl"
         action="browse?address=%{address}&username=%{username}&pw=%{pw}&selectedBranch=%{selectedBranch}&scmType=%{scmType}"/>
  <sjt:tree id="scmTree"
            htmlTitles="true"
            jstreetheme="classic"
            href="%{searchTreeUrl}"
            onClickTopics="treeClicked"
            onSuccessTopics="treeChanged"
    />

</div>

<s:if test="error">

  <s:text name="scm.cantFindRepo"/>

</s:if>
<s:else>
  <!--

  <s:set id="scm.upload">
    <s:text name="scm.upload"/>
  </s:set>
  <s:set id="scm.uploadTitle">
    <s:text name="scm.uploadTitle"/>
  </s:set>

  <s:set id="scm.removeFile">
    <s:text name="scm.removeFile"/>
  </s:set>
  <s:set id="scm.removeFileTitle">
    <s:text name="scm.removeFileTitle"/>
  </s:set>

  <s:set id="scm.createDirectory">
    <s:text name="scm.createDirectory"/>
  </s:set>
  <s:set id="scm.createDirectoryTitle">
    <s:text name="scm.createDirectoryTitle"/>
  </s:set>

  <s:set id="scm.removeDirectory">
    <s:text name="scm.removeDirectory"/>
  </s:set>
  <s:set id="scm.removeDirectoryTitle">
    <s:text name="scm.removeDirectoryTitle"/>
  </s:set>

  <s:set id="scm.moveFile">
    <s:text name="scm.moveFile"/>
  </s:set>
  <s:set id="scm.moveFileTitle">
    <s:text name="scm.moveFileTitle"/>
  </s:set>

  <s:set name="address">
    <s:property value="address"/>
  </s:set>
  <center id="repositoryButtonsBrowse">
    <s:submit name="uploadButton" value="%{scm.upload}" title="%{scm.uploadTitle}"
              onClick="javascript:open_popup('doUploadFile.action', 'upload' , scmAddress, '%{scmType}' );"/>

    <s:submit name="removeButton" value="%{scm.removeFile}" title="%{scm.removeFileTitle}"
              onClick="javascript:open_popup('doRemoveFile.action', 'remove' , scmAddress, '%{scmType}' );"/>

    <s:submit name="createDirectoryButton" value="%{scm.createDirectory}" title="%{scm.createDirectoryTitle}"
              onClick="javascript:open_popup('doCreateDirectory.action', 'create directory',scmAddress,'%{scmType}');"/>

    <s:submit name="removeDirectoryButton" value="%{scm.removeDirectory}" title="%{scm.removeDirectoryTitle}"
              onClick="javascript:open_popup('doRemoveDirectory.action', 'remove directory',scmAddress,'%{scmType}');"/>

    <s:submit name="moveFileButton" value="%{scm.moveFile}" title="%{scm.moveFileTitle}"
              onClick="javascript:open_popup('doMoveFile.action', 'move file', scmAddress, '%{scmType}');"/>
  </center>

  -->

  <ul id="sweBrowseMenu" class="buttonsGroup">

    <li>
      <!-- Buttons for the actions on the repository -->

      <s:set id="scm.upload">
        <s:text name="scm.upload"/>
      </s:set>
      <s:set id="scm.uploadTitle">
        <s:text name="scm.uploadTitle"/>
      </s:set>

      <s:set id="scm.removeFile">
        <s:text name="scm.removeFile"/>
      </s:set>
      <s:set id="scm.removeFileTitle">
        <s:text name="scm.removeFileTitle"/>
      </s:set>

      <s:set id="scm.createDirectory">
        <s:text name="scm.createDirectory"/>
      </s:set>
      <s:set id="scm.createDirectoryTitle">
        <s:text name="scm.createDirectoryTitle"/>
      </s:set>

      <s:set id="scm.removeDirectory">
        <s:text name="scm.removeDirectory"/>
      </s:set>
      <s:set id="scm.removeDirectoryTitle">
        <s:text name="scm.removeDirectoryTitle"/>
      </s:set>

      <s:set id="scm.moveFile">
        <s:text name="scm.moveFile"/>
      </s:set>
      <s:set id="scm.moveFileTitle">
        <s:text name="scm.moveFileTitle"/>
      </s:set>

      <s:set name="address">
        <s:property value="address"/>
      </s:set>

      <ul id="repositoryButtons" class="buttonsGroup">
        <li>
          <s:a
            href="#"
            title="%{scm.uploadTitle}"
            onClick="javascript:open_popup('doUploadFile.action', 'upload' , scmAddress, '%{scmType}' );"
            name="uploadButton">

            <span class="fa-stack fa-lg">
                <i class="fa fa-file fa-stack-2x"></i>
                <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
            </span>

          </s:a>
        </li>
        <li>
          <s:a
            href="#"
            title="%{scm.removeFileTitle}"
            onClick="javascript:open_popup('doRemoveFile.action', 'remove' , scmAddress, '%{scmType}' );"
            name="removeButton">

            <span class="fa-stack fa-lg">
                <i class="fa fa-file fa-stack-2x"></i>
                <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
            </span>

          </s:a>
        </li>
        <li>
          <s:a
            href="#"
            title="%{scm.moveFileTitle}"
            onClick="javascript:open_popup('doMoveFile.action', 'move file', scmAddress, '%{scmType}');"
            name="moveFileButton">

            <span class="fa-stack fa-lg">
                <i class="fa fa-file fa-stack-2x"></i>
                <i class="fa fa-arrow-right fa-stack-1x fa-inverse"></i>
            </span>

          </s:a>
        </li>
      </ul>

    </li>

    <li>

      <ul id="directoryButtons" class="buttonsGroup">
        <li>
          <s:a
            href="#"
            title="%{scm.createDirectoryTitle}"
            onClick="javascript:open_popup('doCreateDirectory.action', 'create directory',scmAddress,'%{scmType}');"
            name="createDirectoryButton">

            <span class="fa-stack fa-lg">
                <i class="fa fa-folder fa-stack-2x"></i>
                <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
            </span>

          </s:a>
        </li>
        <li>
          <s:a
            href="#"
            title="%{scm.removeDirectoryTitle}"
            onClick="javascript:open_popup('doRemoveDirectory.action', 'remove directory',scmAddress,'%{scmType}');"
            name="removeDirectoryButton">

            <span class="fa-stack fa-lg">
                <i class="fa fa-folder fa-stack-2x"></i>
                <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
            </span>

          </s:a>
        </li>
      </ul>

    </li>

  </ul>

  <!-- popin to choose how to open the SVG file -->
  <div id="svgPopin" class="popin">
    <span class="closePopin" onclick="closePopin('svgPopin')">
      X
    </span>

    <s:set id="scm.openSvgEdit">
      <s:text name="scm.openSvgEdit"/>
    </s:set>

    <s:set id="scm.openSvgView">
      <s:text name="scm.openSvgView"/>
    </s:set>

    <h4><s:text name="scm.svgFile"/></h4>
    <p>
      <s:text name="scm.openSvgFile"/>
    </p>
    <s:submit id="openSvgEdit" value="%{scm.openSvgEdit}" onclick="openSvg('edit.action');"/>
    <s:submit id="openSvgView" value="%{scm.openSvgView}" onclick="openSvg('viewImage.action');"/>
  </div>


  <div id="popinBackground"></div>

</s:else>

