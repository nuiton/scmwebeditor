<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ taglib prefix="s" uri="/struts-tags" %>

<s:set name="lastRevision">
  <s:property value="lastRevision"/>
</s:set>

<s:hidden id="valueOfLastRevision" value="%{lastRevision}"/>
<s:if test="error=='authError'">
  <p><font color="red"><s:text name="scm.badUsernameOrPassword"/></font></p>
</s:if>
<s:elseif test="error=='errorPath'">
  <p><font color="red"><s:text name="scm.repoError"/></font></p>
</s:elseif>
<s:else>
  <script type="text/javascript">

    document.getElementById("resetIndicator").style.display = "none";
    editor.mirror.setValue(document.getElementById('valueOfLastRevision').value);

    var numrev = "<s:property value="numRevision" />";
    if (numrev != null) {
      document.getElementById('numrevisionDiv').innerHTML = numrev;
    }
  </script>
</s:else>
