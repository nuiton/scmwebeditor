<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.titles.badRstFile"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<a target="_blank" href="http://scmwebeditor.nuiton.org"><img
  src="img/ScmWebEditor_main.png" alt="$alt"/></a>

<form id="forceCommitForm" method="POST" action="commit.action">
  <h4><s:text name="scm.rstNotValidMessage"/></h4>

  <input type="hidden" name="force" value="true"/>
  <s:hidden name="address" value="%{address}"/>
  <s:hidden name="commitMessage" value="%{commitMessage}"/>
  <s:hidden name="newText" value="%{newText}"/>
  <s:hidden name="lastText" value="%{lastText}"/>
  <s:hidden name="fileType" value="%{fileType}"/>
  <s:hidden name="projectUrl" value="%{projectUrl}"/>
  <s:hidden name="format" value="%{format}"/>

  <s:hidden name="username" value="%{username}"/>
  <s:hidden name="pw" value="%{pw}"/>


  <s:set id="scm.yes">
    <s:text name="scm.yes"/>
  </s:set>

  <s:submit
    id="ajaxForceCommitButton"
    formIds="forceCommitForm"
    indicator="indicator"
    button="true"
    buttonIcon="ui-icon-refresh"
    value="%{scm.yes}"
    >
  </s:submit>

  <s:set id="scm.no">
    <s:text name="scm.no"/>
  </s:set>
  <s:submit type="button" value="%{scm.no}"
            onclick="history.back();return false;"/>

</form>
</body>
</html>
