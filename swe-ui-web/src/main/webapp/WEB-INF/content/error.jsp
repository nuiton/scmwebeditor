<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.titles.error"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <% if (request.getAttribute("projectUrl") != null) { %>
  <meta http-equiv="Refresh"
        content="3; url=<%=request.getAttribute("projectUrl")%>">
  <% } else { %>
  <meta http-equiv="Refresh" content="3;URL=checkout.action">
  <% } %>
  <%--%=request.getAttribute("scmEditorUrl")%>?adresse=<%=request.getAttribute("Scmpath_url")%>&amp;file_name=<%=request.getAttribute("Filename_url")%>&amp;project_url=<%=request.getAttribute("Redirection_url")%>&amp;lang=<%=request.getAttribute("Lang")%>&amp;format=<%=request.getAttribute("Format")%>"--%>
</head>
<body>
<a target="_blank" href="http://scmwebeditor.nuiton.org/"><img
  src="img/ScmWebEditor_main.png" alt="$alt"/></a>

<p><s:text name="scm.badUsernameOrPassword"/></p>

<div id="footer">
  <jsp:include page="footer.jsp"/>
</div>
</body>
</html>
