<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html"
        pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.titles.privateScm"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<a target="_blank" href="http://scmwebeditor.nuiton.org/"><img
  src="img/ScmWebEditor_main.png" alt="$alt"/></a>

<form method="post" action="edit.action">
  <input type="hidden" name="address"
         value="<%=request.getAttribute("address")%>"/>
  <input type="hidden" name="fromLoginPage" value="true">

  <input type="hidden" id="ProjectUrl" name="ProjectUrl" value="checkout.action"/>

  <input type="hidden" name="selectedBranch" value="<%=request.getAttribute("selectedBranch")%>"/>

  <input type="hidden" name="repositoryRoot" value="<%=request.getAttribute("repositoryRoot")%>"/>

  <script src="js/cancelRedirect.js"></script>
  <p><s:text name="scm.privateScmAccess"/></p>

  <p><label ACCESSKEY=U><s:text name="scm.username"/> : <input TYPE=text
                                                               NAME=username
                                                               SIZE=12></label>
    <label ACCESSKEY=P><s:text name="scm.password"/> : <input
      TYPE=password NAME=pw SIZE=12></label>
    <label><input type="checkbox" name="saveCookie" value="true"><s:text name="scm.stayLogin"/></label>

  </p>

  <s:hidden name="scmType" value="%{scmType}" id="scmType"/>

  <s:set id="submit">
    <s:text name="scm.submit"/>
  </s:set>

  <s:submit id="submitPrivateScm" name="Save" value="%{submit}" onclick="document.getElementById('loadingIndicator').style.display = 'block';"/>


  <s:set id="scm.exit">
    <s:text name="scm.exit"/>
  </s:set>
  <s:set id="scm.exitJavascript">
    <s:text name="scm.exitJavascript"/>
  </s:set>
  <s:submit type="button" value="%{scm.exit}" name="exitLogin"
            onclick="return cancelRedirect('%{scm.exitJavascript}',document.getElementById('ProjectUrl'));"/>

  <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="loadingIndicator"/>

</form>
<div id="footer">
  <jsp:include page="footer.jsp"/>
</div>
</body>
</html>
