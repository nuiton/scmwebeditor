<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<sj:head jquerytheme="default"/>

<div id="search">
  <p><s:text name="scm.mustBeLog"/></p>

  <p>
    <label><s:text name="scm.username"/> : <input type="text"
                                                  name="username" id="username"></input></label><br/>
    <label><s:text name="scm.password"/> : <input type="password"
                                                  name="pw" id="pw"></input></label><br/>

    <s:url id="ajaxSearchLogin" value="browse.action"/>

    <s:set id="scm.loginButton">
      <s:text name="scm.loginButton"/>
    </s:set>
    <sj:submit
      id="ajaxSearchButtonLogin"
      formIds="configForm"
      targets="htmlcontentSearch"
      href="%{ajaxSearchLogin}"
      button="true"
      buttonIcon="ui-icon-refresh"
      value="%{scm.loginButton}"
      >
    </sj:submit>
  </p>
</div>

<script type="text/javascript">
  document.getElementById("loadingIndicator").style.display = "none";
</script>
