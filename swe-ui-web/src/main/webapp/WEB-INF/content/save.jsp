<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<s:if test="result=='login'">
  <p><font color="red"><s:text name="scm.badUsernameOrPassword"/><br/> <s:text
    name="scm.cannotSave"/></font></p>
</s:if>
<s:elseif test="result=='error'">
  <p><s:text name="scm.cannotSave"/></p>
</s:elseif>
<s:elseif test="result=='errorRst'">

  <p><s:text name="scm.rstNotValidMessage"/></p>
  <s:url id="ajaxForceSave" action="save"/>
  <input type="hidden" name="force" value="true"/>

  <s:set id="scm.yes">
    <s:text name="scm.yes"/>
  </s:set>

  <sj:submit
    id="ajaxForceSaveButton"
    formIds="editForm"
    targets="htmlcontentCommit"
    href="%{ajaxForceSave}"
    indicator="indicator"
    button="true"
    buttonIcon="ui-icon-refresh"
    value="%{scm.yes}"
    >
  </sj:submit>

</s:elseif>

<s:elseif test="result=='fileModify'">
  <p><s:text name="scm.fileModify"/> <s:text name="scm.by"/> <s:property
    value="headCommiter"/></p>

  <s:set id="scm.showDiff">
    <s:text name="scm.showDiff"/>
  </s:set>
  <s:submit value="%{scm.showDiff}"
            onclick="getElementById('diffArea').style.display='inline';return false;"/>

  <s:set id="diff">
    <s:property value="diff"/>
  </s:set>
  <s:textarea id="diffArea" cssStyle="display:none" rows="20" cols="80"
              value="%{diff}"/>
  <s:url id="ajaxForceSave" action="save"/>

  <input type="hidden" name="force" value="true"/>

  <s:set id="scm.forceSave">
    <s:text name="scm.forceSave"/>
  </s:set>
  <sj:submit
    id="ajaxForceSaveButton"
    formIds="editForm"
    targets="htmlcontentCommit"
    href="%{ajaxForceSave}"
    indicator="indicator"
    button="true"
    buttonIcon="ui-icon-refresh"
    value="%{scm.forceSave}"
    >
  </sj:submit>

</s:elseif>

<s:elseif test="result=='errorPath'">
  <p><s:text name="scm.cannotSave"/>, <s:text name="scm.pathError"/></p>
</s:elseif>

<s:elseif test="result=='uselessSave'">
  <p><s:text name="scm.uselessSave"/></p>
</s:elseif>

<s:else>
  <p><s:text name="scm.lastChangeSave"/>: <s:property value="formatDate"/></p>
  <s:set id="lastRevSet" value="newText"/>
  <s:hidden id="lastRev" value="%{lastText}"/>
  <script type="text/javascript">
    origText = document.getElementById("origText");
    origText.value = document.getElementById("lastRev").value;
    var numrev = '<s:property value="numRevision" />';
    if (numrev != null) {
      document.getElementById('numrevisionDiv').innerHTML = numrev;
    }
  </script>
</s:else>
