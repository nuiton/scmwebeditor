<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjt" uri="/struts-jquery-tree-tags" %>
<sj:head jquerytheme="default"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <title><s:text name="scm.titles.swe"/></title>

  <script type="text/javascript" src="js/branches.js"></script>
  <script type="text/javascript" src="js/scmDetector.js"></script>
  <script type="text/javascript" src="js/popin.js"></script>

  <script type="text/javascript">

    function browseValidation() {
      var address = document.getElementById('addressInput');
      var addressValid = address.validity.valid;

      return addressValid;
    }

    $.subscribe("validateBrowse", function(event, element) {

      var browseValid = browseValidation();

      if (browseValid) {
        var loadingIndicator = document.getElementById('loadingIndicator');
        loadingIndicator.style.display = 'inline';
        getBranchesList();
      }

      event.originalEvent.options.submit = browseValid;
    });

  </script>

</head>
<body>
<!-- <a target="_blank" href="http://scmwebeditor.nuiton.org/"><img src="img/accueil/machine-a-ecrire.png" alt="$alt" /></a> -->
<div id="wrapperConfig">


  <div id="flagHome">

    <ul class="flags">
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] != null && #session['WW_TRANS_I18N_LOCALE'].language == 'en'}">
          <img src="img/flag-i18n-uk.png"/>
        </s:if>
        <s:else>
          <s:a action="checkout.action" namespace="/">
            <s:param name="request_locale">en_GB</s:param>
            <img src="img/flag-i18n-uk.png"/>
          </s:a>
        </s:else>
      </li>
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] == null || #session['WW_TRANS_I18N_LOCALE'].language == 'fr'}">
          <img src="img/flag-i18n-fr.png"/>
        </s:if>
        <s:else>
          <s:a action="checkout.action" namespace="/">
            <s:param name="request_locale">fr_FR</s:param>
            <img src="img/flag-i18n-fr.png"/>
          </s:a>
        </s:else>
      </li>

    </ul>

  </div>


  <div id="wrapper2Config">

    <div id="titleConfig">
      <h2><s:text name="scm.welcome"/></h2>
      <noscript><h4><s:text name="scm.outConnection.noJavascript"/></h4>
      </noscript>
      <h4>
        <s:text name="scm.outConnection.enterRepo"/>
      </h4>
    </div>


    <form id="configForm" method="get" action="checkout.action">

      <center>


        <p><label><s:text name="scm.outConnection.scmPath"/> <input TYPE="url"
                                                                    name="address"
                                                                    SIZE=50 id="addressInput"></label>

          <s:hidden id="selectedBranch" name="selectedBranch" value=""/>

          <s:url id="ajaxSearch" value="browse.action"/>

          <s:set id="search">
            <s:text name="scm.outConnection.search"/>
          </s:set>
          <sj:submit
            id="ajaxSearchButton"
            formIds="configForm"
            targets="htmlcontentSearch"
            href="%{ajaxSearch}"
            button="true"
            value="%{search}"
            onBeforeTopics="validateBrowse"
            >
          </sj:submit>

          <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="loadingIndicator"/>
        </p>

      </center>

      <label>
        <s:text name="scm.outConnection.scmType"/>
        <s:select id="scmType" name="scmType" list="supportedScmsName"/>
      </label>

      <div id="branches">
        <label>
          <s:text name="scm.outConnection.selectBranch"/>
          <select id="branchesList">
            <option value=""><s:text name="scm.outConnection.branches"/>
          </select>
        </label>
      </div>

      <div id="htmlcontentSearch"></div>
    </form>

    <!--
        <div id="searchTree">
        
            <s:url id="searchTreeUrl" action="search?address=http://localhost/scm"/>
            
            <sjt:tree id="scmTree"
                      jstreetheme="classic"
                      href="%{searchTreeUrl}"
                      onClickTopics="treeClicked"/>
        
        </div>
 -->
  </div>
</div>
<div id="footer">
  <jsp:include page="footer.jsp"/>
</div>

</body>
</html>
