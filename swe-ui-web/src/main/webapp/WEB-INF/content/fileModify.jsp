<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.fileModify"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<a target="_blank" href="http://scmwebeditor.nuiton.org/"><img
  src="img/ScmWebEditor_main.png" alt="$alt"/></a>


<form id="forceCommitForm" method="POST" action="commit.action">
  <h4><s:text name="scm.fileModify"/> <s:text name="scm.by"/> <s:property
    value="headCommiter"/></h4>

  <s:set id="scm.showDiff">
    <s:text name="scm.showDiff"/>
  </s:set>
  <s:submit value="%{scm.showDiff}"
            onclick="getElementById('diffArea').style.display='inline';return false;"/>

  <s:set id="diff">
    <s:property value="diff"/>
  </s:set>
  <s:textarea id="diffArea" cssStyle="display:none" rows="20" cols="80"
              value="%{diff}"/>


  <input type="hidden" name="force" value="true"/>
  <s:hidden name="address" value="%{address}"/>
  <s:hidden name="commitMessage" value="%{commitMessage}"/>
  <s:hidden name="newText" value="%{newText}"/>
  <s:hidden name="lastText" value="%{lastText}"/>
  <s:hidden name="fileType" value="%{fileType}"/>
  <s:hidden name="projectUrl" value="%{projectUrl}"/>
  <s:hidden name="format" value="%{format}"/>

  <s:hidden name="username" value="%{username}"/>
  <s:hidden name="pw" value="%{pw}"/>


  <s:set id="scm.forceSave">
    <s:text name="scm.forceSave"/>
  </s:set>

  <input type="hidden" name="force" value="true"/>
  <s:submit
    id="ajaxForceSaveButton"
    formIds="editForm"
    indicator="indicator"
    button="true"
    buttonIcon="ui-icon-refresh"
    value="%{scm.forceSave}"
    >
  </s:submit>


  <s:set id="scm.exit">
    <s:text name="scm.exit"/>
  </s:set>
  <s:submit type="button" value="%{scm.exit}"
            onclick="history.back();return false;"/>


</form>

</body>
</html>
