<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjt" uri="/struts-jquery-tree-tags" %>
<sj:head jquerytheme="default"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title><s:text name="scm.titles.swe"/></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <!-- languages support -->
  <script type="text/javascript">

    var i18n = {
      "find" : '<s:text name="scm.find"/>',
      "replace" : '<s:text name="scm.replace"/>',
      "all" : '<s:text name="scm.all"/>',
      "language" : '<s:text name="scm.language"/>',
      "caseSensitive" : '<s:text name="scm.caseSensitive"/>',
      "regularExpressions" : '<s:text name="scm.regularExpressions"/>',
      "replaceAll" : '<s:text name="scm.replaceAll"/>',
      "commitWithoutMessage" : '<s:text name="scm.commitWithoutMessage"/>',
      "exitJavascript" : '<s:text name="scm.exitJavascript"/>',
      "undo" : '<s:text name="scm.undo"/>',
      "redo" : '<s:text name="scm.redo"/>',
      "jumpToLine" : '<s:text name="scm.jumpToLine"/>',
      "reformatSelection" : '<s:text name="scm.reformatSelection"/>',
      "reformatDocument" : '<s:text name="scm.reformatDocument"/>'
    };

  </script>

  <script type="text/javascript" src="js/selectLanguage.js"></script>
  <script type="text/javascript" src="js/popin.js"></script>

  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css">
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

  <!-- Code mirror -->
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/lib/codemirror.js" type="text/javascript"></script>
  <link rel="stylesheet" href="webjars/codemirror/<s:property value='codeMirrorVersion'/>/lib/codemirror.css">
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/addon/mode/overlay.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/addon/search/searchcursor.js" type="text/javascript"></script>

  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/rst/rst.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/markdown/markdown.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/clike/clike.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/htmlmixed/htmlmixed.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/javascript/javascript.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/css/css.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/xml/xml.js" type="text/javascript"></script>
  <script src="webjars/codemirror/<s:property value='codeMirrorVersion'/>/mode/stex/stex.js" type="text/javascript"></script>

  <script src="codemirror-ui/js/codemirror-ui.js" type="text/javascript"></script>
  <link rel="stylesheet" href="codemirror-ui/css/codemirror-ui.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


  <script src="js/cancelRedirect.js" type="text/javascript"></script>
  <script src="js/popup.js" type="text/javascript"></script>

  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <script type="text/javascript" src="js/editor.js"></script>

  <script type="text/javascript">

    var svgAddress;

    // Opens the selected file in the tree
    $.subscribe('treeClicked', function(event, data) {
      var item = event.originalEvent.data.rslt.obj;

      if (item.length == 1) {
        var classAttr = item[0].getAttribute("class");

        if (classAttr.indexOf("jstree-leaf") != -1) {

          var fileExtension = item.attr("id").substr(item.attr("id").lastIndexOf(".") + 1);

          if (fileExtension.toLowerCase() === "svg") {
            svgAddress = item.attr("id");
            openPopin("svgPopin");
          } else {

            var scmType = document.getElementById("scmType").value;
            var actionFile = "edit.action";

            var supportedImages = ["jpg", "jpeg", "png", "gif"];

            if (supportedImages.indexOf(fileExtension.toLowerCase()) != -1) {
              actionFile = "viewImage.action";
            }

            document.location.href = (actionFile + "?address=" + item.attr("id") + "&scmType=" + scmType
            + '&selectedBranch=<s:property value="selectedBranch"/>&repositoryRoot=<s:property value="repositoryRoot"/>');
          }
        }
      }
    });

    /* Allows to open a SVG file in the selected mode */
    function openSvg(action) {
      var scmType = document.getElementById("scmType").value;

      document.location.href = (action + "?address=" + svgAddress + "&scmType=" + scmType
              + '&selectedBranch=<s:property value="selectedBranch"/>&repositoryRoot=<s:property value="repositoryRoot"/>');
    }

    window.onbeforeunload = confirmExitOnUnload;

  </script>

  <s:if test="autoSaveInterval > 0">
    <script type="text/javascript" src="js/autoSave.js"></script>

    <script type="text/javascript">
      setInterval(autoSave, <s:property value="autoSaveInterval"/>);
    </script>
  </s:if>

  <s:if test="format == 'rst' || format == 'md'">
    <script type="text/javascript" src="js/preview.js"></script>
  </s:if>


</head>
<body id="editBody">

<div id="wrapper">

<form method="POST" action="commit.action" id="editForm">

<s:hidden name="scmType" id="scmType" value="%{scmType}"/>

<div id="head">


  <div id="flagEdit">

    <ul class="flags">
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] != null && #session['WW_TRANS_I18N_LOCALE'].language == 'en'}">
          <img src="img/flag-i18n-uk.png" alt="EN"/>
        </s:if>
        <s:else>
          <s:a action="edit.action" namespace="/">
            <s:param name="address"><s:property value="address"/></s:param>
            <s:param name="scmType"><s:property value="scmType"/></s:param>
            <s:param name="selectedBranch"><s:property value="selectedBranch"/></s:param>
            <s:param name="repositoryRoot"><s:property value="repositoryRoot"/></s:param>
            <s:param name="request_locale">en_GB</s:param>
            <img src="img/flag-i18n-uk.png" alt="English"/>
          </s:a>
        </s:else>
      </li>
      <li>
        <s:if
          test="%{#session['WW_TRANS_I18N_LOCALE'] == null || #session['WW_TRANS_I18N_LOCALE'].language == 'fr'}">
          <img src="img/flag-i18n-fr.png" alt="FR"/>
        </s:if>
        <s:else>
          <s:a action="edit.action" namespace="/">
            <s:param name="address"><s:property value="address"/></s:param>
            <s:param name="scmType"><s:property value="scmType"/></s:param>
            <s:param name="selectedBranch"><s:property value="selectedBranch"/></s:param>
            <s:param name="repositoryRoot"><s:property value="repositoryRoot"/></s:param>
            <s:param name="request_locale">fr_FR</s:param>
            <img src="img/flag-i18n-fr.png" alt="Français"/>
          </s:a>
        </s:else>
      </li>

    </ul>

  </div>


  <!--     <a title="ScmWebEditor Project Website" target="_blank" href="http://scmwebeditor.nuiton.org/"><img width="200" height="160" src="img/editor/machine-a-ecrire.png" alt="ScmWebEditor logo"/></a> -->

  <h1 id="editorTitle">SCMWebEditor</h1>

  <p id="fileRevision">
    <s:text name="scm.FileInEditor"/> <s:property value="address"/>
    <s:text name="scm.atRevision"/> <span id="numrevisionDiv"><s:property value="numRevision"/></span>
  </p>

  <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="resetIndicator"/>

  <s:if test="scmSupportsBranches">
    <p id="workingBranch">
        <s:text name="scm.modificationViewer.branch"/> <s:property value="selectedBranch"/>
    </p>
  </s:if>

</div>

<div id="editorBody">

  <s:hidden key="format" label=''/>
  <s:hidden key="mimeType" label=''/>

  <!-- BEGIN menu -->
  <ul id="sweMenu" class="buttonsGroup">

    <li>

      <div class="menuDescription">

        <p><s:text name="scm.files"/></p>

        <!-- Buttons for the files operations -->

        <s:set id="scm.upload">
          <s:text name="scm.upload"/>
        </s:set>
        <s:set id="scm.uploadTitle">
          <s:text name="scm.uploadTitle"/>
        </s:set>

        <s:set id="scm.removeFile">
          <s:text name="scm.removeFile"/>
        </s:set>
        <s:set id="scm.removeFileTitle">
          <s:text name="scm.removeFileTitle"/>
        </s:set>

        <s:set id="scm.moveFile">
          <s:text name="scm.moveFile"/>
        </s:set>
        <s:set id="scm.moveFileTitle">
          <s:text name="scm.moveFileTitle"/>
        </s:set>

        <s:set name="address">
          <s:property value="address"/>
        </s:set>

        <ul id="repositoryButtons" class="buttonsGroup">
          <li>
            <s:a
              href="#"
              title="%{scm.uploadTitle}"
              onClick="javascript:open_popup('doUploadFile.action', 'upload', getElementById('address').value, '%{scmType}');"
              name="uploadButton">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-file fa-stack-2x"></i>
                  <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
              </span>

            </s:a>
          </li>
          <li>
            <s:a
              href="#"
              title="%{scm.removeFileTitle}"
              onClick="javascript:open_popup('doRemoveFile.action', 'remove', getElementById('address').value, '%{scmType}');"
              name="removeButton">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-file fa-stack-2x"></i>
                  <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
              </span>

            </s:a>
          </li>
          <li>
            <s:a
              href="#"
              title="%{scm.moveFileTitle}"
              onClick="javascript:open_popup('doMoveFile.action', 'move file',getElementById('address').value,'%{scmType}');"
              name="moveFileButton">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-file fa-stack-2x"></i>
                  <i class="fa fa-arrow-right fa-stack-1x fa-inverse"></i>
              </span>

            </s:a>
          </li>
        </ul>

      </div>

    </li>

    <li>

      <div class="menuDescription">

        <p><s:text name="scm.directories"/></p>

        <!-- Buttons for the directories operations -->

        <s:set id="scm.createDirectory">
          <s:text name="scm.createDirectory"/>
        </s:set>
        <s:set id="scm.createDirectoryTitle">
          <s:text name="scm.createDirectoryTitle"/>
        </s:set>

        <s:set id="scm.removeDirectory">
          <s:text name="scm.removeDirectory"/>
        </s:set>
        <s:set id="scm.removeDirectoryTitle">
          <s:text name="scm.removeDirectoryTitle"/>
        </s:set>

        <ul id="directoryButtons" class="buttonsGroup">
          <li>
            <s:a
              href="#"
              title="%{scm.createDirectoryTitle}"
              onClick="javascript:open_popup('doCreateDirectory.action', 'create directory',getElementById('address').value,'%{scmType}');"
              name="createDirectoryButton">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-folder fa-stack-2x"></i>
                  <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
              </span>

            </s:a>
          </li>
          <li>
            <s:a
              href="#"
              title="%{scm.removeDirectoryTitle}"
              onClick="javascript:open_popup('doRemoveDirectory.action', 'remove directory',getElementById('address').value,'%{scmType}');"
              name="removeDirectoryButton">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-folder fa-stack-2x"></i>
                  <i class="fa fa-minus fa-stack-1x fa-inverse"></i>
              </span>

            </s:a>
          </li>
        </ul>

      </div>

    </li>

    <li>

      <div class="menuDescription">

        <p><s:text name="scm.shortcuts"/></p>

        <ul id="fileViewButtons" class="buttonsGroup">
          <li>
            <s:set id="openAnotherFile">
              <s:text name="scm.openAnotherFile"/>
            </s:set>

            <s:a
              id="openAnotherFile"
              href="#"
              title="%{openAnotherFile}"
              onClick="javascript:openPopin('openFilePopin');">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-folder-open fa-2x"></i>
              </span>

            </s:a>
          </li>

          <li>
            <s:set id="viewHistory">
              <s:text name="scm.viewHistory"/>
            </s:set>

            <s:a
              id="viewHistory"
              href="#"
              title="%{viewHistory}"
              onClick="javascript:openPopin('fileHistoryPopin');">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-history fa-2x"></i>
              </span>

            </s:a>
          </li>
        </ul>

      </div>

    </li>

    <s:if test="format == 'rst' || format == 'md'">

      <li>

        <div class="menuDescription">

          <p><s:text name="scm.preview"/></p>

          <ul id="previewButtons" class="buttonsGroup">
            <li id="previewSideButton" class="previewPosSelected">
              <s:set id="previewSide">
                <s:text name="scm.modificationViewer.previewPosition.side"/>
              </s:set>

              <s:a
                href="javascript:changePreviewPos('side');"
                title="%{previewSide}">

              <span class="fa-stack fa-lg">
                  <i class="fa fa-pause fa-stack-2x"></i>
              </span>

              </s:a>
            </li>

            <li id="previewBelowButton">
              <s:set id="previewBelow">
                <s:text name="scm.modificationViewer.previewPosition.below"/>
              </s:set>

              <s:a
                href="javascript:changePreviewPos('below');"
                title="%{previewBelow}">

                <span class="fa-stack fa-lg">
                  <i class="fa fa-pause fa-stack-2x fa-rotate-90"></i>
                </span>

              </s:a>
            </li>

            <li id="previewNoneButton">
              <s:set id="previewNone">
                <s:text name="scm.modificationViewer.previewPosition.none"/>
              </s:set>

              <s:a
                href="javascript:changePreviewPos('none');"
                title="%{previewNone}">

                <span class="fa-stack fa-lg">
                  <i class="fa fa-eye-slash fa-2x"></i>
                </span>

              </s:a>
            </li>
          </ul>

        </div>

      </li>

    </s:if>

    <li>

      <div class="menuDescription">

        <p><s:text name="scm.edit"/></p>

        <ul id="contentButtons" class="buttonsGroup">

          <!--BEGIN Save and continue -->
          <li>
            <s:url id="ajaxCommit" value="save.action"/>

            <s:set id="scm.saveAndContinueTitle">
              <s:text name="scm.saveAndContinueTitle"/>
            </s:set>

            <s:a
              title="%{scm.saveAndContinueTitle}"
              href="javascript:loadChange(); openPopin('commitPopin');"
              >
              <span id="saveContinueButton" class="fa-stack fa-lg">
                  <i class="fa fa-floppy-o fa-2x"></i>
              </span>
            </s:a>
          </li>
          <!--END Save and continue -->

          <!--BEGIN reset -->
          <li>
            <s:set id="scm.resetTitle">
              <s:text name="scm.resetTitle"/>
            </s:set>
            <sj:a
              title="%{scm.resetTitle}"
              formIds="editForm,commitForm"
              id="resetButton"
              targets="htmlcontentCommit"
              href="reset.action"
              onclick="document.getElementById('resetIndicator').style.display = 'inline';"
              >
              <span id="resetButton" class="fa-stack fa-lg">
                  <i class="fa fa-arrow-left fa-2x"></i>
              </span>
            </sj:a>
          </li>

          <!--END reset -->

          <!-- BEGIN save and exit -->
          <li>
            <s:set id="scm.saveAndQuitTitle">
              <s:text name="scm.saveAndQuitTitle"/>
            </s:set>

            <s:a
              href="javascript:exitAfterCommit = true; loadChange(); openPopin('commitPopin');"
              title="%{scm.saveAndQuitTitle}"
              name="Save"
              >
              <span class="fa-stack fa-lg save-exit">
                  <i class="fa fa-floppy-o fa-2x"></i>
                  <i class="fa fa-times fa-1x"></i>
              </span>
            </s:a>
          </li>

          <!-- END save and exit -->

          <!-- BEGIN exit -->
          <li>
            <s:if test="projectUrl != null">
              <s:hidden key="projectUrl" label=''/>
            </s:if>
            <s:else>
              <s:hidden id="projectUrl" value="checkout.action"/>
            </s:else>


            <s:set id="scm.exitTitle">
              <s:text name="scm.exitTitle"/>
            </s:set>
            <s:set id="scm.exitJavascript">
              <s:text name="scm.exitJavascript"/>
            </s:set>

            <s:a
              href="#"
              title="%{scm.exitTitle}"
              value="%{scm.exit}"
              name="Cancel"
              onclick="confirmExit();">

              <span id="exitButton" class="fa-stack fa-lg">
                  <i class="fa fa-times fa-2x"></i>
              </span>
            </s:a>
          </li>
          <!-- END exit -->

        </ul>

      </div>

    </li>

    <li>

      <s:if test="format=='rst'">

        <div class="menuDescription">

          <p><s:text name="scm.help"/></p>

          <ul class="buttonsGroup">
            <li>

              <s:a
                cssClass="infoButton"
                href="#"
                title="%{scm.info.rstFile}"
                onClick="javascript:openPopin('rstInfoPopin');"
                name="infoRstButton">
                  <span class="fa-stack fa-lg">
                    RST ?
                  </span>
              </s:a>

            </li>
          </ul>

        </div>

      </s:if>
      <s:elseif test="format == 'md'">

        <div class="menuDescription">

          <p><s:text name="scm.help"/></p>

          <ul class="buttonsGroup">
            <li>

              <s:a
                cssClass="infoButton"
                href="#"
                title="%{scm.info.mdFile}"
                onClick="javascript:openPopin('mdInfoPopin');"
                name="infoMdButton">
                  <span class="fa-stack fa-lg">
                    MD ?
                  </span>
              </s:a>

            </li>
          </ul>

        </div>

      </s:elseif>

    </li>
  </ul>
  <!-- END menu -->

  <textarea id="newTextId" name="newText"><s:property
      escapeHtml="false" value="OrigText"/></textarea>


  <script type="text/javascript">
    var textarea = document.getElementById('newTextId');
    var uiOptions = { path : 'codemirror-ui/js/', searchMode : 'inline', imagePath: 'codemirror-ui/images/silk',
      buttons: ['undo', 'redo', 'jump', 'reindentSelection', 'reindent'] }
    var codeMirrorOptions = { mode: "null", lineNumbers: true, lineWrapping: true }
    var editor = new CodeMirrorUI(textarea, uiOptions, codeMirrorOptions);

    editor.mirror.setOption("extraKeys", {
      "Ctrl-S": function(cm) {
        openPopin("commitPopin");
      }
    });
  </script>


  <s:if test="format == 'rst' || format == 'md'">
    <script type="text/javascript">

      var htmlcontentPreview = document.createElement("div");
      htmlcontentPreview.id = "htmlcontentPreview";
      var editorBody = document.getElementById("editorBody");
      editorBody.appendChild(htmlcontentPreview);

      var ed = document.getElementsByClassName("CodeMirror-wrap");
      ed[0].style.width = "50%";

    </script>
  </s:if>

  <script type='text/javascript'>
    selectLanguage('<s:property value="mimeType"/>', '<s:property value="format"/>');
    changeModeBy(editor, document.getElementById('language'));
  </script>


  <noscript><h4><s:text name="scm.modificationViewer.noJavascript"/></h4>
  </noscript>
  <noscript><h4><s:text name="scm.modificationViewer.betterUseJavascript"/></h4>
  </noscript>

  <div id="form">


    <p>


      <s:hidden key="address" label=''/>
      <s:hidden key="origText" label=''/>
      <s:hidden key="scmEditorUrl" label=''/>
      <s:hidden key="selectedBranch" label=''/>
      <s:hidden key="repositoryRoot" label=''/>


    <div id="scmButton"></div>

  </div>
</div>
</form>

  <div id="footer">
    <jsp:include page="footer.jsp"/>
    <s:if test="autoSaveInterval > 0">
      <span id="lastAutoSave">
        <s:text name="scm.lastAutoSave"/> <span id="lastAutoSaveTime"></span>
      </span>
    </s:if>
  </div>

</div>

<!-- popin for commit message and authentication information -->
<div class="popin" id="commitPopin">
  <span class="closePopin" onclick="closePopin('commitPopin')">
    X
  </span>

  <h1><s:text name="scm.titles.saveChanges"/></h1>

  <form id="commitForm">

    <label for="commitMessage"><s:text name="scm.commitMessage"/></label>
    <textarea id="commitMessage" name="commitMessage"></textarea><br/>

    <s:if test="username==null || pw==null">
      <label for="username"><s:text name="scm.username"/> :</label> <input type="text" name="username" id="username"/><br/>
      <label for="pw"><s:text name="scm.password"/> :</label> <input type="password" id="pw" name="pw"/><br/>
      <input type="checkbox" name="saveCookie" value="true" id="saveCookie"><s:text name="scm.stayLogin"/>
    </s:if>

    <s:else>
      <div>
      <s:text name="scm.logAs"/> <s:property value="username"/>

      <s:set id="addressDeco">
        <s:property value="address"/>
      </s:set>
      <s:set id="projectUrlDeco">
        <s:property value="projectUrl"/>
      </s:set>
      <s:a
        id="logout"
        href="logout.action?address=%{addressDeco}&projectUrl=%{projectUrlDeco}&scmType=%{scmType}&selectedBranch=%{selectedBranch}&repositoryRoot=%{repositoryRoot}"
        title="Logout"
        >
        <span class="fa-stack fa-lg signOutButton">
          <i class="fa fa-sign-out fa-2x"></i>
        </span>
      </s:a>
      </div>
    </s:else>

    <s:set id="scm.submit">
      <s:text name="scm.submit"/>
    </s:set>

    <sj:a
        onBeforeTopics="beforeCommit"
        id="ajaxSaveButton"
        formIds="editForm,commitForm"
        targets="htmlcontentCommit"
        href="%{ajaxCommit}"
        title="%{scm.saveAndContinueTitle}"
        >

      <s:if test="autoSaveInterval > 0">
        <s:submit id="Save" value="%{scm.submit}" onclick="removeLocalAutoSave(); return false;"/>
      </s:if>
      <s:else>
        <s:submit id="Save" value="%{scm.submit}" onclick="return false;"/>
      </s:else>
    </sj:a>

  </form>

  <div id="htmlcontentCommit">
    <s:if test="badLogin">
      <p>
        <font color="red">
          <s:text name="scm.badUsernameOrPassword"/>
        </font>
      </p>
    </s:if>
  </div>
</div>


<!-- popin to open another file -->
<div class="popin" id="openFilePopin">
  <span class="closePopin" onclick="closePopin('openFilePopin')">
    X
  </span>

  <h1><s:text name="scm.openAnotherFile"/></h1>

  <div id="searchTree">

    <s:url id="searchTreeUrl"
           action="browse?address=%{repositoryRoot}&username=%{username}&pw=%{pw}&selectedBranch=%{selectedBranch}&scmType=%{scmType}"/>
    <sjt:tree id="scmTree"
              htmlTitles="true"
              jstreetheme="classic"
              href="%{searchTreeUrl}"
              onClickTopics="treeClicked"
              onSuccessTopics="treeChanged"
      />

  </div>

</div>


<!-- popin to view the file history -->
<div class="popin" id="fileHistoryPopin">
  <span class="closePopin" onclick="closePopin('fileHistoryPopin')">
    X
  </span>

  <h1><s:text name="scm.viewHistory"/></h1>

  <s:url id="ajaxViewRevision1" value="viewRevision.action?rev=1"/>
  <s:url id="ajaxViewRevision2" value="viewRevision.action?rev=2"/>
  <s:url id="ajaxViewDiffs" value="viewDiffs.action"/>

  <s:set id="scm.viewRevision">
    <s:text name="scm.viewRevision"/>
  </s:set>

  <s:set id="scm.compareRevisions">
    <s:text name="scm.compareRevisions"/>
  </s:set>

  <form id="viewHistoryForm">

    <label for="revisionsList"><s:text name="scm.fileRevisionA"/></label><br/>
    <s:select id="revisionsList" name="revision1" list="revisions"/>
    <sj:a
      id="ajaxRev1Button"
      formIds="editForm,viewHistoryForm"
      targets="htmlContentHistory"
      href="%{ajaxViewRevision1}"
      >
      <s:submit id="viewRev1" value="%{scm.viewRevision}"
                onclick="document.getElementById('historyIndicator').style.display = 'inline'; return false;"/>
    </sj:a><br/><br/>

    <label for="revisionsList2"><s:text name="scm.fileRevisionB"/></label><br/>
    <s:select id="revisionsList2" name="revision2" list="revisions"/>
    <sj:a
      id="ajaxRev2Button"
      formIds="editForm,viewHistoryForm"
      targets="htmlContentHistory"
      href="%{ajaxViewRevision2}"
      >
      <s:submit id="viewRev2" value="%{scm.viewRevision}"
                onclick="document.getElementById('historyIndicator').style.display = 'inline'; return false;"/>
    </sj:a><br/><br/>

    <sj:a
      id="ajaxHistoryButton"
      formIds="editForm,viewHistoryForm"
      targets="htmlContentHistory"
      href="%{ajaxViewDiffs}"
      title="%{scm.viewHistory}"
      >

      <s:submit id="viewHistoryButton" value="%{scm.compareRevisions}"
                onclick="document.getElementById('historyIndicator').style.display = 'inline'; return false;"/>

    </sj:a>

      <img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading" class="indicator" id="historyIndicator"/>

  </form>

  <div id="htmlContentHistory"></div>

</div>

<s:if test="format=='rst'">
  <div id="rstInfoPopin" class="popin">
    <span class="closePopin" onclick="closePopin('rstInfoPopin')">
      X
    </span>

    <h4><s:text name="scm.info.ProblemWithRst"/> <a
      href="http://docutils.sourceforge.net/docs/user/rst/quickref.html" target="_blank"><s:text
      name="scm.info.rstWebsite"/></a>.</h4>
  </div>
</s:if>
<s:elseif test="format=='md'">
  <div id="mdInfoPopin" class="popin">
    <span class="closePopin" onclick="closePopin('mdInfoPopin')">
      X
    </span>

    <h4><s:text name="scm.info.ProblemWithMd"/> <a
      href="http://daringfireball.net/projects/markdown/syntax" target="_blank"><s:text
      name="scm.info.mdWebsite"/></a>.</h4>
  </div>
</s:elseif>

<!-- popin to choose how to open the SVG file -->
<div id="svgPopin" class="popin">
    <span class="closePopin" onclick="closePopin('svgPopin')">
      X
    </span>

    <s:set id="scm.openSvgEdit">
      <s:text name="scm.openSvgEdit"/>
    </s:set>

    <s:set id="scm.openSvgView">
      <s:text name="scm.openSvgView"/>
    </s:set>

    <h4><s:text name="scm.svgFile"/></h4>
    <p>
      <s:text name="scm.openSvgFile"/>
    </p>
    <s:submit id="openSvgEdit" value="%{scm.openSvgEdit}" onclick="openSvg('edit.action');"/>
    <s:submit id="openSvgView" value="%{scm.openSvgView}" onclick="openSvg('viewImage.action');"/>
</div>


<div id="popinBackground"></div>

<script type="text/javascript">
  resizeEditor();
  editor.mirror.refresh();
</script>

<s:if test="autoSaveInterval > 0">
  <script type="text/javascript">

    var address = document.getElementById("address").value;

    if (localStorage.getItem(address) != null) {
      var loadSavedContent = confirm('<s:text name="scm.modificationViewer.loadAutoSavedFile"/>');

      if (loadSavedContent) {
        editor.mirror.setValue(localStorage.getItem(address));
      } else {
        removeLocalAutoSave();
      }
    }

  </script>
</s:if>

</body>
</html>
