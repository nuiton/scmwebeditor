<%--
  #%L
  ScmWebEditor UI web
  %%
  Copyright (C) 2009 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="sweVersion">
  <a href="http://scmwebeditor.nuiton.org/s" target="_blank">SCMWebEditor</a>
  &nbsp;
  <a href="https://forge.nuiton.org/projects/scmwebeditor/files" target="_blank"><s:property value="sweVersion"/></a>
</div>
<div id="footerLinks">
  <a href="http://www.gnu.org/licenses/agpl.html" title="License" target="_blank">License</a>
  &nbsp;|&nbsp;
  <a href="https://forge.nuiton.org/projects/scmwebeditor/issues" title="Rapport de Bug" target="_blank">Rapport de
    Bug</a>
  &nbsp;|&nbsp;
  <a href="http://list.nuiton.org/cgi-bin/mailman/listinfo/scmwebeditor-users" title="Support utilisateur"
     target="_blank">Support utilisateur</a>
</div>
<div id="copyright">&#169;2004-2017&nbsp;<a href="http://www.codelutin.com" title="Code Lutin" target="_blank">Code
  Lutin</a></div>
