<%--
  #%L
  ScmWebEditor
  %%
  Copyright (C) 2009 - 2015 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>

<%@page contentType="text/html"
        pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><s:text name="scm.fileNotEditable"/></title>
  <link rel="icon" href="img/edit-icon.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<a target="_blank" href="http://scmwebeditor.nuiton.org/"><img
  src="img/ScmWebEditor_main.png" alt="$alt"/></a>

<p><s:text name="scm.file"/> <s:property value="address"/> <s:text name="scm.notEditable"/></p>

<p><s:text name="scm.canDownload"/>
  <s:if test="fileDirectlyAccessible">
    <a href="<%=request.getAttribute("address")%>">
  </s:if>
  <s:else>
    <a href="downloadFile.action?address=<%=request.getAttribute("address")%>&repositoryRoot=<%=request.getAttribute("repositoryRoot")%>&scmType=<%=request.getAttribute("scmType")%>">
  </s:else>

  <s:text name="scm.clickHere"/></a>.</p>

<p>
  <s:text name="scm.back"/> <a href="checkout.action"><s:text name="scm.homepage"/></a>.
</p>

<div id="footer">
  <jsp:include page="footer.jsp"/>
</div>
</body>
</html>
