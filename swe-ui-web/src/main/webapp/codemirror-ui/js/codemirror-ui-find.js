/*
 * #%L
 * ScmWebEditor UI web
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * @author jgreen
 */
var cursor = null;

function setupFindReplace(){
    document.getElementById('closeButton').onclick = closeWindow;
    document.getElementById('findButton').onclick = find;
    document.getElementById('replaceButton').onclick = replace;
	document.getElementById('replaceAllButton').onclick = replaceAll;
    document.getElementById('replaceFindButton').onclick = replaceFind;
}

function closeWindow(){
    codeMirrorUI.searchWindow = null;
    window.close();
}

function find(){
    var findString = document.getElementById('find').value;
    if (findString == null || findString === '') {
        alert('You must enter something to search for.');
        return;
    }

	if(document.getElementById('regex').checked){
		findString = new RegExp(findString);
	}

	cursor = codeMirrorUI.mirror.getSearchCursor(findString, true);
    var found = moveCursor(cursor);

	//if we didn't find anything, let's check to see if we should start from the top
	if(!found && document.getElementById('wrap').checked){
		cursor = codeMirrorUI.mirror.getSearchCursor(findString, false);
		found = moveCursor(cursor);
	}

	if(found){
		cursor.select();
	}else{
		alert("No instances found. (Maybe you need to enable 'Wrap Search'?)");
	}

}

function moveCursor(cursor){
	var found = false;
	if( getFindDirection() === "forward" ){
		found = cursor.findNext();
    }else{
		found = cursor.findPrevious();
	}
	return found;
}


function getFindDirection(){
    var dRadio = document.forms[0].elements['direction'];

    for (var i = 0; i < dRadio.length; i++) {
        if (dRadio[i].checked) {
            return dRadio[i].value;
        }
    }
    
    return 'no-value?';
}


function replaceAll(){
	var cursor = codeMirrorUI.mirror.getSearchCursor(document.getElementById('find').value, false);
    while (cursor.findNext()) {
      cursor.replace(document.getElementById('replace').value);
    }
}


function replace(){
    cursor.replace(document.getElementById('replace').value);
    setTimeout(window.focus, 100);
}

function replaceFind(){
    replace();
    find();
}
