/*
 * #%L
 * ScmWebEditor UI web
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* Demonstration of embedding CodeMirror in a bigger application. The
* interface defined here is a mess of prompts and confirms, and
* should probably not be used in a real project.
*/

function CodeMirrorUI(place, options, mirrorOptions) {
  this.initialize(place, options, mirrorOptions);
}

CodeMirrorUI.prototype = {

  initialize: function(textarea, options, mirrorOptions) {
    var defaultOptions = {
      searchMode: 'popup', // other options are 'inline' and 'dialog'.  The 'dialog' option needs work.
      imagePath: 'images/silk',
      path: 'js',
      buttons: ['search', 'undo', 'redo', 'jump', 'reindentSelection', 'reindent','about'],
      saveCallback: function() {}
    };
    this.textarea = textarea;
    this.options = options;
    this.setDefaults(this.options, defaultOptions);

    this.buttonDefs = {
      'save': ["Save", "save", this.options.imagePath + "/page_save.png", this.save],
      'search': ["Search/Replace", "find_replace_popup", this.options.imagePath + "/find.png", this.find_replace_popup],
      'searchClose': ["Close", "find_replace_popup_close", this.options.imagePath + "/cancel.png", this.find_replace_popup_close],
      'searchDialog': ["Search/Replace", "find_replace_window", this.options.imagePath + "/find.png", this.find_replace_window],
      'undo': [i18n["undo"], "undo", this.options.imagePath + "/arrow_undo.png", this.undo],
      'redo': [i18n["redo"], "redo", this.options.imagePath + "/arrow_redo.png", this.redo],
      'jump': [i18n["jumpToLine"], "jump", this.options.imagePath + "/page_go.png", this.jump],
      'reindentSelection': [i18n["reformatSelection"], "reindentSelect", this.options.imagePath + "/text_indent.png", this.reindentSelection],
      'reindent': [i18n["reformatDocument"], "reindent", this.options.imagePath + "/page_refresh.png", this.reindent],
      'about': ["About CodeMirror-UI", "about", this.options.imagePath + "/help.png", this.about]
    };

    //place = CodeMirror.replace(place)

    this.home = document.createElement("div");
    this.home.id = "editorHeader";
    this.textarea.parentNode.insertBefore(this.home, this.textarea);
    this.self = this;

    var onChange = this.editorChanged.cmuiBind(this);
    // preserve custom onChange handler
    if (mirrorOptions.onChange) {
        mirrorOptions.oldOnChange = mirrorOptions.onChange;
        mirrorOptions.onChange = function() {
            mirrorOptions.oldOnChange();
            onChange();
        };
    } else {
        mirrorOptions.onChange = onChange;
    }
    mir = CodeMirror.fromTextArea(this.textarea, mirrorOptions);
    mir.on("change", mirrorOptions.onChange);
    this.mirror = mir;

    this.initButtons();

    if (this.options.searchMode === 'inline') {
      this.initFindControl();
    } else if (this.options.searchMode === 'popup') {
      this.initPopupFindControl();
    }

    if (this.saveButton) {
      this.addClass(this.saveButton,'inactive');
    }
    if (this.undoButton) {
      this.addClass(this.undoButton,'inactive');
    }
    if (this.redoButton) {
      this.addClass(this.redoButton,'inactive');
    }
  },
  setDefaults: function(object, defaults) {
    for (var option in defaults) {
      if (!object.hasOwnProperty(option)) {
        object[option] = defaults[option];
      }
    }
  },
  toTextArea: function() {
    this.home.parentNode.removeChild(this.home);
    this.mirror.toTextArea();
  },
  initButtons: function() {
    this.buttonFrame = document.createElement("div");
    this.buttonFrame.className = "codemirror-ui-clearfix codemirror-ui-button-frame";
    this.home.appendChild(this.buttonFrame);

    this.buttonList = document.createElement("ul");
    this.buttonList.id = "editorMenu";
    this.buttonList.className = "buttonsGroup";
    this.buttonFrame.appendChild(this.buttonList);

    var undoRedoContainer = document.createElement("li");
    this.buttonList.appendChild(undoRedoContainer);
    this.undoRedo = document.createElement("ul");
    this.undoRedo.className = "buttonsGroup";
    undoRedoContainer.appendChild(this.undoRedo);
    this.buttonList.appendChild(undoRedoContainer);

    var jumpContainer = document.createElement("li");
    this.buttonList.appendChild(jumpContainer);
    this.jump = document.createElement("ul");
    this.jump.className = "buttonsGroup";
    jumpContainer.appendChild(this.jump);
    this.buttonList.appendChild(jumpContainer);

    var indentContainer = document.createElement("li");
    this.buttonList.appendChild(indentContainer);
    this.indent = document.createElement("ul");
    this.indent.className = "buttonsGroup";
    indentContainer.appendChild(this.indent);
    this.buttonList.appendChild(indentContainer);

    for (var i = 0; i < this.options.buttons.length; i++) {
      var buttonId = this.options.buttons[i];
      var buttonDef = this.buttonDefs[buttonId];
      this.addButton(buttonDef[0], buttonDef[1], buttonDef[2], buttonDef[3], this.buttonList);
    }
  },
  createFindBar: function() {
    var findBar = document.createElement("div");
    findBar.className = "codemirror-ui-find-bar";

    this.findString = document.createElement("input");
    this.findString.type = "text";
    this.findString.size = 8;

    this.findButton = document.createElement("input");
    this.findButton.type = "button";
    this.findButton.value = i18n["find"];
    this.findButton.onclick = function() { this.find(); }.cmuiBind(this);

    this.connect(this.findString, "keyup", function(e) {
      var code = e.keyCode;
      if (code === 13) {
        this.find(this.mirror.getCursor(false));
      } else {
        if(!this.findString.value === "") {
          this.find(this.mirror.getCursor(true));
        }
      }
      this.findString.focus();

    }.cmuiBind(this) );

    var regLabel = document.createElement("label");
    regLabel.title = i18n["regularExpressions"];
    this.regex = document.createElement("input");
    this.regex.type = "checkbox";
    this.regex.className = "codemirror-ui-checkbox";
    regLabel.appendChild(this.regex);
    regLabel.appendChild(document.createTextNode("RegEx"));

    var caseLabel = document.createElement("label");
    caseLabel.title = i18n["caseSensitive"];
    this.caseSensitive = document.createElement("input");
    this.caseSensitive.type = "checkbox";
    this.caseSensitive.className = "codemirror-ui-checkbox";
    caseLabel.appendChild(this.caseSensitive);
    caseLabel.appendChild(document.createTextNode("A/a"));

    this.replaceString = document.createElement("input");
    this.replaceString.type = "text";
    this.replaceString.size = 8;

    this.connect(this.replaceString, "keyup", function(e) {
      var code = e.keyCode;
      if (code === 13){
        this.replace();
      }
    }.cmuiBind(this) );

    this.replaceButton = document.createElement("input");
    this.replaceButton.type = "button";
    this.replaceButton.value = i18n["replace"];
    this.replaceButton.onclick = this.replace.cmuiBind(this);

    var replaceAllLabel = document.createElement("label");
    replaceAllLabel.title = i18n["replaceAll"];
    this.replaceAll = document.createElement("input");
    this.replaceAll.type = "checkbox";
    this.replaceAll.className = "codemirror-ui-checkbox";
    replaceAllLabel.appendChild(this.replaceAll);
    replaceAllLabel.appendChild(document.createTextNode(i18n["all"]));

    findBar.appendChild(this.findString);
    findBar.appendChild(this.findButton);
    findBar.appendChild(caseLabel);
    findBar.appendChild(regLabel);

    findBar.appendChild(this.replaceString);
    findBar.appendChild(this.replaceButton);
    findBar.appendChild(replaceAllLabel);
    return findBar;
  },
  createLanguageSelector: function() {

    var languageSelector = document.createElement("div");
    languageSelector.id = "languageSelector";

    var label = document.createElement("label");
    languageSelector.appendChild(label);

    var text = document.createTextNode(i18n["language"] + " ");
    label.appendChild(text);

    var select = document.createElement("select");
    select.id = "language";
    select.name = "langageSelection";
    select.onchange = function() {
      changeModeBy(editor, this);
    };
    label.appendChild(select);

    var optText = document.createElement("option");
    optText.value = "null";
    var optTextContent = document.createTextNode("Text");
    optText.appendChild(optTextContent);
    select.appendChild(optText);

    var optRst = document.createElement("option");
    optRst.value = "rst";
    var optRstContent = document.createTextNode("ReStructuredText");
    optRst.appendChild(optRstContent);
    select.appendChild(optRst);

    var optMd = document.createElement("option");
    optMd.value = "markdown";
    var optMdContent = document.createTextNode("Markdown");
    optMd.appendChild(optMdContent);
    select.appendChild(optMd);

    var optJs = document.createElement("option");
    optJs.value = "javascript";
    var optJsContent = document.createTextNode("Javascript");
    optJs.appendChild(optJsContent);
    select.appendChild(optJs);

    var optHtml = document.createElement("option");
    optHtml.value = "text/html";
    var optHtmlContent = document.createTextNode("HTML");
    optHtml.appendChild(optHtmlContent);
    select.appendChild(optHtml);

    var optXml = document.createElement("option");
    optXml.value = "xml";
    var optXmlContent = document.createTextNode("XML");
    optXml.appendChild(optXmlContent);
    select.appendChild(optXml);

    var optJava = document.createElement("option");
    optJava.value = "text/x-java";
    var optJavaContent = document.createTextNode("JAVA");
    optJava.appendChild(optJavaContent);
    select.appendChild(optJava);

    var optCss = document.createElement("option");
    optCss.value = "css";
    var optCssContent = document.createTextNode("CSS");
    optCss.appendChild(optCssContent);
    select.appendChild(optCss);

    var optLatex = document.createElement("option");
    optLatex.value = "stex";
    var optLatexContent = document.createTextNode("LaTeX");
    optLatex.appendChild(optLatexContent);
    select.appendChild(optLatex);

    return languageSelector;
  },
  initPopupFindControl: function() {
    var findBar = this.createFindBar();

    this.popupFindWrap = document.createElement("div");
    this.popupFindWrap.className = "codemirror-ui-popup-find-wrap";

    this.popupFindWrap.appendChild(findBar);

    var buttonDef = this.buttonDefs['searchClose'];
    this.addButton(buttonDef[0], buttonDef[1], buttonDef[2], buttonDef[3], this.popupFindWrap);

    this.buttonFrame.appendChild(this.popupFindWrap);

  },
  initFindControl: function() {
    var findBar = this.createFindBar();
    this.buttonFrame.appendChild(findBar);

    var languageSelector = this.createLanguageSelector();
    this.buttonFrame.appendChild(languageSelector);
  },
  find: function( start ) {
    var isCaseSensitive = this.caseSensitive.checked;
    if(start == null){
      start = this.mirror.getCursor();
    }
    var findString = this.findString.value;
    if (findString == null || findString === '') {
      alert('You must enter something to search for.');
      return;
    }
    if (this.regex.checked) {
      findString = new RegExp(findString, !isCaseSensitive ? "i" : "");
    }

    this.cursor = this.mirror.getSearchCursor(findString, start, !isCaseSensitive );
    var found = this.cursor.findNext();
    if (found) {
      this.mirror.setSelection(this.cursor.from(),this.cursor.to());
    } else {
      if (confirm("No more matches.  Should we start from the top?")) {
        this.cursor = this.mirror.getSearchCursor(findString, 0, !isCaseSensitive);
        found = this.cursor.findNext();
        if (found) {
          this.mirror.setSelection(this.cursor.from(),this.cursor.to());
        } else {
          alert("No matches found.");
        }
      }
    }
  },
  replace: function() {
  	var findString = this.findString.value,
  	replaceString = this.replaceString.value,
  	isCaseSensitive = this.caseSensitive.checked,
  	isRegex = this.regex.checked,
  	regFindString = isRegex ? new RegExp(findString, !isCaseSensitive ? "i" : "") : "";

    if (this.replaceAll.checked) {
      var cursor = this.mirror.getSearchCursor(isRegex ? regFindString : findString, 0, !isCaseSensitive);
      while (cursor.findNext()) {
        this.mirror.replaceRange(
            isRegex ? cursor.pos.match[0].replace(regFindString, replaceString) : replaceString
            ,cursor.from(),cursor.to());
      }
    } else {
      this.mirror.replaceRange(
        isRegex ? this.cursor.pos.match[0].replace(regFindString, replaceString) : replaceString
        ,this.cursor.from(),this.cursor.to());
      this.find();
    }
  },
  initWordWrapControl: function() {
    var wrapDiv = document.createElement("div");
    wrapDiv.className = "codemirror-ui-wrap";

    var label = document.createElement("label");

    this.wordWrap = document.createElement("input");
    this.wordWrap.type = "checkbox";
    this.wordWrap.checked = true;
    label.appendChild(this.wordWrap);
    label.appendChild(document.createTextNode("Word Wrap"));
    this.wordWrap.onchange = this.toggleWordWrap.cmuiBind(this);
    wrapDiv.appendChild(label);
    this.buttonFrame.appendChild(wrapDiv);
  },
  toggleWordWrap: function() {
    if (this.wordWrap.checked) {
      this.mirror.setTextWrapping("nowrap");
    } else {
      this.mirror.setTextWrapping("");
    }
  },
  addButton: function(name, action, image, func, frame) {
    var button = document.createElement("a");
    button.className = "codemirror-ui-button " + action;
    button.title = name;
    button.func = func.cmuiBind(this);
    button.onclick = function(event) {

      //normalize for IE
      event = event ? event : window.event;
      if (typeof event.target === 'undefined') {
        var target = event.srcElement;
      } else {
      	var target = event.target;
      }
      target.parentElement.func();
      return false;
    }
    .cmuiBind(this, func);

    var li = document.createElement("li");

    var span = document.createElement("span");
    span.className = "fa-stack fa-lg";
    span.func = func.cmuiBind(this);
    button.appendChild(span);
    li.appendChild(button);

    if (action === 'save') {
      this.saveButton = button;

    } else if (action === 'undo') {

      this.undoButton = button;
      var i = document.createElement("i");
      i.className = "fa fa-undo fa-2x";
      span.appendChild(i);
      this.undoRedo.appendChild(li);

    } else if (action === 'redo') {

      this.redoButton = button;
      var i = document.createElement("i");
      i.className = "fa fa-repeat fa-2x";
      span.appendChild(i);
      this.undoRedo.appendChild(li);

    } else if (action === 'jump') {

      var i = document.createElement("i");
      i.className = "fa fa-file fa-stack-2x";
      span.appendChild(i);

      var i2 = document.createElement("i");
      i2.className = "fa fa-arrow-down fa-stack-1x fa-inverse";
      span.appendChild(i2);
      this.jump.appendChild(li);

    } else if (action === 'reindentSelect') {

      var i = document.createElement("i");
      i.className = "fa fa-indent fa-stack-2x";
      span.appendChild(i);
      this.indent.appendChild(li);

    } else if (action === 'reindent') {

      var i = document.createElement("i");
      i.className = "fa fa-file fa-stack-2x";
      span.appendChild(i);

      var i2 = document.createElement("i");
      i2.className = "fa fa-refresh fa-stack-1x fa-inverse";
      span.appendChild(i2);
      this.indent.appendChild(li);

    }
  },
  classNameRegex: function(className) {
    var regex = new RegExp("(.*) *" + className + " *(.*)");
    return regex;
  },
  addClass: function(element, className) {
    if (!element.className.match(this.classNameRegex(className))) {
       element.className += " " + className;
    }
  },
  removeClass: function(element, className) {
    if (element && element.className) {
      var m = element.className.match(this.classNameRegex(className));
      if (m) {
        element.className = m[1] + " " + m[2];
      }
    }
  },
  editorChanged: function() {
    if(!this.mirror) {
      return;
    }
    var his = this.mirror.historySize();
    if (his['undo'] > 0) {
//      this.removeClass(this.saveButton, 'inactive');
      this.removeClass(this.undoButton, 'inactive');
    } else {
//      this.addClass(this.saveButton, 'inactive');
      this.addClass(this.undoButton, 'inactive');
    }
    if (his['redo'] > 0) {
      this.removeClass(this.redoButton, 'inactive');
    } else {
      this.addClass(this.redoButton, 'inactive');
    }
  },
  save: function() {
    this.options.saveCallback();
    this.addClass(this.saveButton, 'inactive');
  },
  getValue: function() {
	return this.mirror.getValue();
  },
  undo: function() {
    this.mirror.undo();
  },
  redo: function() {
    this.mirror.redo();
  },
  replaceSelection: function(newVal) {
    this.mirror.replaceSelection(newVal);
    this.searchWindow.focus();
  },
  raise_search_window: function() {
    this.searchWindow.focus();
  },
  find_replace_window: function() {
    if (this.searchWindow == null) {
      this.searchWindow = window.open(this.options.path + "find_replace.html", "mywindow", "scrollbars=1,width=400,height=350,modal=yes");
      this.searchWindow.codeMirrorUI = this;
    }
    this.searchWindow.focus();
  },
  find_replace_popup: function() {
    this.popupFindWrap.className = "codemirror-ui-popup-find-wrap active";
    this.findString.focus();
  },
  find_replace_popup_close: function() {
    this.popupFindWrap.className = "codemirror-ui-popup-find-wrap";
  },
  jump: function() {
    var line = prompt("Jump to line:", "");
    if (line && !isNaN(Number(line))) {
      this.mirror.setCursor(Number(line) - 1,0);
      this.mirror.setSelection({line:Number(line) - 1,ch:0},{line:Number(line),ch:0});
      this.mirror.focus();
    }
  },
  reindent: function() {
    var lineCount = this.mirror.lineCount();
    for(var line = 0; line < lineCount; line++) {
      this.mirror.indentLine(line);
    }
  },
  about : function() {
    string = "CodeMirror-UI was written by Jeremy Green (http://www.octolabs.com/) as a light interface around CodeMirror by Marijn Haverbeke (http://codemirror.net).";
    string += "\n\n";
    string += "Documentation and the code can be found at https://github.com/jagthedrummer/codemirror-ui/.";
    alert(string);
  },
  reindentSelection: function() {
    this.mirror.getCursor()
    var start = this.mirror.getCursor(true)["line"];
    var end = this.mirror.getCursor(false)["line"];
    for(var line = start; line <= end; line++) {
      this.mirror.indentLine(line);
    }

  },
  // Event handler registration. If disconnect is true, it'll return a
  // function that unregisters the handler.
  // Borrowed from CodeMirror + modified
  connect: function (node, type, handler, disconnect) {

    if (typeof node.addEventListener === "function") {
      node.addEventListener(type, handler, false);
      if (disconnect) {
        return function() {
          node.removeEventListener(type, handler, false);
        };
      }
    } else {
      node.attachEvent("on" + type, handler);
      if (disconnect) {
        return function() {
          node.detachEvent("on" + type, handler);
        };
      }
    }
  }
};

/*
 * This makes coding callbacks much more sane
 */
Function.prototype.cmuiBind = function(scope) {
  var _function = this;

  return function() {
    return _function.apply(scope, arguments);
  };
};
