/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


// preview
var lastValue = "";

// updates the preview content
function updatePreview() {

    if (lastValue !== editor.getValue()) {

        lastValue = editor.getValue();

        $.post("preview.action", {
            scmType: $("#scmType").val(),
            username: $("#username").val(),
            pw: $("#pw").val(),
            projectUrl: $("#projectUrl").val(),
            commitMessage: $("#commitMessage").val(),
            format: $("#format").val(),
            mimeType: $("#mimeType").val(),
            newText: lastValue,
            langageSelection: $("#language").val(),
            address: $("#address").val(),
            origText: $("#origText").val(),
            scmEditorUrl: $("#scmEditorUrl").val()
        }, function(data) {
            $("#htmlcontentPreview").html(data);
            resizeEditor();
        });
    }
}

// change the preview's position
function changePreviewPos(selectedPos) {

    var newEditorHeight = editorHeight();

    var $editor = $(".CodeMirror-wrap");
    var $preview = $("#htmlcontentPreview");
    var sideButton = document.getElementById("previewSideButton");
    var belowButton = document.getElementById("previewBelowButton");
    var noneButton = document.getElementById("previewNoneButton");

    var minWidth = window.innerWidth * 0.2; // 20% of the window's width
    var maxWidth = window.innerWidth * 0.8; // 80% of the window's width

    var editorBodyHeight = document.getElementById("editorBody").offsetHeight;
    var sweMenuHeight = document.getElementById("sweMenu").offsetHeight;
    var editorHeaderHeight = document.getElementById("editorHeader").offsetHeight;

    var minHeight, maxHeight;

    if (editorBodyHeight === sweMenuHeight + editorHeaderHeight) {
        minHeight = $editor.height() * 0.2;
        maxHeight = $editor.height() * 0.8;
    } else {
        minHeight = (editorBodyHeight - sweMenuHeight - editorHeaderHeight) * 0.2;
        maxHeight = (editorBodyHeight - sweMenuHeight - editorHeaderHeight) * 0.8;
    }

    if (selectedPos === "none") {
        $editor.css("width", "100%");
        $editor.css("height", newEditorHeight + "px");

        $preview.hide();

        sideButton.className = "";
        belowButton.className = "";
        noneButton.className = "previewPosSelected";

        if ($editor.resizable("instance") !== undefined) {
            $editor.resizable("destroy");
        }
    } else {
        $preview.show();

        if (selectedPos === "side") {
            $editor.css("width", "50%");
            $editor.css("height", newEditorHeight + "px");
            $preview.css("width", "50%");
            $preview.css("height", newEditorHeight + "px");

            sideButton.className = "previewPosSelected";
            belowButton.className = "";
            noneButton.className = "";

            if ($editor.resizable("instance") !== undefined) {
                $editor.resizable("destroy");
            }
            $editor.resizable({
                alsoResizeReverse: "#htmlcontentPreview",
                handles: "e",
                minWidth: minWidth,
                maxWidth: maxWidth
            });
        } else {
            $editor.css("width", "100%");
            $editor.css("height", (newEditorHeight / 2) + "px");
            $preview.css("width", "100%");
            $preview.css("height", (newEditorHeight / 2) + "px");

            sideButton.className = "";
            belowButton.className = "previewPosSelected";
            noneButton.className = "";

            if ($editor.resizable("instance") !== undefined) {
                $editor.resizable("destroy");
            }

            $editor.resizable({
                alsoResizeReverse: "#htmlcontentPreview",
                handles: "s",
                minHeight: minHeight,
                maxHeight: maxHeight
            });
        }
    }
}

/* make the jQuery UI "alsoResize" function resize another element in the opposite direction */
$.ui.plugin.add("resizable", "alsoResizeReverse", {

	start: function() {
		var that = $(this).resizable( "instance" ),
			o = that.options;

		$(o.alsoResizeReverse).each(function() {
			var el = $(this);
			el.data("ui-resizable-alsoresizereverse", {
				width: parseInt(el.width(), 10), height: parseInt(el.height(), 10),
				left: parseInt(el.css("left"), 10), top: parseInt(el.css("top"), 10)
			});
		});
	},

	resize: function(event, ui) {
		var that = $(this).resizable( "instance" ),
			o = that.options,
			os = that.originalSize,
			op = that.originalPosition,
			delta = {
				height: (that.size.height - os.height) || 0,
				width: (that.size.width - os.width) || 0,
				top: (that.position.top - op.top) || 0,
				left: (that.position.left - op.left) || 0
			};

			$(o.alsoResizeReverse).each(function() {
				var el = $(this), start = $(this).data("ui-resizable-alsoresizereverse"), style = {},
					css = el.parents(ui.originalElement[0]).length ?
							[ "width", "height" ] :
							[ "width", "height", "top", "left" ];

				$.each(css, function(i, prop) {
					var sum = (start[prop] || 0) - (delta[prop] || 0);
					if (sum && sum >= 0) {
						style[prop] = sum || null;
					}
				});

				el.css(style);
			});
	},

	stop: function() {
		$(this).removeData("resizable-alsoresizereverse");
	}
});


$(document).ready(function() {

    // the preview is updated every 2 seconds if something was changed since last update
    window.setInterval(updatePreview, 2000);

    updatePreview();

    var minWidth = window.innerWidth * 0.2; // 20% of the window's width
    var maxWidth = window.innerWidth * 0.8; // 80% of the window's width

    $(".CodeMirror-wrap").resizable({
        alsoResizeReverse: "#htmlcontentPreview",
        handles: "e",
        minWidth: minWidth,
        maxWidth: maxWidth
    });
});
