/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

var exitAfterCommit = false;
var previousWindowHeight = window.innerHeight;

window.onresize = function() {

    var currentWindowHeight = window.innerHeight;

    if (currentWindowHeight !== previousWindowHeight) {
        previousWindowHeight = currentWindowHeight;
        resizeEditor();
    }
};

/* Changes the value of the new text */
function loadChange() {
    document.getElementById('newTextId').value = editor.getValue();
}

/* Exits the edit page if asked */
function checkExit() {
    if (exitAfterCommit) {
        document.location.href = "checkout.action";
    }
}

/* Gives the best height for the editor */
function editorHeight() {

    var windowHeight = window.innerHeight;
    var headHeight = document.getElementById("head").offsetHeight;
    var sweMenuHeight = document.getElementById("sweMenu").offsetHeight;
    var editorHeaderHeight = document.getElementById("editorHeader").offsetHeight;
    var footerHeight = document.getElementById("footer").offsetHeight;

    var lastAutoSave = document.getElementById("lastAutoSave");
    if (lastAutoSave !== null) {
        if (lastAutoSave.style.display === "" || lastAutoSave.style.display === "none") {
            footerHeight += document.getElementById("sweVersion").offsetHeight;
        }
    }

    var newHeight = windowHeight - headHeight - sweMenuHeight - editorHeaderHeight - footerHeight - 5;

    return newHeight;
}

/* Resizes the editor to use all the available height */
function resizeEditor() {

    var newHeight = editorHeight();

    var preview = document.getElementById("htmlcontentPreview");
    if (preview !== null) {
        var previewBelowButton = document.getElementById("previewBelowButton");
        var previewBelow = (previewBelowButton.classList.length > 0);

        if (previewBelow) {
            newHeight /= 2;
        }

        preview.style.height = newHeight + "px";
    }

    editor.mirror.getWrapperElement().style.height = newHeight + "px";

    if (preview !== null) {

        var posButtonSelected = $(".previewPosSelected").attr("id");
        var posSelected = "none";

        if (posButtonSelected === "previewSideButton") {
            posSelected = "side";
        } else if (posButtonSelected === "previewBelowButton") {
            posSelected = "below";
        }

        changePreviewPos(posSelected);
    }
}

/* Asks the user if he really wants to exit when changes are made */
function confirmExit() {
    var redirect = true;

    if (document.getElementById('newTextId').value == editor.getValue()) {
        redirect = cancelRedirect(i18n["exitJavascript"], document.getElementById('projectUrl'));
    } else {
        document.location.href = document.getElementById('projectUrl').value;
    }

    return redirect;
}

/* Asks the user if he really wants to close the page when changes are made */
function confirmExitOnUnload() {
    if (document.getElementById('newTextId').value != editor.getValue()) {
        return i18n["exitJavascript"];
    }
}

/* Commit form validation */
$.subscribe("beforeCommit", function(event, data) {
    var commitMessage = document.getElementById("commitMessage").value;
    var continueProcess = true;

    if (commitMessage == "") {
        continueProcess = confirm(i18n["commitWithoutMessage"]);
    }

    event.originalEvent.options.submit = continueProcess;

    if (continueProcess) {
        var message = document.getElementById("htmlcontentCommit");
        message.innerHTML = '<img src="struts/js/jstree/themes/classic/throbber.gif" alt="loading"/>';
        loadChange();
        checkExit();
    }
});

// automatically expand the directory when there is no other file
$.subscribe('treeChanged', function(event, data) {

    var json = event.originalEvent.data.responseJSON;

    if (json.length == 1) {
        var object = json[0];

        if (object.state == "closed") {
            var htmlObject = document.getElementById(object.id);
            var children = htmlObject.children;
            children.item('ins').click();
        }
    }
});
