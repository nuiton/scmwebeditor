/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

function autoSave() {

    if (document.getElementById('newTextId').value !== editor.getValue()) {

        var address = document.getElementById("address").value;
        var text = editor.getValue();

        localStorage.setItem(address, text);

        document.getElementById("lastAutoSave").style.display = "block";

        var currentDate = new Date();

        var day = currentDate.getDate();
        if (day < 10) {
            day = "0" + day;
        }

        var month = currentDate.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }

        var year = currentDate.getFullYear();

        var hours = currentDate.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }

        var minutes = currentDate.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        var seconds = currentDate.getSeconds();
        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        document.getElementById("lastAutoSaveTime").innerHTML = day + "/" + month + "/" + year + " " + hours + ":"
            + minutes + ":" + seconds;
    }
}

function removeLocalAutoSave() {

    var address = document.getElementById("address").value;
    localStorage.removeItem(address);
    document.getElementById("lastAutoSave").style.display = "none";
}
