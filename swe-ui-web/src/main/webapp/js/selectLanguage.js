/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


function contains(text, test) {

    if (text.indexOf(test) === -1) {
    	return false;
    } else {
    	return true;
    }
}



function changeModeBy(CodeMirrorEditor, select){
	CodeMirrorEditor.mirror.setOption("mode",select.value);
}


/* select a language for the editor's syntax highlighting */
function selectLanguage(mimetype, format) {
	
	var typeEditor = null;
	
	if(typeEditor === null && mimetype !== null) {

        if(contains(mimetype, "rst") ) {
            typeEditor="rst";
            document.getElementById('language').selectedIndex = 1;
        } else if(contains(mimetype, "markdown")) {
            typeEditor="markdown";
            document.getElementById('language').selectedIndex = 2;
        } else if(contains(mimetype, "javascript")) {
            typeEditor="javascript";
            document.getElementById('language').selectedIndex = 3;
        } else if(contains(mimetype, "html") ) {
            typeEditor="text/html";
            document.getElementById('language').selectedIndex = 4;
        } else if( contains(mimetype, "xml") ) {
            typeEditor="xml";
            document.getElementById('language').selectedIndex = 5;
        } else if( contains(mimetype, "java/application") ) {
            typeEditor="text/x-java";
            document.getElementById('language').selectedIndex = 6;
        } else if(contains(mimetype, "text/css") ) {
            typeEditor="css";
            document.getElementById('language').selectedIndex = 7;
        } else if(contains(mimetype, "x-tex") ) {
            typeEditor="text/stex";
            document.getElementById('language').selectedIndex = 8;
        }
    }
    
    if (typeEditor === null && format !== null) {

        if (format === "rst") {
	        typeEditor="rst";
	
	        document.getElementById('language').selectedIndex = 1;
        
        } else if(format === "md") {
            typeEditor="markdown";

            document.getElementById('language').selectedIndex = 2;

        } else if(format === "javascript") {
        	typeEditor="javascript";
    
        	document.getElementById('language').selectedIndex = 3;
                            
        } else if(format === "html") {
        	typeEditor="text/html";
     
        	document.getElementById('language').selectedIndex = 4;
      
        } else if(format === "xml") {
        	typeEditor="xml";
     
        	document.getElementById('language').selectedIndex = 5;

        } else if(format === "java") {
        	typeEditor="text/x-java";

        	document.getElementById('language').selectedIndex = 6;

        } else if(format === "css") {
       	    typeEditor="css";
         
    	    document.getElementById('language').selectedIndex = 7;
                        
        } else if(format === "tex") {
            typeEditor="text/stex";
             
            document.getElementById('language').selectedIndex = 8;

        }
    }
}
