/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

function getBranchesList() {

    var listDiv = $("#branches");
    var listSelect = $("#branchesList");
    var address = $("#addressInput").val();
    var scmType = $("#scmType").val();
    var username = $("#username").val();
    var pw = $("#pw").val();
    var branchesSupported = false;
    var branches;

    $.getJSON("getBranches.action?address=" + address + "&scmType=" + scmType +
                                        "&username=" + username + "&pw=" + pw, function(result) {

        branchesSupported = result.scmSupportsBranches;
        branches = result.branches;

        if (branchesSupported) {
            listSelect.find("option:gt(0)").remove();
            $.each(branches, function(i, field) {
                listSelect.append("<option>" + field + "</option>");
            });
            listDiv.show();
        } else {
            listDiv.hide();
        }
    });
}


$(document).ready(function() {

    $("#branchesList").on("change", function() {

        var $searchButton = $("#ajaxSearchButton");
        var $selectedBranch = $("#selectedBranch");
        var selectedBranchName = $("#branchesList").val();

        $selectedBranch.val(selectedBranchName);
        $searchButton.trigger("click");
    });
});
