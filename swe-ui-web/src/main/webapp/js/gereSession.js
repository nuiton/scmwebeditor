/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

function rappelSession(minutes)
{
   var msg='Your session expired on ' + minutes + ' minutes';
   if (minutes > 1) {
       msg+='s';
   }
   msg+='.\nDo you want to reload this page ?(You will not lose your data)';

   if(confirm(msg)) {
        location.reload();
   }
}

/**
 * affiche une alerte sur l'expiration de la session et redirige vers une autre page
 */
function expirationSession()
{
   alert("Your session has expired. Please relogin");
   window.history.back(-1);
}

/**
 * affiche des avertissements sur le délai d'expiration de la session :
 * - un premier au bout de (expiration-rappel) minutes (par ex. : 20-3 = 17 minutes)
 * - un second au bout de (expiration) minutes (par ex. : 20 minutes)
 */
function geresession(expiration, rappel)
{
   // affichage du rappel
   setTimeout('rappelSession('+rappel+')', (expiration-rappel)*60*1000);

   // une fois le rappel affiché, on avertit uniquement de l'expiration
   setTimeout('expirationSession()', (expiration)*60*1000);
}
