/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The first action called when entering the website
 */
public class ScmWebEditorMainAction extends AbstractScmWebEditorAction implements ServletResponseAware {


    private static final long serialVersionUID = 8361035067228171624L;

    private static final Log log = LogFactory.getLog(ScmWebEditorMainAction.class);

    public static final String NO_PARAMETER = "noParameter";

    public static final String EDIT_PAGE = "editPage";

    /** the repository's address */
    protected String address;

    /** the URL to the repository's root */
    protected String projectUrl;

    /** the format of the edited file */
    protected String format;

    /** the original file's content */
    protected String origText;

    /** the edited file's MIME type */
    protected String mimeType;

    /** equals true if the authentication information has to be saved in a cookie */
    protected boolean saveCookie = true;

    /** equals true if there is a problem during the authentication process */
    protected boolean badLogin;

    /** the number of the edited file's revision */
    protected String numRevision;

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;

    /** the HTTP response to send to the client */
    protected transient HttpServletResponse response;

    /** the repository's unique identifier */
    protected String repositoryId;

    /** equals true if the client comes from the login page */
    protected boolean fromLoginPage;

    public String getMimeType() { return mimeType; }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getOrigText() {
        return origText;
    }

    public String getProjectUrl() {
        return projectUrl;
    }

    public String getNumRevision() {
        return numRevision;
    }

    public void setProjectUrl(String projectUrl) {
        this.projectUrl = projectUrl;
    }

    public void setFromLoginPage(boolean fromLoginPage) {
        this.fromLoginPage = fromLoginPage;
    }

    public boolean isBadLogin() {
        return badLogin;
    }

    public List<String> getSupportedScmsName() { return Lists.newArrayList(ScmWebEditorConfig.getProviders().keySet()); }

    /**
     * Checks whether the parameters are empty
     * @return true if the parameters are empty
     */
    private boolean testParameters() {
        if (address == null || address.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Execution of the main action
     * @return a code interpreted in the file struts.xml
     */
    @Override
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("Connection to SCMWebEditor\n");
        }


        if (!fromLoginPage && projectUrl == null) {
            projectUrl = request.getHeader("referer");
        }


        if (log.isDebugEnabled()) {
            log.debug("ProjectUrl= " + projectUrl);
        }

        // if there is no parameter, the user is redirected to the browse page
        if (testParameters()) {
            return NO_PARAMETER;
        } else {
            return ERROR_PATH;
        }

    }


    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }


}
