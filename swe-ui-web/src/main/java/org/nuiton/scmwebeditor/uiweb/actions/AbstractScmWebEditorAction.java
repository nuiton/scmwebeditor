/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import com.opensymphony.xwork2.ActionContext;
import info.monitorenter.cpdetector.io.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.nuiton.scmwebeditor.uiweb.ScmSession;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;
import org.nuiton.web.struts2.BaseAction;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

/**
 * Base for all the other actions
 */
public abstract class AbstractScmWebEditorAction extends BaseAction implements ServletRequestAware {

    private static final Log log = LogFactory.getLog(AbstractScmWebEditorAction.class);


    public static final String AUTH_ERROR = "authError";

    public static final String ERROR_PATH = "errorPath";

    protected static final String ENCODING = "UTF-8";

    /** the session which stores all the users' authentication information */
    protected Map<String, Object> session;

    /** the HTTP request received by the server */
    protected transient HttpServletRequest request;

    /** the name of the SCM to use for the given repository */
    protected String scmType;

    private static final long serialVersionUID = 1L;

    final static protected String CONTEXT_ACTION_KEY = "action";

    //TODO-TC200924 : uniformize all this different parameter and attribute, this is a bit messy...
    protected static final String PARAMETER_ADDRESS = "address";

    protected static final String PARAMETER_SCM_EDITOR_URL = "scmEditorUrl";

    protected static final String PARAMETER_PROJECT_URL = "projectUrl";

    protected static final String PARAMETER_FILE_NAME = "file_name";

    protected static final String PARAMETER_LANG = "lang";

    protected static final String PARAMETER_DEFAULT_LANG = "defaultLang";

    protected static final String PARAMETER_FORMAT = "format";

    protected static final String PARAMETER_USERNAME = "username";

    protected static final String PARAMETER_PW = "pw";

    protected static final String PARAMETER_MYTEXT = "Mytext";

    protected static final String PARAMETER_ORIG_TEXT = "Orig_text";

    protected static final String PARAMETER_COMMIT_MESSAGE = "Commit_message";

    protected static final String PARAMETER_TEXT = "text";

    //protected static final String ATTRIBUTE_TEMPDIR = "javax.servlet.context.tempdir";
//    protected static final String ATTRIBUTE_FILE_NAME_URL = "fileName_url";
    protected static final String ATTRIBUTE_REDIRECTION_URL = "Redirection_url";

    //    protected static final String ATTRIBUTE_LANG = "Lang";
    //    protected static final String ATTRIBUTE_FORMAT = "Format";

    protected static final String ATTRIBUTE_ORIG_TEXT = "OrigText";

    protected static final String ATTRIBUTE_INVALIDATE_MAX_TIME = "InvalidateMaxTime";

    protected static final String ATTRIBUTE_LOGIN = "Login";

    protected static final String ATTRIBUTE_IS_LOGIN = "IsLogin";

    protected static final String ATTRIBUTE_BAD_LOGIN = "badLogin";

    protected static final String ATTRIBUTE_PROJECT_URL = "projectUrl";

    protected static final String ATTRIBUTE_SCM_EDITOR_URI = "scmEditorUri";

    protected static final String ATTRIBUTE_PREVIEW_SERVLET_URL = "previewServletUrl";

    protected static final String ATTRIBUTE_FILESEARCH_SERVLET_URL = "searchServletUrl";

    protected static final String ATTRIBUTE_REDIRECT_URL = "Redirect_url";

    protected static final String ATTRIBUTE_PRIVATE_SERVLET_URI = "privateServletUri";

    protected static final String PROPERTIESFILES = "scmwebeditor.properties";

    protected static final String EDITABLESFILES = "editableFiles";

    public String getScmType() { return scmType; }

    public void setScmType(String scmType) { this.scmType = scmType; }

    public HttpServletRequest getRequest() { return request; }

    protected static CodepageDetectorProxy detector;

    protected CodepageDetectorProxy getCodepageDetector() {

        if (detector == null) {
            detector = CodepageDetectorProxy.getInstance(); // A singleton.

            // Add the implementations of info.monitorenter.cpdetector.io.ICodepageDetector:
            // This one is quick if we deal with unicode codepages:
            detector.add(new ByteOrderMarkDetector());
            // The first instance delegated to tries to detect the meta charset attribut in html pages.
            detector.add(new ParsingDetector(true)); // be verbose about parsing.
            // This one does the tricks of exclusion and frequency detection, if first implementation is
            // unsuccessful:
            detector.add(JChardetFacade.getInstance()); // Another singleton.
            detector.add(ASCIIDetector.getInstance()); // Fallback, see javadoc.
        }
        return detector;
    }


    /**
     * Convert all files to UTF-8.
     *
     * @param files files to convert
     */
    protected void convertToUnicode(File... files) {

        CodepageDetectorProxy myDetector = getCodepageDetector();

        for (File file : files) {
            try {
                Charset charset = myDetector.detectCodepage(file.toURI().toURL());

                if (log.isDebugEnabled()) {
                    log.debug("Charset for " + file.getAbsolutePath() + " is " + charset);
                }

                if (charset != null && !charset.name().equalsIgnoreCase(ENCODING)) {

                    if (log.isDebugEnabled()) {
                        log.debug("Convert " + file.getAbsolutePath() + " to unicode");
                    }

                    File tmpFile = File.createTempFile(file.getName(), ".copy");
                    tmpFile.deleteOnExit();

                    // direct copy
                    InputStream is = new FileInputStream(file);
                    OutputStream os = new FileOutputStream(tmpFile);
                    try {
                        IOUtils.copy(is, os);
                    } finally {
                        is.close();
                        os.close();
                    }

                    // copy using cp transaltion
                    is = new FileInputStream(tmpFile);
                    os = new FileOutputStream(file);
                    Reader ir = new InputStreamReader(is, charset);
                    Writer ow = new OutputStreamWriter(new FileOutputStream(file), ENCODING);
                    try {
                        IOUtils.copy(ir, ow);
                    } finally {
                        ir.close();
                        ow.close();
                        is.close();
                        os.close();
                    }

                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("File " + file.getAbsolutePath() + " already in unicode : skip");
                    }
                }
            } catch (MalformedURLException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't convert file in unicode", e);
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't convert file in unicode", e);
                }
            }
        }
    }


    protected String getMimeType(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        String result = null;
        try {
            BodyContentHandler contenthandler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
            AutoDetectParser parser = new AutoDetectParser();
            parser.parse(is, contenthandler, metadata);
            result = metadata.get(Metadata.CONTENT_TYPE);
            if (log.isDebugEnabled()) {
                log.debug("Mime type of " + file.getName() + " is : " + result);
            }

        } catch (SAXException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get MimeType, parsing error", e);
            }
        } catch (TikaException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get MimeType, tika error", e);
            }
        } finally {
            is.close();
        }
        return result;
    }

    protected String getMimeType(String content, String filename) throws IOException {

        InputStream is = new ByteArrayInputStream(content.getBytes(ENCODING));
        String result = null;
        try {
            BodyContentHandler contenthandler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, filename);
            AutoDetectParser parser = new AutoDetectParser();
            parser.parse(is, contenthandler, metadata);
            result = metadata.get(Metadata.CONTENT_TYPE);
            if (log.isDebugEnabled()) {
                log.debug("Mime type of " + filename + " is : " + result);
            }

        } catch (SAXException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get MimeType, parsing error", e);
            }
        } catch (TikaException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get MimeType, tika error", e);
            }
        } finally {
            is.close();
        }
        return result;
    }

    protected String[] getMimeTypes(String content, String filename) throws IOException, SAXException, TikaException {
        InputStream is = new ByteArrayInputStream(content.getBytes(ENCODING));
        try {
            BodyContentHandler contenthandler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            metadata.set(Metadata.RESOURCE_NAME_KEY, filename);
            AutoDetectParser parser = new AutoDetectParser();
            parser.parse(is, contenthandler, metadata);
            String[] result = metadata.getValues(Metadata.CONTENT_TYPE);

            if (log.isDebugEnabled()) {
                log.debug("Mime type of " + filename + " is : " + Arrays.toString(result));
            }

            return result;
        } finally {
            is.close();
        }
    }

    /**
     * Getting the properties from a properties file
     *
     * @param file the file containing the properties
     * @return a Properties object containing the properties from the file
     * @throws IOException
     */
    public static Properties loadProperties(String file) throws IOException {
        Properties properties = new Properties();

        FileInputStream input = new FileInputStream(file);
        try {
            properties.load(input);
            return properties;
        } finally {
            input.close();
        }

    }

    /**
     * Getting the properties from a properties file
     *
     * @param inStream the file containing the properties
     * @return a Properties object containing the properties from the file
     * @throws IOException
     * @throws NullPointerException
     */
    public static Properties loadProperties(InputStream inStream) throws IOException, NullPointerException {
        Properties properties = new Properties();

        properties.load(inStream);
        return properties;


    }


    public ScmSession getScmSession() {
        session = ActionContext.getContext().getSession();
        Object obj = session.get("ScmSession");

        ScmSession scmSession;
        if (obj == null) {
            scmSession = new ScmSession();
            session.put("ScmSession", scmSession);
        } else {
            scmSession = (ScmSession) obj;
        }
        return scmSession;
    }

    /**
     * Reads the information in the session to give the username and the password for a given repository if necessary
     * @param repositoryUUID the ID of the repository to connect to
     * @param username the currently known username: the session won't be read if it is has a value
     * @param pw the currently known password: the session won't be read if it is has a value
     * @return the username to use at position [0] ; the password to use at position [1]
     */
    protected String[] getUsernamePwFromSession(String repositoryUUID, String username, String pw) {

        String[] usernamePw = new String[2];
        usernamePw[0] = username;
        usernamePw[1] = pw;

        if (username == null || pw == null) {

            String login = getScmSession().getUsername(repositoryUUID);
            String password = getScmSession().getPassword(repositoryUUID);

            if (login != null && password != null) {

                // getting the authentication information in session
                usernamePw[0] = login;
                usernamePw[1] = password;
            }
        } else {
            if (username.equals("") || pw.equals("")) {

                String login = getScmSession().getUsername(repositoryUUID);
                String password = getScmSession().getPassword(repositoryUUID);

                if (login != null && password != null) {

                    // getting the authentication information in session
                    usernamePw[0] = login;
                    usernamePw[1] = password;
                }
            } else {
                getScmSession().addScmUser(repositoryUUID, username, pw);
            }
        }

        return usernamePw;
    }

    public String getUsername(String url) {
        return getScmSession().getUsername(url);
    }

    public String getPassword(String url) {
        return getScmSession().getPassword(url);
    }

    public String getSweVersion() { return ScmWebEditorConfig.getVersion(); }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }


}
