/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Gets the image at the given path
 */
public class GetImageAction extends ScmWebEditorMainAction {

    private static final Log log = LogFactory.getLog(GetImageAction.class);

    /** the bytes coding the image */
    protected byte[] imageInByte = null;

    /** the path to the image */
    protected String imagePath;

    /** the HTTP request sent to the server */
    protected HttpServletRequest servletRequest;


    public byte[] getImageInByte() { return imageInByte; }

    public void setImageInByte(byte[] imageInByte) {

        this.imageInByte = new byte[0];

        if (imageInByte != null){
            this.imageInByte = Arrays.copyOf(imageInByte, imageInByte.length);
        }
    }

    public String getImagePath() { return imagePath; }

    public void setImagePath(String imagePath) { this.imagePath = imagePath; }


    public String execute() {
        return SUCCESS;
    }

    public byte[] getCustomImageInBytes() {


        HttpSession session = servletRequest.getSession();
        String sessionId = session.getId();

        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        if (imagePath.startsWith(pathToLocalRepos)) {

            BufferedImage originalImage;

            try {
                File fileImage = new File(imagePath);
                String fileFormat = imagePath.substring(imagePath.lastIndexOf('.') + 1);

                if (fileFormat.toLowerCase().equals("svg")) {

                    Path path = Paths.get(imagePath);
                    imageInByte = Files.readAllBytes(path);

                } else {

                    originalImage = ImageIO.read(fileImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    ImageIO.write(originalImage, fileFormat, baos);

                    baos.flush();

                    imageInByte = baos.toByteArray();

                    baos.close();
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not get image file at " + imagePath, e);
                }
            }
        }

        byte[] imageInByteCopy = new byte[0];

        if (imageInByte != null) {
            imageInByteCopy = Arrays.copyOf(imageInByte, imageInByte.length);
        }

        return imageInByteCopy;
    }

    public String getCustomContentType() {

        String type = "image/jpeg";
        String path =  imagePath.toLowerCase();

        if (path.endsWith(".png")) {
            type = "image/png";
        } else if (path.endsWith(".gif")) {
            type = "image/gif";
        } else if (path.endsWith(".svg")) {
            type = "image/svg+xml";
        }

        return type;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        servletRequest = request;
    }
}
