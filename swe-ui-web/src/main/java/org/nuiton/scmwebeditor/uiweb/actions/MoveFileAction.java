/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmFileManager;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.dto.MoveFileDto;
import org.nuiton.scmwebeditor.api.dto.result.MoveFileResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.Normalizer;

/**
 * Moves a file in the repository
 */
public class MoveFileAction extends AbstractScmWebEditorAction {

    private static final long serialVersionUID = 4244339447567114412L;

    public static final String REDIRECT = "redirect";

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;

    /** the repository's address */
    protected String address;

    /** the URL the root of the repository */
    protected String scmRoot;

    /** equals true if there is a problem during the authentication process */
    protected boolean badLogin;

    /** equals true if an error occurs */
    protected boolean error;

    /** the full path to the root */
    protected String scmPath;

    /** the full path of the root */
    protected String fileRoot;

    /** the full path to the file to move */
    protected String fileToMove;

    /** the full path of the destination directory */
    protected String destinationDirectory;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public boolean isBadLogin() {
        return badLogin;
    }

    public boolean isError() {
        return error;
    }

    public String getScmRoot() {
        return scmRoot;
    }

    public void setScmRoot(String scmRoot) { this.scmRoot = scmRoot; }

    public void setBadLogin(boolean badLogin) { this.badLogin = badLogin; }

    public void setError(boolean error) { this.error = error; }

    public String getScmPath() { return scmPath; }

    public void setScmPath(String scmPath) { this.scmPath = scmPath; }

    public String getFileRoot() { return fileRoot; }

    public void setFileRoot(String fileRoot) { this.fileRoot = fileRoot; }

    public String getFileToMove() { return fileToMove; }

    public void setFileToMove(String fileToMove) { this.fileToMove = fileToMove; }

    public String getDestinationDirectory() { return destinationDirectory; }

    public void setDestinationDirectory(
            String destinationDirectory) { this.destinationDirectory = destinationDirectory; }

    /**
     * Execution of the move a file action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        HttpSession session = request.getSession();
        String sessionId = session.getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);

        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);
        ScmFileManager scmFileManager = provider.getFileManager(scmConn);

        // if the repository is not protected for writing, we get its UUID
        if (address.endsWith("/")) {
            address = address.substring(0, address.lastIndexOf('/'));
        }

        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        MoveFileDto dto = new MoveFileDto();
        dto.setUsername(username);
        dto.setPassword(pw);
        dto.setScmPath(scmPath);
        dto.setFileToMove(fileToMove);
        dto.setDestinationDirectory(destinationDirectory);

        MoveFileResultDto resultDto = scmFileManager.moveFile(dto);

        if (resultDto.getScmRoot() != null) {
            scmRoot = resultDto.getScmRoot();
        }
        if (resultDto.getFileRoot() != null) {
            fileRoot = resultDto.getFileRoot();
        }

        if (username != null && pw != null) {
            if (username.equals("") && pw.equals("")) {
                username = null;
                pw = null;
            }
        }


        if (resultDto.getError() != null) {

            String errorMessage = resultDto.getError();
            error = true;

            if (errorMessage.equals(MoveFileResultDto.CONNECTION_FAILED)) {

                getScmSession().delScmUser(scmConn.getRepositoryId());
                username = null;
                pw = null;

                return ERROR;

            } else if (errorMessage.equals(MoveFileResultDto.AUTH_ERROR)) {

                badLogin = true;
                username = null;
                pw = null;
                getScmSession().delScmUser(scmConn.getRepositoryId());

                return LOGIN;

            } else if (errorMessage.equals(MoveFileResultDto.REDIRECT)) {

                error = false;

                return REDIRECT;
            } else {

                return ERROR;
            }
        }

        return SUCCESS;
    }
}
