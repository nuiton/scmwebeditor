/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.scmwebeditor.api.OperationNotSupportedException;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import java.util.List;

/**
 * Gives a list of the existing branches on the repository
 */
public class ListBranchesAction extends AbstractScmWebEditorAction {

    private static final Log log = LogFactory.getLog(ListBranchesAction.class);

    /** the repository's address */
    protected String address;

    /** equals true if the SCM is able to use branches */
    protected boolean scmSupportsBranches;

    /** list of the existing branches on the repository */
    protected List<String> branches;

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;


    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public boolean isScmSupportsBranches() { return scmSupportsBranches; }

    public void setScmSupportsBranches(boolean scmSupportsBranches) { this.scmSupportsBranches = scmSupportsBranches; }

    public List<String> getBranches() { return branches; }

    public void setBranches(List<String> branches) { this.branches = branches; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPw() { return pw; }

    public void setPw(String pw) { this.pw = pw; }


    /**
     * Execution of the list branches action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        if (username == null) {
            username = "anonymous";
        }
        if (pw == null) {
            pw = "anonymous";
        }

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);

        scmSupportsBranches = provider.supportsBranches();

        try {
            branches = provider.listBranches(address, username, pw);
        } catch (OperationNotSupportedException e) {
            if (log.isDebugEnabled()) {
                log.debug("The SCM " + scmType + " does not support branches", e);
            }
        }

        return SUCCESS;
    }
}
