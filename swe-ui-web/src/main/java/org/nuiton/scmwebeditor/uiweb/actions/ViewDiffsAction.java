/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;

/**
 * Allows to view the history of a file
 */
public class ViewDiffsAction extends ScmWebEditorMainAction {

    private static final Log log = LogFactory.getLog(ViewDiffsAction.class);

    /** the content of the file */
    protected String fileContent;

    /** one of the revisions to use to get the differences */
    protected String revision1;

    /** the other revision to use to get the differences */
    protected String revision2;

    /** the error name or null if no error occurred */
    protected String error;


    public String getFileContent() { return fileContent; }

    public void setFileContent(String fileContent) { this.fileContent = fileContent; }

    public String getRevision1() { return revision1; }

    public void setRevision1(String revision1) { this.revision1 = revision1; }

    public String getRevision2() { return revision2; }

    public void setRevision2(String revision2) { this.revision2 = revision2; }

    public String getError() { return error; }

    public void setError(String error) { this.error = error; }

    /**
     * Execution of the view history action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        HttpSession session = request.getSession();
        String sessionId = session.getId();

        error = null;

        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // if the repository is not protected, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        if (log.isDebugEnabled()) {
            log.debug("Login : " + username);
        }


        /*
        * Reading the cookie
        */


        String usernamepwCookie = null;
        // read the cookies

        BlowfishCipherService bf = new BlowfishCipherService();

        byte[] privateKey = Base64.decode(ScmWebEditorConfig.getKey());

        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    usernamepwCookie = c.getValue();
                }
            }
        }

        if (usernamepwCookie != null) {

            String usernameDecode = null;
            try {
                usernameDecode = new String(bf.decrypt(Base64.decode(usernamepwCookie), privateKey).getBytes(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not create a String with UTF-8 encoding");
                }
            }

            if (usernameDecode != null) {
                String[] resCookie = usernameDecode.split(",");
                if (resCookie.length == 2) {
                    username = resCookie[0];
                    pw = resCookie[1];
                }
            }
        }

        if (saveCookie) {
            if (username != null && pw != null) {

                if (!username.equals("") && !pw.equals("")) {
                    Cookie authCookie = new Cookie(repositoryUUID, bf.encrypt((username + "," + pw).getBytes(), privateKey).toBase64());
                    authCookie.setMaxAge(60 * 60 * 24 * 365);
                    response.addCookie(authCookie);
                }
            }

        }

        // authentication
        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        String name = username;
        String password = pw;

        if (name == null) {
            name = "anonymous";
        }
        if (password == null) {
            password = "anonymous";
        }


        /*
        * Getting the differences
        */

        try {
            File tempFile = scmConn.getDiffs(address, name, password, revision1, revision2);

            if (tempFile != null) {
                fileContent = FileUtils.readFileToString(tempFile);
            } else {
                error = ERROR;
                return ERROR;
            }
        } catch (AuthenticationException e) {
            request.setAttribute(PARAMETER_ADDRESS, address);

            // if scm authentication failed user is redirected on login page
            if (log.isDebugEnabled()) {
                log.debug("Auth Fail ", e);
            }

            // deleting the cookies for this repository
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    c.setMaxAge(0);//On supprime le cookie
                    response.addCookie(c);
                    if (log.isDebugEnabled()) {
                        log.debug("Cookie supprimé");
                    }
                }
            }

            getScmSession().delScmUser(repositoryUUID);
            error = LOGIN;
            return LOGIN;
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not read temp file for " + address, e);
            }
            error = ERROR;
            return ERROR;
        }

        return SUCCESS;
    }
}
