/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.SweInternalException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Allows to access to the configured options
 */
public class ScmWebEditorConfig {

    private static final Log log = LogFactory.getLog(ScmWebEditorConfig.class);
    public static final String CONFIG_CODE_MIRROR_VERSION_PROPERTY = "swe.codeMirror.version";

    /** this application's configuration */
    protected static ApplicationConfig config;


    /**
     * Gives the configuration options for this application
     * @param args the arguments to use when creating the configuration
     * @return the configuration to use for this application
     */
    public static ApplicationConfig getConfig(String... args) {
        if (config == null) {
            synchronized (ScmWebEditorConfig.class) {
                if (config == null) {
                    config = new ApplicationConfig(ScmWebEditorConfigOption.CONFIG_FILE.getDefaultValue());
                    try {
                        config.parse(args);
                    } catch (ArgumentsParserException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Error when parsing ApplicationConfig", e);
                        }
                    }
                }
            }
        }
        return config;
    }

    public static List<String> getEditableFiles() {
        return getConfig().getOptionAsList(ScmWebEditorConfigOption.EDITABLE_FILES.getKey()).getOption();
    }

    public static String getLocalRepositoriesPath() {

        String key = ScmWebEditorConfigOption.LOCAL_REPOSITORIES_PATH.getKey();
        return getConfig().getOption(key);
    }

    public static Map<String, String> getProviders() {

        Map<String, String> providers = new HashMap<String, String>();

        String key = ScmWebEditorConfigOption.PROVIDERS.getKey();
        Properties options = getConfig().getOptionStartsWith(key);

        for (String propertyName : options.stringPropertyNames()) {

            String providerKey = propertyName.substring(propertyName.lastIndexOf('.') + 1);
            String providerClass = getConfig().getOption(propertyName);

            providers.put(providerKey, providerClass);
        }

        return providers;
    }

    public static String getKey() {
        return getConfig().getOption(ScmWebEditorConfigOption.COOKIES_PRIVATE_KEY.getKey());
    }

    public static String getVersion() {
        return getConfig().getOption(ScmWebEditorConfigOption.VERSION.getKey());
    }

    public static String getCodeMirrorVersion() {
        return getConfig().getOption(CONFIG_CODE_MIRROR_VERSION_PROPERTY);
    }

    public static int getAutoSaveInterval() {
        String readValue = getConfig().getOption(ScmWebEditorConfigOption.AUTO_SAVE_INTERVAL.getKey());
        return Integer.parseInt(readValue);
    }

    /**
     * Gives an instance of the provider for the given SCM type
     * @param scmType the type of SCM to use
     * @return the provider for the given SCM
     */
    public static ScmProvider getProvider(String scmType) {

        Map<String, String> providers = ScmWebEditorConfig.getProviders();
        String providerClassName = providers.get(scmType);
        ScmProvider provider = null;

        if (providerClassName != null) {
            try {

                Class<?> providerClass = Class.forName(providerClassName);
                provider = (ScmProvider) providerClass.newInstance();

            } catch (ClassNotFoundException e) {

                if (log.isErrorEnabled()) {
                    log.error("Can not find SCM '" + scmType + "'", e);
                }
                throw new SweInternalException("Can not find SCM '" + scmType + "'", e);

            } catch (InstantiationException e) {

                if (log.isErrorEnabled()) {
                    log.error("Can not instantiate class " + providerClassName, e);
                }
                throw new SweInternalException("Can not instantiate class '" + providerClassName + "'", e);

            } catch (IllegalAccessException e) {

                if (log.isErrorEnabled()) {
                    log.error("Can not access to class " + providerClassName, e);
                }
                throw new SweInternalException("Can not access to class " + providerClassName, e);

            }
        } else {
            throw new SweInternalException("Can not find the provider '" + scmType + "' in the configuration file");
        }

        return provider;
    }


}
