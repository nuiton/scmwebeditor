/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import com.jgeppert.struts2.jquery.tree.result.TreeNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.dto.BrowseDto;
import org.nuiton.scmwebeditor.api.dto.result.BrowseResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Allows to browse through a repository
 */
public class BrowseAction extends AbstractScmWebEditorAction implements ServletResponseAware {

    private static final long serialVersionUID = 4432027215087932750L;

    private static final Log log = LogFactory.getLog(BrowseAction.class);

    public static final String ROOT = "root";

    /** the repository's address */
    protected String address;

    /** the username to connect with */
    protected String username;

    /** the password to connect with */
    protected String pw;

    /** equals true when an error occured */
    protected boolean error;

    /** the nodes for the tree showing the repository's files and directories */
    protected List<TreeNode> nodes = new ArrayList<TreeNode>();

    /** the SCM's identifier */
    protected String id = "";

    /** the selected branch's name */
    protected String selectedBranch;

    /** the name of the head branch on the SCM */
    protected String headBranchName;

    /** equals true if the SCM is able to use branches */
    protected boolean scmSupportsBranches;

    /** the HTTP response to send to the client */
    protected transient HttpServletResponse response;


    public boolean getError() { return error; }

    public void setError(boolean error) { this.error = error; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPw() { return pw; }

    public void setPw(String pw) { this.pw = pw; }

    public String getId() { return id; }

    public String getHeadBranchName() { return headBranchName; }

    public String getSelectedBranch() { return selectedBranch; }

    public void setSelectedBranch(String selectedBranch) { this.selectedBranch = selectedBranch; }

    public void setHeadBranchName(String headBranchName) { this.headBranchName = headBranchName; }

    public boolean isScmSupportsBranches() { return scmSupportsBranches; }

    public void setScmSupportsBranches(boolean scmSupportsBranches) { this.scmSupportsBranches = scmSupportsBranches; }

    public List<TreeNode> getNodes() { return nodes; }

    public void setId(String id) { this.id = id; }


    /**
     * Execution of the browse action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("Enter in browse action");
        }

        if (address.endsWith("/")) {
            address = address.substring(0, address.length() - 1);
        }

        if (username == null) {
            username = "anonymous";
        }
        if (pw == null) {
            pw = "anonymous";
        }

        // connection to the repository
        HttpSession session = request.getSession();
        String sessionId = session.getId();

        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        scmSupportsBranches = provider.supportsBranches();

        if (scmConn == null) {
            error = true;
            return ROOT;
        }

        // putting all the parameters into a DTO
        BrowseDto dto = new BrowseDto();
        dto.setUsername(username);
        dto.setPassword(pw);
        dto.setId(id);
        dto.setAddress(address);
        dto.setSelectedBranch(selectedBranch);

        BrowseResultDto resultDto = scmConn.browse(dto);

        // getting the results from the DTO
        if (resultDto.getHeadBranchName() != null) {
            headBranchName = resultDto.getHeadBranchName();
        }
        if (resultDto.getSelectedBranch() != null) {
            selectedBranch = resultDto.getSelectedBranch();
        }

        String dtoError = resultDto.getError();

        if (dtoError != null) {

            if (!dtoError.equals(BrowseResultDto.ROOT)) {
                error = true;
            }

            if (dtoError.equals(BrowseResultDto.AUTH_ERROR)) {
                return AUTH_ERROR;
            } else if (dtoError.equals(BrowseResultDto.ROOT) || dtoError.equals(BrowseResultDto.ERROR)) {
                return ROOT;
            }
        }


        // building the tree
        if (resultDto.getFiles() != null) {
            for (String file : resultDto.getFiles()) {
                TreeNode node = new TreeNode();
                node.setId(file);
                node.setTitle(file.substring(file.lastIndexOf("/") + 1));
                node.setState(TreeNode.NODE_STATE_LEAF);

                String fileName = file.toLowerCase();

                if (fileName.endsWith((".js"))) {
                    node.setIcon("ui-icon-js");
                } else if (fileName.endsWith(".html") || file.toLowerCase().endsWith(".htm")) {
                    node.setIcon("ui-icon-html");
                } else if (fileName.endsWith(".xml")) {
                    node.setIcon("ui-icon-xml");
                } else if (fileName.endsWith(".java")) {
                    node.setIcon("ui-icon-java");
                } else if (fileName.endsWith(".css")) {
                    node.setIcon("ui-icon-css");
                } else if (fileName.endsWith(".rst") || fileName.endsWith(".md")) {
                    node.setIcon("ui-icon-preview");
                } else if (fileName.endsWith(".tex")) {
                    node.setIcon("ui-icon-tex");
                } else if (fileName.endsWith(".txt")) {
                    node.setIcon("ui-icon-txt");
                } else if (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png")
                        || fileName.endsWith(".gif") || fileName.endsWith(".svg")) {
                    node.setIcon("ui-icon-image");
                } else {
                    node.setIcon("ui-icon-document");
                }

                nodes.add(node);
            }
        }

        if (resultDto.getDirectories() != null) {
            for (Map.Entry<String, String> entry : resultDto.getDirectories().entrySet()) {

                String value = entry.getValue();

                TreeNode node = new TreeNode();
                node.setId(value);
                node.setTitle(value.substring(value.lastIndexOf("/") + 1));
                nodes.add(node);
            }
        }


        // if the repository is not protected for writing, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        if (username != null && pw != null) {
            if (!username.equals("anonymous") && !username.equals("") && !pw.equals("anonymous") && !pw.equals("")) {

                BlowfishCipherService bf = new BlowfishCipherService();
                byte[] privateKey = Base64.decode(ScmWebEditorConfig.getKey());

                Cookie authCookie = null;
                try {
                    authCookie = new Cookie(repositoryUUID, bf.encrypt((username + "," + pw).getBytes("UTF-8"), privateKey).toBase64());
                } catch (UnsupportedEncodingException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not get bytes from UTF-8 encoding");
                    }
                }

                if (authCookie != null) {
                    authCookie.setMaxAge(60 * 60 * 24 * 365);
                    response.addCookie(authCookie);
                }

                if (log.isDebugEnabled()) {
                    log.debug("addscmuser uuid == " + repositoryUUID);
                }

                getScmSession().addScmUser(repositoryUUID, username, pw);
            }
        }

        return SUCCESS;
    }


    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }


}
