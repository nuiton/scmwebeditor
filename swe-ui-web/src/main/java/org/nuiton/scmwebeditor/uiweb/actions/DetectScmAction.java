/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import java.util.List;

/**
 * Detection of the SCM to use following the repository's address
 */
public class DetectScmAction extends AbstractScmWebEditorAction {

    private static final Log log = LogFactory.getLog(DetectScmAction.class);

    /** the repository's address */
    protected String address;

    /** the detected SCM for the given address */
    protected String detectedScm;


    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getDetectedScm() { return detectedScm; }

    public void setDetectedScm(String detectedScm) { this.detectedScm = detectedScm; }


    /**
     * Execution of the detect SCM action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        List<String> supportedScms = Lists.newArrayList(ScmWebEditorConfig.getProviders().keySet());

        for (String scm : supportedScms) {

            ScmProvider provider = ScmWebEditorConfig.getProvider(scm);

            if (provider.addressSeemsCompatible(address)) {
                detectedScm = scm;

                return SUCCESS;
            }
        }

        detectedScm = null;

        if (log.isDebugEnabled()) {
            log.debug("Detected SCM '" + detectedScm + "' for address " + address);
        }

        return SUCCESS;
    }
}
