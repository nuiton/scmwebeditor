/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.scmwebeditor.uiweb;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.io.File;
import java.io.IOException;

/**
 * Manages creation and deletion of HTTP sessions
 */
public class SweSessionListener implements HttpSessionListener {

    private static final Log log = LogFactory.getLog(SweSessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

        if (log.isDebugEnabled()) {
            log.debug("Session destroyed");
        }

        String sessionId = httpSessionEvent.getSession().getId();

        String localReposPath = ScmWebEditorConfig.getLocalRepositoriesPath();

        File localDirectory = new File(localReposPath + File.separator + sessionId);

        try {
            FileUtils.deleteDirectory(localDirectory);

            if (log.isDebugEnabled()) {
                log.debug("Deleted directory " + localDirectory.getAbsolutePath());
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not delete directory " + localDirectory.getAbsolutePath(), e);
            }
        }
    }
}
