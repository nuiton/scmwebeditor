/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.nuiton.scmwebeditor.api.OperationNotSupportedException;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.dto.result.AbstractResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;

/**
 * Allows to view an image
 */
public class ViewImageAction extends ScmWebEditorMainAction {

    private static final Log log = LogFactory.getLog(ViewImageAction.class);

    protected static final String VIEW_IMAGE = "viewImage";

    protected static final List<String> SUPPORTED_IMAGE_FORMATS = Arrays.asList("jpg", "jpeg", "png", "gif", "svg");

    /** the name of the selected branch */
    protected String selectedBranch;

    /** equals true if the SCM is able to use branches */
    protected boolean scmSupportsBranches;

    /** the full path to the root of the repository */
    protected String repositoryRoot;

    /** the path to use to get the image displayed in a web page */
    protected String imagePath;

    /** equals true if the repository's files are directly accessible using their address */
    protected boolean filesDirectlyAccessible;


    public String getSelectedBranch() { return selectedBranch; }

    public void setSelectedBranch(String selectedBranch) { this.selectedBranch = selectedBranch; }

    public boolean isScmSupportsBranches() { return scmSupportsBranches; }

    public void setScmSupportsBranches(boolean scmSupportsBranches) { this.scmSupportsBranches = scmSupportsBranches; }

    public String getRepositoryRoot() { return repositoryRoot; }

    public void setRepositoryRoot(String repositoryRoot) { this.repositoryRoot = repositoryRoot; }

    public String getImagePath() { return imagePath; }

    public void setImagePath(String imagePath) { this.imagePath = imagePath; }

    public boolean isFilesDirectlyAccessible() { return filesDirectlyAccessible; }

    public void setFilesDirectlyAccessible(
            boolean filesDirectlyAccessible) { this.filesDirectlyAccessible = filesDirectlyAccessible; }

    /**
     * Execution of the view image action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        format = address.substring(address.lastIndexOf('.') + 1).toLowerCase();

        if (!SUPPORTED_IMAGE_FORMATS.contains(format)) {
            return ERROR_PATH;
        }

        HttpSession session = request.getSession();
        String sessionId = session.getId();

        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        scmSupportsBranches = provider.supportsBranches();
        filesDirectlyAccessible = provider.filesDirectlyAccessible();

        // if the repository is not protected, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        if (repositoryRoot == null) {
            repositoryRoot = address.substring(0, address.lastIndexOf('/'));
        } else if (repositoryRoot.equals("")) {
            repositoryRoot = address.substring(0, address.lastIndexOf('/'));
        }

        if (log.isDebugEnabled()) {
            log.debug("Login : " + username);
        }


        /*
        * Reading the cookie
        */


        String usernamepwCookie = null;
        // read the cookies

        BlowfishCipherService bf = new BlowfishCipherService();

        byte[] privateKey = Base64.decode(ScmWebEditorConfig.getKey());

        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    usernamepwCookie = c.getValue();
                }
            }
        }

        if (usernamepwCookie != null) {

            String usernameDecode = null;
            try {
                usernameDecode = new String(bf.decrypt(Base64.decode(usernamepwCookie), privateKey).getBytes(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not create a String with UTF-8 encoding");
                }
            }

            if (usernameDecode != null) {
                String[] resCookie = usernameDecode.split(",");
                if (resCookie.length == 2) {
                    username = resCookie[0];
                    pw = resCookie[1];
                }
            }
        }

        if (saveCookie) {
            if (username != null && pw != null) {

                if (!username.equals("") && !pw.equals("")) {

                    Cookie authCookie = null;

                    try {
                        authCookie = new Cookie(repositoryUUID, bf.encrypt((username + "," + pw).getBytes("UTF-8"), privateKey).toBase64());
                    } catch (UnsupportedEncodingException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can not get a String from UTF-8 encoding");
                        }
                    }

                    if (authCookie != null) {
                        authCookie.setMaxAge(60 * 60 * 24 * 365);
                        response.addCookie(authCookie);
                    }
                }
            }

        }

        // authentication
        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        String name = username;
        String password = pw;

        if (name == null) {
            name = "anonymous";
        }
        if (password == null) {
            password = "anonymous";
        }

        String changeBranchError = null;

        if (scmSupportsBranches) {
            try {
                changeBranchError = provider.changeBranch(selectedBranch, pathToLocalRepos, username, pw);
            } catch (OperationNotSupportedException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not change branch with SCM '" + scmType + "'");
                }
            }
        }

        if (changeBranchError != null) {
            if (changeBranchError.equals(AbstractResultDto.AUTH_ERROR)) {
                return LOGIN;
            } else {
                return ERROR_PATH;
            }
        }


        /*
        * Getting the file's revision
        */

        try {
            numRevision = scmConn.getHeadRevisionNumber(address, name, password);
        } catch (AuthenticationException e) {
            request.setAttribute(PARAMETER_ADDRESS, address);

            // if scm authentication failed user is redirected on login page
            if (log.isDebugEnabled()) {
                log.debug("Auth Fail ", e);
            }

            // deleting the cookies for this repository
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    c.setMaxAge(0);//On supprime le cookie
                    response.addCookie(c);
                    if (log.isDebugEnabled()) {
                        log.debug("Cookie supprimé");
                    }
                }
            }

            getScmSession().delScmUser(repositoryUUID);
            //redirect to a login page
            return LOGIN;
        } catch (IllegalArgumentException e) {
            if (log.isErrorEnabled()) {
                log.error("The file does not exist", e);
            }
            return ERROR_PATH;
        }

        imagePath = scmConn.getFilePath(address, repositoryRoot, username, pw);


        if (log.isInfoEnabled()) {
            log.info("IP client : " + request.getRemoteAddr() + " , get file : " + address + ". File's mimetype : "
                    + mimeType);
        }

        return VIEW_IMAGE;
    }
}
