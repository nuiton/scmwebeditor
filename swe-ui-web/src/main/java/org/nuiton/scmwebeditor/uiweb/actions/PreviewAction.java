/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import com.github.rjeschke.txtmark.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.dom4j.Document;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.legacy.JRSTReader;

import javax.servlet.http.HttpServletRequest;
import java.io.StringReader;

/**
 * Gives a preview of the edited RST or Markdown file
 */
public class PreviewAction extends AbstractScmWebEditorAction implements ServletRequestAware {

    /** serialVersionUID. */
    private static final long serialVersionUID = -2388759298175611718L;

    private static final Log log = LogFactory.getLog(PreviewAction.class);

    /** the HTTP request sent to the server */
    protected transient HttpServletRequest request;

    /** the HTML code to display for the preview */
    protected String htmlPreview;

    /** the text to preview */
    protected String newText;

    /** the file's format */
    protected String format;

    public String getNewText() { return newText; }

    public void setNewText(String newText) { this.newText = newText; }

    public String getHtmlPreview() { return htmlPreview; }

    public void setHtmlPreview(String htmlPreview) { this.htmlPreview = htmlPreview; }

    public String getFormat() { return format; }

    public void setFormat(String format) { this.format = format; }


    /**
     * Execution of the preview action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("Enter in preview action");
        }

        htmlPreview = "";

        if (format.equals("rst")) {

            // Using jrst to generate html document
            try {
                JRSTReader jrst = new JRSTReader();
                Document doc = jrst.read(new StringReader(newText));
                Document generatedDoc = JRST.generateXml(doc, JRST.TYPE_HTML);
                htmlPreview = generatedDoc.asXML();

                if (log.isDebugEnabled()) {
                    log.debug("RST generate");
                }

            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("RST generate fail", eee);
                }
                htmlPreview = "<h4>Parsing error, please read RST specification<h4>";
            }
        } else if (format.equals("md")) {

            // Using txtmark to generate html document
            if (log.isDebugEnabled()) {
                log.debug("Markdown generate");
            }

            htmlPreview = Processor.process(newText);
        }

        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }


}
