/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.nuiton.scmwebeditor.api.OperationNotSupportedException;
import org.nuiton.scmwebeditor.api.RepositoryNotFoundException;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.dto.CreateBranchDto;
import org.nuiton.scmwebeditor.api.dto.result.AbstractResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.Normalizer;
import java.util.List;

/**
 * Creates a new branch on the repository
 */
public class CreateBranchAction extends AbstractScmWebEditorAction implements ServletRequestAware {

    private static final Log log = LogFactory.getLog(CreateBranchAction.class);

    public static final String REDIRECT = "redirect";

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;

    /** the repository's address */
    protected String address;

    /** equals true if there is a problem during the authentication process */
    protected boolean badLogin;

    /** equals true if an error occurs */
    protected boolean error;

    /** the list of all the repository's branches */
    protected List<String> branches;

    /** the selected branch to make the new branch from */
    protected String selectedBranch;

    /** the name of the new branch */
    protected String newBranchName;


    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPw() { return pw; }

    public void setPw(String pw) { this.pw = pw; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public boolean isBadLogin() { return badLogin; }

    public void setBadLogin(boolean badLogin) { this.badLogin = badLogin; }

    public boolean isError() { return error; }

    public void setError(boolean error) { this.error = error; }

    public List<String> getBranches() { return branches; }

    public void setBranches(List<String> branches) { this.branches = branches; }

    public String getSelectedBranch() { return selectedBranch; }

    public void setSelectedBranch(String selectedBranch) { this.selectedBranch = selectedBranch; }

    public String getNewBranchName() { return newBranchName; }

    public void setNewBranchName(String newBranchName) { this.newBranchName = newBranchName; }

    /**
     * Execution of the create branch action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        HttpSession session = request.getSession();
        String sessionId = session.getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);

        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // if the repository is not protected for writing, we get its UUID
        if (address.endsWith("/")) {
            address = address.substring(0, address.lastIndexOf('/'));
        }

        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        try {
            branches = provider.listBranches(address, username, pw);
        } catch (OperationNotSupportedException e) {
            if (log.isErrorEnabled()) {
                log.error("The SCM " + scmType + " does not support branches", e);
            }
            error = true;

            return ERROR;
        }

        if (selectedBranch == null || newBranchName == null) {

            return REDIRECT;
        }

        CreateBranchDto dto = new CreateBranchDto();
        dto.setAddress(address);
        dto.setNewBranchName(newBranchName);
        dto.setUsername(username);
        dto.setPassword(pw);
        dto.setPathToLocalRepos(pathToLocalRepos);
        dto.setSelectedBranch(selectedBranch);

        String createBranchError;

        try {
            createBranchError = provider.createBranch(dto);
        } catch (OperationNotSupportedException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not create a branch with SCM '" + scmType + "'", e);
            }
            error = true;
            return ERROR;
        } catch (AuthenticationException e) {
            if (log.isErrorEnabled()) {
                log.error("Authentication problem", e);
            }
            badLogin = true;
            username = null;
            pw = null;
            return LOGIN;
        } catch (RepositoryNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not reach repository at address " + address, e);
            }
            error = true;
            return ERROR;
        }

        if (createBranchError != null) {
            if (createBranchError.equals(AbstractResultDto.AUTH_ERROR)) {
                badLogin = true;
                username = null;
                pw = null;
                return LOGIN;
            } else {
                error = true;
                return ERROR;
            }
        }


        return SUCCESS;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }
}
