/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

/**
 * Definition of the configuration to use
 */
public enum ScmWebEditorConfigOption implements ConfigOptionDef {

    CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME, "The file name", "scmwebeditor.properties", String.class, false, false),
    VERSION("swe.version", "The version of SCMWebEditor", "", String.class, false, true),
    EDITABLE_FILES("swe.editableFiles", "description", "Files types that are editable", String.class, true, true),
    COOKIES_PRIVATE_KEY("swe.cookiePrivateKey", "Private key for cookies", null, String.class, true, true),
    LOCAL_REPOSITORIES_PATH("swe.localRepositoriesPath", "The path where the local repositories will be stored", "/var/local/swe", String.class, false, true),
    AUTO_SAVE_INTERVAL("swe.autoSaveInterval", "The time between two automatic saves in milliseconds", "300000", Integer.class, false, true),
    PROVIDERS("swe.provider.", "The SCMs that can be used", null, String.class, false, true);


    private final String key;

    private final String description;

    private String defaultValue;

    private final Class<?> type;

    private boolean _transient;

    private boolean _final;


    ScmWebEditorConfigOption(String key, String description, String defaultValue,
                             Class<?> type, boolean _transient, boolean _final) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this._final = _final;
        this._transient = _transient;
    }


    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return _transient;
    }

    @Override
    public boolean isFinal() {
        return _final;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean isTransient) {
        _transient = isTransient;
    }

    @Override
    public void setFinal(boolean isFinal) {
        _final = isFinal;
    }


}
