/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.nuiton.scmwebeditor.api.OperationNotSupportedException;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.ScmRevision;
import org.nuiton.scmwebeditor.api.dto.result.AbstractResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.LinkedList;
import java.util.Map;

/**
 * Allows to edit a file
 */
public class EditAction extends ScmWebEditorMainAction {

    private static final Log log = LogFactory.getLog(EditAction.class);

    protected static final String NOT_EDITABLE = "notEditable";

    /** the name of the selected branch */
    protected String selectedBranch;

    /** equals true if the SCM is able to use branches */
    protected boolean scmSupportsBranches;

    /** equals true if the SCM allows to choose when the commits are pushed to the server */
    protected boolean scmSupportsPush;

    /** the full path to the root of the repository */
    protected String repositoryRoot;

    /** the interval between two automatic saves */
    protected int autoSaveInterval;

    /** a list of the revision which can be used to view the file's history */
    protected Map<ScmRevision, String> revisions;

    /** one of the revisions to use to get the differences */
    protected String revision1;

    /** the other revision to use to get the differences */
    protected String revision2;

    /** equals true if this file is directly accessible from its address */
    protected boolean fileDirectlyAccessible;


    public String getCodeMirrorVersion() { return ScmWebEditorConfig.getCodeMirrorVersion(); }
    
    public String getSelectedBranch() { return selectedBranch; }

    public void setSelectedBranch(String selectedBranch) { this.selectedBranch = selectedBranch; }

    public boolean isScmSupportsBranches() { return scmSupportsBranches; }

    public void setScmSupportsBranches(boolean scmSupportsBranches) { this.scmSupportsBranches = scmSupportsBranches; }

    public boolean isScmSupportsPush() { return scmSupportsPush; }

    public void setScmSupportsPush(boolean scmSupportsPush) { this.scmSupportsPush = scmSupportsPush; }

    public String getRepositoryRoot() { return repositoryRoot; }

    public void setRepositoryRoot(String repositoryRoot) { this.repositoryRoot = repositoryRoot; }

    public int getAutoSaveInterval() { return autoSaveInterval; }

    public void setAutoSaveInterval(int autoSaveInterval) { this.autoSaveInterval = autoSaveInterval; }

    public Map<ScmRevision, String> getRevisions() { return revisions; }

    public void setRevisions(
            Map<ScmRevision, String> revisions) { this.revisions = revisions; }

    public String getRevision1() { return revision1; }

    public void setRevision1(String revision1) { this.revision1 = revision1; }

    public String getRevision2() { return revision2; }

    public void setRevision2(String revision2) { this.revision2 = revision2; }

    public boolean isFileDirectlyAccessible() { return fileDirectlyAccessible; }

    public void setFileDirectlyAccessible(
            boolean fileDirectlyAccessible) { this.fileDirectlyAccessible = fileDirectlyAccessible; }

    /**
     * Execution of the edit action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        autoSaveInterval = ScmWebEditorConfig.getAutoSaveInterval();

        HttpSession session = request.getSession();
        String sessionId = session.getId();

        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        fileDirectlyAccessible = provider.filesDirectlyAccessible();

        scmSupportsBranches = provider.supportsBranches();
        scmSupportsPush = provider.supportsPush();

        String fileName = scmConn.getFileName();
        format = fileName.substring(fileName.lastIndexOf(".") + 1);

        if ("vm".equals(format)) {

            // Try to get extension before vm one
            fileName = fileName.substring(0, fileName.length()-3);
            format = fileName.substring(fileName.lastIndexOf(".") + 1);

        }

        String originalText = "";

        // if the repository is not protected, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        if (repositoryRoot == null) {
            repositoryRoot = address.substring(0, address.lastIndexOf('/'));
        } else if (repositoryRoot.equals("")) {
            repositoryRoot = address.substring(0, address.lastIndexOf('/'));
        }

        if (log.isDebugEnabled()) {
            log.debug("Login : " + username);
        }


        /*
        * Reading the cookie
        */


        String usernamepwCookie = null;
        // read the cookies

        BlowfishCipherService bf = new BlowfishCipherService();

        byte[] privateKey = Base64.decode(ScmWebEditorConfig.getKey());

        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    usernamepwCookie = c.getValue();
                }
            }
        }

        if (usernamepwCookie != null) {

            String usernameDecode = null;
            try {
                usernameDecode = new String(bf.decrypt(Base64.decode(usernamepwCookie), privateKey).getBytes(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not create a String with UTF-8 encoding");
                }
            }

            if (usernameDecode != null) {
                String[] resCookie = usernameDecode.split(",");
                if (resCookie.length == 2) {
                    username = resCookie[0];
                    pw = resCookie[1];
                }
            }
        }

        if (saveCookie) {
            if (username != null && pw != null) {

                if (!username.equals("") && !pw.equals("")) {

                    Cookie authCookie = null;

                    try {
                        authCookie = new Cookie(repositoryUUID, bf.encrypt((username + "," + pw).getBytes("UTF-8"),
                                privateKey).toBase64());
                    } catch (UnsupportedEncodingException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can not get a String from UTF-8 encoding");
                        }
                    }

                    if (authCookie != null) {
                        authCookie.setMaxAge(60 * 60 * 24 * 365);
                        response.addCookie(authCookie);
                    }
                }
            }

        }

        // authentication
        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        String name = username;
        String password = pw;

        if (name == null) {
            name = "anonymous";
        }
        if (password == null) {
            password = "anonymous";
        }

        String changeBranchError = null;

        if (scmSupportsBranches) {

            try {

                if (selectedBranch == null) {
                    selectedBranch = provider.getDefaultBranchName();
                } else if (selectedBranch.equals("")) {
                    selectedBranch = provider.getDefaultBranchName();
                }

                changeBranchError = provider.changeBranch(selectedBranch, pathToLocalRepos, username, pw);
            } catch (OperationNotSupportedException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not change branch with SCM '" + scmType + "'");
                }
            }
        }

        if (changeBranchError != null) {
            if (changeBranchError.equals(AbstractResultDto.AUTH_ERROR)) {
                return LOGIN;
            } else {
                return ERROR_PATH;
            }
        }

        try {
            revisions = scmConn.getRevisions(address, username, pw);
        } catch (AuthenticationException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not get revisions for address " + address + " : authentication error", e);
            }
        }


        /*
        * Getting the file and its revision
        */

        try {
            File originalFile = scmConn.getFileContent(address, name, password);
            originalText = FileUtils.readFileToString(originalFile);
            numRevision = scmConn.getHeadRevisionNumber(address, name, password);
        } catch (AuthenticationException e) {
            request.setAttribute(PARAMETER_ADDRESS, address);

            // if scm authentication failed user is redirected on login page
            if (log.isDebugEnabled()) {
                log.debug("Auth Fail ", e);
            }

            // deleting the cookies for this repository
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals(repositoryUUID)) {
                    c.setMaxAge(0);//On supprime le cookie
                    response.addCookie(c);
                    if (log.isDebugEnabled()) {
                        log.debug("Cookie supprimé");
                    }
                }
            }

            getScmSession().delScmUser(repositoryUUID);
            //redirect to a login page
            return LOGIN;
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not read content file", e);
            }
        } catch (IllegalArgumentException e) {
            if (log.isErrorEnabled()) {
                log.error("The file does not exist", e);
            }
            return ERROR_PATH;
        }

        mimeType = null;

        try {
            mimeType = getMimeType(originalText, fileName);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get MimeType, problem when reading file", e);
            }
        }


        LinkedList<String> editableFiles = new LinkedList<String>();

        editableFiles.add("text");
        editableFiles.add("xml");
        editableFiles.add("x-java");

        editableFiles.addAll(ScmWebEditorConfig.getEditableFiles());


        boolean editable = false;
        if (mimeType != null) {
            for (String fileType : editableFiles) {
                if (mimeType.matches(".*" + fileType + ".*")) {
                    editable = true;
                }
            }
        }


        // if the file is not of text type, we can't edit it
        if (mimeType == null || !editable) {

            if (log.isErrorEnabled()) {
                log.error("Can't edit this file, mimetype : " + mimeType);
            }
            return NOT_EDITABLE;
        }

        origText = originalText;


        if (log.isInfoEnabled()) {
            log.info("IP client : " + request.getRemoteAddr() + ", get file : " + address + ". File's mimetype : "
                    + mimeType);
        }

        return EDIT_PAGE;
    }
}
