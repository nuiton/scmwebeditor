/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.naming.AuthenticationException;
import java.io.File;
import java.io.IOException;
import java.text.Normalizer;

/**
 * Deletes all the changes made to the edited file
 */
public class ResetAction extends AbstractScmWebEditorAction {

    private static final long serialVersionUID = -1154924826535371319L;

    private static final Log log = LogFactory.getLog(ResetAction.class);

    /** the file's content in the last revision */
    protected String lastRevision;

    /** the number of the current revision */
    protected String numRevision;

    /** the repository's address */
    protected String address;

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;

    /** information about an error if one occurs */
    protected String error;


    public String getLastRevision() { return lastRevision; }

    public String getNumRevision() { return numRevision; }

    public void setAddress(String address) { this.address = address; }

    public void setUsername(String username) { this.username = username; }

    public void setPw(String pw) { this.pw = pw; }

    public String getError() { return error; }

    /**
     * Execution of the reset action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        String sessionId = request.getSession().getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // getting the repository unique identifier if it is possible
        String repositoryId = scmConn.getRepositoryId();
        if (repositoryId == null) {
            repositoryId = address.replace(' ', '_');
            repositoryId = Normalizer.normalize(repositoryId, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }
        String[] usernamePw = getUsernamePwFromSession(repositoryId, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        try {
            File originalFile = scmConn.getFileContent(address, username, pw);
            lastRevision = FileUtils.readFileToString(originalFile);
            numRevision = scmConn.getHeadRevisionNumber(address, username, pw);
        } catch (AuthenticationException e) {
            if (log.isErrorEnabled()) {
                log.error("AUTH FAIL");
            }
            error = AUTH_ERROR;
            return AUTH_ERROR;
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can not read content file", e);
            }
        }

        return SUCCESS;
    }


}
