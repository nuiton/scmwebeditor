/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.Normalizer;

/**
 * Ends the user session on the repository
 */
public class LogoutAction extends AbstractScmWebEditorAction implements ServletRequestAware, ServletResponseAware {

    private static final long serialVersionUID = 6937086747942656369L;

    private static final Log log = LogFactory.getLog(LogoutAction.class);

    /** the repository's address */
    protected String address;

    /** the URL to the repository's root */
    protected String projectUrl;

    /** the HTTP request sent to the server */
    protected transient HttpServletRequest request;

    /** the HTTP response to send to the client */
    protected transient HttpServletResponse response;


    public void setAddress(String address) { this.address = address; }

    public String getAddress() { return address; }

    public String getProjectUrl() { return projectUrl; }

    public void setProjectUrl(String projectUrl) { this.projectUrl = projectUrl; }


    /**
     * Execution of the logout action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        String sessionId = request.getSession().getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // getting the repository unique identifier if it is possible
        String repositoryId = scmConn.getRepositoryId();
        if (repositoryId == null) {
            repositoryId = address.replace(' ', '_');
            repositoryId = Normalizer.normalize(repositoryId, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        // deleting the cookies for this repository
        for (Cookie c : request.getCookies()) {
            if (c.getName().equals(repositoryId)) {
                c.setMaxAge(0);// deleting the cookie
                response.addCookie(c);
                if (log.isDebugEnabled()) {
                    log.debug("Cookie supprimé");
                }
            }
        }

        // deleting the authentication info in session
        getScmSession().delScmUser(repositoryId);

        return SUCCESS;
    }


    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }

}
