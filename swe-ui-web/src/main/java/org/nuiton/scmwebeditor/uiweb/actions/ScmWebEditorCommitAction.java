/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.dom4j.Document;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.legacy.JRSTReader;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.api.dto.CommitDto;
import org.nuiton.scmwebeditor.api.dto.result.CommitResultDto;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;

/**
 * Commits all the changes to the remote repository
 */
public class ScmWebEditorCommitAction extends AbstractScmWebEditorAction implements ServletRequestAware, ServletResponseAware {

    public static final String FILE_MODIFY = "fileModify";

    private static final long serialVersionUID = 6374273568146287730L;

    private static final Log log = LogFactory.getLog(ScmWebEditorCommitAction.class);

    /** the new file content */
    protected String newText;

    /** a message to describe the commit */
    protected String commitMessage;

    /** the old file content */
    protected String origText;

    /** the username to use to connect to the repository */
    protected String username;

    /** the password to use to connect to the repository */
    protected String pw;

    /** the repository's address */
    protected String address;

    /** the last revision's file content */
    protected String lastText;

    /** the edited file's format */
    protected String format;

    /** the URL to the root of the repository */
    protected String projectUrl;

    /** the differences between the current content and the last revision's content */
    protected String diff;

    /** the name of the person who made the last commit */
    protected String headCommiter;

    /** the file's MIME type */
    protected String mimeType;

    /** equals true to force the commit */
    protected boolean force;

    /** equals true if the authentication information has to be saved in a cookie */
    protected boolean saveCookie;

    /** equals true if there has been a problem during the authentication process */
    protected boolean badLogin;

    /** the number of the current revision */
    protected String numRevision;

    /** equals true if only a commit is requested, without a push */
    protected boolean commitOnly;

    /** the HTTP request sent to the server */
    protected transient HttpServletRequest request;

    /** the HTTP response to send to the client */
    protected transient HttpServletResponse response;

    public String getCommitMessage() { return commitMessage; }

    public void setCommitMessage(String commitMessage) { this.commitMessage = commitMessage; }

    public String getNewText() { return newText; }

    public void setNewText(String newText) { this.newText = newText; }

    public String getFormat() { return format; }

    public void setFormat(String format) { this.format = format; }

    public String getOrigText() { return origText; }

    public void setOrigText(String origText) { this.origText = origText; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPw() { return pw; }

    public void setPw(String pw) { this.pw = pw; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getLastText() { return lastText; }

    public void setLastText(String lastText) { this.lastText = lastText; }

    public String getProjectUrl() { return projectUrl; }

    public void setProjectUrl(String projectUrl) { this.projectUrl = projectUrl; }

    public String getNumRevision() { return numRevision; }

    public void setNumRevision(String numRevision) { this.numRevision = numRevision; }

    public String getDiff() { return diff; }

    public void setDiff(String diff) { this.diff = diff; }

    public String getHeadCommiter() { return headCommiter; }

    public void setHeadCommiter(String headCommiter) { this.headCommiter = headCommiter; }

    public boolean isSaveCookie() { return saveCookie; }

    public void setSaveCookie(boolean saveCookie) { this.saveCookie = saveCookie; }

    public String getMimeType() { return mimeType; }

    public void setMimeType(String mimeType) { this.mimeType = mimeType; }

    public HttpServletRequest getRequest() { return request; }

    public boolean getForce() { return force; }

    public void setForce(boolean force) { this.force = force; }

    public String getParameterAddress() { return PARAMETER_ADDRESS; }

    public boolean isBadLogin() { return badLogin; }

    public void setBadLogin(boolean badLogin) { this.badLogin = badLogin; }

    public boolean isCommitOnly() { return commitOnly; }

    public void setCommitOnly(boolean commitOnly) { this.commitOnly = commitOnly; }

    /**
     * Tells whether a RST file has a valid syntax
     * @param rstContent the text to check
     * @return true if the syntax is valid
     */
    protected boolean isRstValid(String rstContent) {
        try {
            JRSTReader jrst = new JRSTReader();
            Document doc = jrst.read(new StringReader(rstContent));
            Document generatedDoc = JRST.generateXml(doc, JRST.TYPE_HTML);
            generatedDoc.asXML();

            if (log.isDebugEnabled()) {
                log.debug("RST generate success");
            }
            return true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("RST generate fail", e);
            }
            return false;
        }


    }

    /**
     * Execution of the commit action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        System.setProperty("file.encoding", "UTF-8");

        if (!force) {
            if (format.equals("rst")) {
                if (!isRstValid(newText)) {
                    return "errorRst";
                }
            }
        }

        // connection to the repository
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // if the repository is not protected for writing, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }


        /*
        * reading the cookie
        */
        String usernamepwCookie = null;
        // read the cookies

        BlowfishCipherService bf = new BlowfishCipherService();

        byte[] privateKey = Base64.decode(ScmWebEditorConfig.getKey());

        for (Cookie c : request.getCookies()) {
            if (c.getName().equals(repositoryUUID))
                usernamepwCookie = c.getValue();
        }


        if (usernamepwCookie != null) {

            String usernameDecode = null;
            try {
                usernameDecode = new String(bf.decrypt(Base64.decode(usernamepwCookie), privateKey).getBytes(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not create a String with UTF-8 encoding");
                }
            }

            if (usernameDecode != null) {
                String[] resCookie = usernameDecode.split(",");
                if (resCookie.length == 2) {
                    username = resCookie[0];
                    pw = resCookie[1];
                }
            }
        }

        if (saveCookie) {
            if (username != null && pw != null) {

                Cookie authCookie = null;

                try {
                    authCookie = new Cookie(repositoryUUID, bf.encrypt((username + "," + pw).getBytes("UTF-8"), privateKey).toBase64());
                } catch (UnsupportedEncodingException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can not get bytes from UTF-8 encoding");
                    }
                }

                if (authCookie != null) {
                    authCookie.setMaxAge(60 * 60 * 24 * 365);
                    response.addCookie(authCookie);
                }
            }

        }

        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        CommitDto dto = new CommitDto();
        dto.setUsername(username);
        dto.setPassword(pw);
        dto.setNewText(newText);
        dto.setCommitMessage(commitMessage);
        dto.setForce(force);
        dto.setAddress(address);
        dto.setCommitOnly(commitOnly);

        CommitResultDto resultDto = scmConn.commit(dto);

        if (resultDto.getLastText() != null) {
            lastText = resultDto.getLastText();
        }
        if (resultDto.getOrigText() != null) {
            origText = resultDto.getOrigText();
        }
        if (resultDto.getDiff() != null) {
            diff = resultDto.getDiff();
        }
        if (resultDto.getDiff() != null) {
            headCommiter = resultDto.getHeadCommiter();
        }
        if (resultDto.getNumRevision() != null) {
            numRevision = resultDto.getNumRevision();
        }


        if (resultDto.getError() != null) {

            String error = resultDto.getError();

            if (error.equals(CommitResultDto.ERROR_PATH)) {
                return ERROR_PATH;
            } else if (error.equals(CommitResultDto.AUTH_ERROR)) {
                request.setAttribute(getParameterAddress(), address);
                getScmSession().delScmUser(scmConn.getRepositoryId());
                username = null;
                pw = null;

                return LOGIN;
            } else if (error.equals(CommitResultDto.FILE_MODIFY)) {
                return FILE_MODIFY;
            } else {
                return ERROR;
            }
        }

        return SUCCESS;
    }


    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }

}
