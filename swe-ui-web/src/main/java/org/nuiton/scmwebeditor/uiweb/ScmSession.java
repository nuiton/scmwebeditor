/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb;

import java.util.HashMap;
import java.util.Map;

/**
 * Allows to manage sessions on a SCM
 */
public class ScmSession {

    /** list of all the users */
    protected Map<String, ScmUser> scmUsers;


    public Map<String, ScmUser> getScmUsers() { return scmUsers; }


    /**
     * Constructor which makes a new map of users
     */
    public ScmSession() {
        scmUsers = new HashMap<String, ScmUser>();
    }

    /**
     * Adds a user to the list of users
     * @param address the repository's address
     * @param login the username to use to connect to the repository
     * @param password the password to use to connect to the repository
     */
    public void addScmUser(String address, String login, String password) {
        scmUsers.put(address, new ScmUser(login, password));
    }

    /**
     * Removes a user from the list of users
     * @param address the repository's address
     */
    public void delScmUser(String address) {
        scmUsers.remove(address);
    }

    /**
     * Gives the username related to the given repository
     * @param url the repository's address
     * @return the known username for the repository
     */
    public String getUsername(String url) {
        if (scmUsers.containsKey(url)) {
            return scmUsers.get(url).getUsername();
        } else {
            return null;
        }

    }

    /**
     * Gives the password related to the given repository
     * @param url the repository's address
     * @return the known password for the repository
     */
    public String getPassword(String url) {
        if (scmUsers.containsKey(url)) {
            return scmUsers.get(url).getPassword();
        } else {
            return null;
        }
    }


}
