/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.scmwebeditor.api.ScmConnection;
import org.nuiton.scmwebeditor.api.ScmProvider;
import org.nuiton.scmwebeditor.uiweb.ScmWebEditorConfig;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.Normalizer;

/**
 * Allows to download the file at the given path
 */
public class DownloadFileAction extends ScmWebEditorMainAction {

    private static final Log log = LogFactory.getLog(DownloadFileAction.class);

    /** the path to the root of the repository */
    protected String repositoryRoot;


    public String getRepositoryRoot() { return repositoryRoot; }

    public void setRepositoryRoot(String repositoryRoot) { this.repositoryRoot = repositoryRoot; }


    /**
     * Execution of the download file action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        HttpSession session = request.getSession();
        String sessionId = session.getId();
        String pathToLocalRepos = ScmWebEditorConfig.getLocalRepositoriesPath() + File.separator + sessionId;

        ScmProvider provider = ScmWebEditorConfig.getProvider(scmType);
        ScmConnection scmConn = provider.getConnection(address, pathToLocalRepos);

        // getting the authentication information
        // if the repository is not protected for writing, we get its UUID
        String repositoryUUID = scmConn.getRepositoryId();
        if (repositoryUUID == null) {
            repositoryUUID = address.replace(' ', '_');
            repositoryUUID = Normalizer.normalize(repositoryUUID, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        }

        String[] usernamePw = getUsernamePwFromSession(repositoryUUID, username, pw);
        username = usernamePw[0];
        pw = usernamePw[1];

        // getting the path to use to download the file
        String filePath = scmConn.getFilePath(address, repositoryRoot, username, pw);
        String filename = filePath.substring(filePath.lastIndexOf('/') + 1);

        File fileToDownload = new File(filePath);

        if (fileToDownload.exists()) {

            // getting the file content
            response.setContentType("application/octet-stream");

            String disHeader = "Attachment; Filename=\"" + filename + "\"";
            response.setHeader("Content-Disposition", disHeader);

            InputStream in;
            ServletOutputStream outs;

            try {
                outs = response.getOutputStream();

                FileInputStream fis = new FileInputStream(fileToDownload);
                in = new BufferedInputStream(fis);

                int ch = 0;
                while (ch != -1) {
                    ch = in.read();
                    outs.print((char) ch);
                }

                outs.flush();
                outs.close();
                in.close();
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can not read file " + fileToDownload, e);
                }
            }
        } else {
            return ERROR;
        }

        return SUCCESS;
    }
}
