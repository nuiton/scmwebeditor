/*
 * #%L
 * ScmWebEditor
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.scmwebeditor.uiweb.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

/**
 * Saves the edited file without quitting
 */
public class SaveAction extends ScmWebEditorCommitAction {

    private static final Log log = LogFactory.getLog(SaveAction.class);

    private static final long serialVersionUID = -115627369699637253L;

    protected static final String USELESS_SAVE = "uselessSave";

    /** the result of the commit action */
    protected String result;

    /** the saving date */
    protected Date date;


    public String getResult() { return result; }

    public void setResult(String result) { this.result = result; }

    public Date getDate() { return date; }


    /**
     * Execution of the save action
     * @return a code interpreted in the file struts.xml
     */
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("originText : " + origText);
            log.debug("newText : " + newText);
        }


        if (origText.equals(newText)) {
            result = USELESS_SAVE;
            return SUCCESS;
        }

        result = super.execute();
        date = new Date();


        return SUCCESS;
    }


    /**
     * Use to display a beautiful date in jsp
     *
     * @return
     */
    public String getFormatDate() {
        return date.toString();
    }


}
